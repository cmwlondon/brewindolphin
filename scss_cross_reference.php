<?php
/*
cross reference ids/classes used in HTML templates with ids/classes pulled from SCSS source files to find unused SCSS styles
*/

function getselectorcountforfile( $file ) {
	global $selectors2;
	return count($selectors2[$file]);
}

$rootPath = '/Applications/MAMP/htdocs/bd/current/';

$usedSelectorsFile = $rootPath.'used_selectors.json';
$selectorFile = $rootPath.'selectors.json';
$unusedFile = $rootPath.'unused.json';

// load data sets
$fp = fopen($selectorFile , 'r');
$selectorsJSON = fread($fp,filesize($selectorFile));
fclose( $fp );
$selectors = json_decode($selectorsJSON);

$fp = fopen($usedSelectorsFile , 'r');
$usedSelectorsJSON = fread($fp,filesize($usedSelectorsFile));
fclose( $fp );
$usedSelectors = json_decode($usedSelectorsJSON);

echo "<p>selectors: ".count($selectors)."</p>";
echo "<p>usedSelectors: ".count($usedSelectors)."</p>";

// rearrange selctors, group by file
$selectors2 = [];
foreach ($selectors AS $selector) {
	if ( !array_key_exists ( $selector->file , $selectors2 )) {
		$selectors2[$selector->file] = [];
	}
	$prefix = ($selector->type === 'id') ? '#' : '.';
	$selectors2[$selector->file][] = $prefix.$selector->name;
}

// find unused selectors (selectors specified in SCSS but not used in HTML temnplates)
// some selectors are applied by scripts

$unused = [];

foreach ($selectors AS $selector) {
	$found = false;
	foreach ($usedSelectors AS $usedSelector) {
		// echo "<p>".$usedSelector->type." - ".$selector->type." / ".$usedSelector->name." - ".$selector->name."</p>";
		if( $usedSelector->type === $selector->type && $usedSelector->name === $selector->name ) {
			$found = true;
		}
	}
	if (!$found) {
		// create file
		if ( !array_key_exists ( $selector->file , $unused )) {
			$unused[$selector->file] = [
				'selectors' => [],
				'total' => getselectorcountforfile($selector->file),
				'unused' => 0
			];
		}
		$prefix = ($selector->type === 'id') ? '#' : '.';
		$unused[$selector->file]['selectors'][] = $prefix.$selector->name;
		$unused[$selector->file]['unused'] = count($unused[$selector->file]['selectors']);
	}
}

// echo "<pre>".print_r($unused, true)."</pre>";
echo "<h2>unused CSS selectors</h2>";
foreach ($unused AS $key => $unusedFile) {
	echo "<p>".$key."</p>";
	echo "<p>total selectors in file: <strong>".$unusedFile['total']."</strong> unused: <strong>".$unusedFile['unused']."</strong></p>";
	echo "<ul>";
	foreach ($unusedFile['selectors'] AS $selector) {
		echo "<li>$selector</li>";
	}	
	echo "</ul>";
}

$fp = fopen( $unusedFile, 'w');
fwrite($fp, json_encode( $unused ));
fclose($fp);

?>
