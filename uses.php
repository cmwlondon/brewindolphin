<?php
/*
scans template files building a list of id and classes used in each template file
outputes two JSON files:
templatelist.json
[
	'filename',
	....
]
used_selectors.json
[
	[
		'type' => 'id' / 'class',
		'file' => 'FILENAME',
		'name' => 'NAME'
	],
	...
]
*/

function isSelectorAlreadyRecorded($type, $name) {
	global $usedSelectors;

	if ( count($usedSelectors) > 0 ) {
		foreach( $usedSelectors AS $usedSelector ) {
			if ($usedSelector['type'] == $type && $usedSelector['name'] == $name) {
				return true;
			}
		}
	}

	return false;
}

$rootPath = '/Applications/MAMP/htdocs/bd/current/';
$templates = $rootPath.'templates/';
$templatelist = $rootPath.'templatelist.json';
$usedSelectorsFile = $rootPath.'used_selectors.json';

$files = [];
$usedSelectors = [];
$attributeRegex = "=\"(.*)\"";

$omitList = array('bootstrap', 'fontawesome', 'slick', 'styles.scss','#fff','#ffffff','#cb2026','#ededed','#ff0000','#f00','#d2d7df','#c7c7c7','#ececed','#ebebed');

function scanDirectory($directory) {
	// echo "<p><strong>directory: '$directory'</strong></p>";

	global $omitList;
	global $files;
	$result = array(); 

	$fileList = scandir($directory);

	foreach($fileList AS $item) {
		switch($item) {
			case '.' :
			case '..' :
			case '.DS_Store' : {

			} break;
			default : {
				if (is_dir($directory . $item)) {
					// directory
					if ( !in_array($item, $omitList) ) {
						scanDirectory($directory . $item . '/');
					} else {
					}
				} else {
					if ( !in_array($item, $omitList) ) {
						$files[] = $directory . $item;
					}
				}
			}
		}
	}
	return;
};

// scan templates directory to find hTML files to examine
scanDirectory($templates);
$templateFiles = $files;

/*
$fp = fopen( $templatelist, 'w');
fwrite($fp, json_encode( $templateFiles ));
fclose($fp);

header('Content-Type: application/json');
echo json_encode( $templateFiles );
*/

// iterate through list of template files
// scan each one for class/id attributes
// class="(.*)"
// id="(.*)"
foreach($templateFiles AS $template) {
	echo "<p>$template size: ".filesize ( $template )."</p>";

	$fp = fopen($template , 'r');
	$contents = fread($fp,filesize($template));
	fclose( $fp );

	// id
	preg_match_all('/id="([[:alnum:][:space:]_-]*)"/', $contents, $matches, PREG_PATTERN_ORDER);

	if ( count($matches) > 0 ) {
		foreach( $matches[1] AS $id ) {
			if ( !isSelectorAlreadyRecorded('id', $id) ) {
				$usedSelectors[] = [
					'type' => 'id',
					'file' => $template,
					'name' => $id
				];
			}
		}
	}

	// class
	preg_match_all('/class="([[:alnum:][:space:]_-]*)"/', $contents, $matches, PREG_PATTERN_ORDER);

	if ( count($matches) > 0 ) {
		foreach( $matches[1] AS $match ) {
			// split class attribute on space to handle multiple classes
			$classes = explode(" ", $match);

			foreach( $classes AS $class ) {
				if ( !isSelectorAlreadyRecorded('class', $class) ) {
					$usedSelectors[] = [
						'type' => 'class',
						'file' => $template,
						'name' => $class
					];
				}
			}
		}
	}
}

$usedSelectorCount = count($usedSelectors);
if ( $usedSelectorCount > 0 ) {
	echo "<p>used selectors: $usedSelectorCount</p>";
	// echo "<pre>".print_r($usedSelectors, true)."</pre>";
	$fp = fopen( $usedSelectorsFile, 'w');
	fwrite($fp, json_encode( $usedSelectors ));
	fclose($fp);

}

?>