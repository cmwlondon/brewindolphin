var markers = [];
var map;
var offices = [{ lat: 53.312238, lng: -6.237485 }];

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    zoom: 12,
    center: offices[0]
  });
  drop();
}

function drop() {
  clearMarkers();
  for (var i = 0; i < offices.length; i++) {
    addMarkerWithTimeout(offices[i], i * 400);
  }
}

function addMarkerWithTimeout(position, timeout) {
  window.setTimeout(function () {
    markers.push(new google.maps.Marker({
      position: position,
      map: map,
      animation: google.maps.Animation.DROP
    }));
  }, timeout);
}

function clearMarkers() {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(null);
  }
  markers = [];
}