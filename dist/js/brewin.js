$(document).ready(function () {
    SetDropdownValAndRedirect(false);
});
function GetQueryStringVal(paramname) {
    var vars = [], hash;
    var q = document.URL.split('?')[1];
    if (q != undefined) {
        q = q.split('&');
        for (var i = 0; i < q.length; i++) {
            hash = q[i].split('=');
            vars.push(hash[1]);
            vars[hash[0]] = hash[1];
        }
    }
    return vars[paramname];
}
function SetDropdownValAndRedirect(doredirect) {
    var chsel = getCookie("userchannel");
    if (chsel != "") {
        var urltoredirect = "";
        switch (chsel) {
        case $(".mchnlval1").attr("data-val"):
            urltoredirect = $("#mchnlval1").val();
            break;
        case $(".mchnlval2").attr("data-val"):
            urltoredirect = $("#mchnlval2").val();
            break;
        case $(".mchnlval3").attr("data-val"):
            urltoredirect = $("#mchnlval3").val();
            break;
        case $(".mchnlval4").attr("data-val"):
            urltoredirect = $("#mchnlval4").val();
            break;
        }
        if (urltoredirect != "") {
            var pathname = window.location.pathname.toLowerCase();
            //if (pathname.indexOf("/episerver/cms") == -1 && window.location.pathname != urltoredirect && (pathname == "/" || doredirect == true)) {
            if (pathname.indexOf("/episerver/cms") == -1 && window.location.pathname != urltoredirect && (doredirect == true)) {
                window.location.href = urltoredirect;
            }
        }
    }
    var sitesection = $("#sitesectionval").val();
    if (sitesection != "") {
        var chnltxt = "";
        switch (sitesection) {
        case $(".mchnlval1").attr("data-val"):
            chnltxt = $(".mchnlval1").html();
            break;
        case $(".mchnlval2").attr("data-val"):
            chnltxt = $(".mchnlval2").html();
            break;
        case $(".mchnlval3").attr("data-val"):
            chnltxt = $(".mchnlval3").html();
            break;
        case $(".mchnlval4").attr("data-val"):
            chnltxt = $(".mchnlval4").html();
            break;
        }
        if (chnltxt != "") {
            $("#channelselected").html(chnltxt);
        }
    }
}
$(".mchnlval").click(function () {
    var channel = $(this).attr("data-val");
    setCookie("userchannel", channel, 30);
    SetDropdownValAndRedirect(true);
});
$(".mchnl-top-nav").click(function() {
    var urlval = $(this).attr("data-val");
    var channel = "";
    switch (urlval) {
        case $("#mchnlval1").val():
            channel = $(".mchnlval1").attr("data-val");
            break;
        case $("#mchnlval2").val():
            channel = $(".mchnlval2").attr("data-val");
            break;
        case $("#mchnlval3").val():
            channel = $(".mchnlval3").attr("data-val");
            break;
        case $("#mchnlval4").val():
            channel = $(".mchnlval4").attr("data-val");
            break;
    }
    if (channel != "") {
        setCookie("userchannel", channel, 30);
    }
});
function FaTermsAccept() {
    setCookie("fa-term-accp", "yes", 30);
    instance.close();
};
function FaTermsDecline() {
    setCookie("fa-term-accp", '', -1);
    setCookie("userchannel", '', -1);
    window.location.href = "/";
};
var BDFormValidateFormError = function () {
    if ($('#formerror').val() == "true") {
        return false;
    } else {
        return true;
    }
};
function FindOutMoreOpenEvent() {
    console.log('form_open');
    dataLayer.push({ 'event': 'form_open' });
}