/*! @source http://purl.eligrey.com/github/classList.js/blob/master/classList.js */
"document"in self&&("classList"in document.createElement("_")&&(!document.createElementNS||"classList"in document.createElementNS("http://www.w3.org/2000/svg","g"))||!function(t){"use strict";if("Element"in t){var e="classList",n="prototype",i=t.Element[n],s=Object,r=String[n].trim||function(){return this.replace(/^\s+|\s+$/g,"")},o=Array[n].indexOf||function(t){for(var e=0,n=this.length;n>e;e++)if(e in this&&this[e]===t)return e;return-1},c=function(t,e){this.name=t,this.code=DOMException[t],this.message=e},a=function(t,e){if(""===e)throw new c("SYNTAX_ERR","The token must not be empty.");if(/\s/.test(e))throw new c("INVALID_CHARACTER_ERR","The token must not contain space characters.");return o.call(t,e)},l=function(t){for(var e=r.call(t.getAttribute("class")||""),n=e?e.split(/\s+/):[],i=0,s=n.length;s>i;i++)this.push(n[i]);this._updateClassName=function(){t.setAttribute("class",this.toString())}},u=l[n]=[],h=function(){return new l(this)};if(c[n]=Error[n],u.item=function(t){return this[t]||null},u.contains=function(t){return~a(this,t+"")},u.add=function(){var t,e=arguments,n=0,i=e.length,s=!1;do t=e[n]+"",~a(this,t)||(this.push(t),s=!0);while(++n<i);s&&this._updateClassName()},u.remove=function(){var t,e,n=arguments,i=0,s=n.length,r=!1;do for(t=n[i]+"",e=a(this,t);~e;)this.splice(e,1),r=!0,e=a(this,t);while(++i<s);r&&this._updateClassName()},u.toggle=function(t,e){var n=this.contains(t),i=n?e!==!0&&"remove":e!==!1&&"add";return i&&this[i](t),e===!0||e===!1?e:!n},u.replace=function(t,e){var n=a(t+"");~n&&(this.splice(n,1,e),this._updateClassName())},u.toString=function(){return this.join(" ")},s.defineProperty){var f={get:h,enumerable:!0,configurable:!0};try{s.defineProperty(i,e,f)}catch(p){void 0!==p.number&&-2146823252!==p.number||(f.enumerable=!1,s.defineProperty(i,e,f))}}else s[n].__defineGetter__&&i.__defineGetter__(e,h)}}(self),function(){"use strict";var t=document.createElement("_");if(t.classList.add("c1","c2"),!t.classList.contains("c2")){var e=function(t){var e=DOMTokenList.prototype[t];DOMTokenList.prototype[t]=function(t){var n,i=arguments.length;for(n=0;i>n;n++)t=arguments[n],e.call(this,t)}};e("add"),e("remove")}if(t.classList.toggle("c3",!1),t.classList.contains("c3")){var n=DOMTokenList.prototype.toggle;DOMTokenList.prototype.toggle=function(t,e){return 1 in arguments&&!this.contains(t)==!e?e:n.call(this,t)}}"replace"in document.createElement("_").classList||(DOMTokenList.prototype.replace=function(t,e){var n=this.toString().split(" "),i=n.indexOf(t+"");~i&&(n=n.slice(i),this.remove.apply(this,n),this.add(e),this.add.apply(this,n.slice(1)))}),t=null}());

// https://gist.github.com/davidhund/b995353fdf9ce387b8a2
function cssPropertySupported(pNames) {
	var element = document.createElement('a');
	var index = pNames.length;

	try {
		while (index--) {
			var name = pNames[index];

			element.style.display = name;
			if (element.style.display === name) {
				return true;
			}
		}
	} catch (pError) {}

	return false;
}

// cssPropertySupported(['-webkit-box', '-ms-flex', 'flex']);

(function(d){
  var c = ( cssPropertySupported(['-webkit-box', '-ms-flex', 'flex'])) ? " flex" : " noflex";
  d.documentElement.className += c; 
})(document);
// IE9 requestanimationframe
(function() {
    var lastTime = 0;
    var vendors = ['webkit', 'moz'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame =
          window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

// object.assign polyfill for all IE
// https://stackoverflow.com/questions/32148218/why-does-object-assign-require-a-polyfill-when-babel-loader-is-being-used
if (typeof Object.assign != 'function') {
  Object.assign = function(target, varArgs) {
    'use strict';
    if (target == null) { // TypeError if undefined or null
      throw new TypeError('Cannot convert undefined or null to object');
    }

    var to = Object(target);

    for (var index = 1; index < arguments.length; index++) {
      var nextSource = arguments[index];

      if (nextSource != null) { // Skip over if undefined or null
        for (var nextKey in nextSource) {
          // Avoid bugs when hasOwnProperty is shadowed
          if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
            to[nextKey] = nextSource[nextKey];
          }
        }
      }
    }
    return to;
  };
}

/*
https://developer.mozilla.org/en-US/docs/Web/Events/resize
*/

(function() {
    var throttle = function(type, name, obj) {
        obj = obj || window;
        var running = false;
        var func = function() {
            if (running) { return; }
            running = true;
             requestAnimationFrame(function() {
                obj.dispatchEvent(new CustomEvent(name));
                running = false;
            });
        };
        obj.addEventListener(type, func);
    };

    /* init - you can init any event */
    throttle("resize", "optimizedResize");
})();

(function() {

     "use strict";

     //shim for String's trim function..
     function trim(string){
         return string.trim ? string.trim() : string.replace(/^\s+|\s+$/g, "");
     }

     //returns whether the given element has the given class name..
     function hasClassName(element, className){
         //refactoring of Prototype's function..
         var elClassName = element.className;
         if(!elClassName)
             return false;
         var regex = new RegExp("(^|\\s)" + className + "(\\s|$)");
         return regex.test(element.className);
     }

     function removeClassName(element, className){
         //refactoring of Prototype's function..
         var elClassName = element.className;
         if(!elClassName)
             return;
         element.className = elClassName.replace(
             new RegExp("(^|\\s+)" + className + "(\\s+|$)"), ' ');
     }

     function addClassName(element, className){
         var elClassName = element.className;
         if(elClassName)
             element.className += " " + className;
         else
             element.className = className;
     }

     //strings to make event attachment x-browser..
     var addEvent = document.addEventListener ?
            'addEventListener' : 'attachEvent';
     var eventPrefix = document.addEventListener ? '' : 'on';

     //the class which is added when the placeholder is being used..
     var placeHolderClassName = 'usingPlaceHolder';

     //allows the given textField to use it's placeholder attribute
     //as if it's functionality is supported natively..
     window.placeHolder = function(textField){

         //don't do anything if you get it for free..
         if('placeholder' in document.createElement('input'))
             return;

         //don't do anything if the place holder attribute is not
         //defined or is blank..
         var placeHolder = textField.getAttribute('placeholder');
         if(!placeHolder)
             return;

         //if it's just the empty string do nothing..
         placeHolder = trim(placeHolder);
         if(placeHolder === '')
             return;

         //called on blur - sets the value to the place holder if it's empty..
         var onBlur = function(){
             if(textField.value !== '') //a space is a valid input..
                 return;
             textField.value = placeHolder;
             addClassName(textField, placeHolderClassName);
         };

         //the blur event..
         textField[addEvent](eventPrefix + 'blur', onBlur, false);

         //the focus event - removes the place holder if required..
         textField[addEvent](eventPrefix + 'focus', function(){
             if(hasClassName(textField, placeHolderClassName)){
                removeClassName(textField, placeHolderClassName);
                textField.value = "";
             }
         }, false);

         //the submit event on the form to which it's associated - if the
         //placeholder is attached set the value to be empty..
         var form = textField.form;
         if(form){
             form[addEvent](eventPrefix + 'submit', function(){
                 if(hasClassName(textField, placeHolderClassName))
                     textField.value = '';
            }, false);
         }

         onBlur(); //call the onBlur to set it initially..
    };

}());

!function(e,t){if("object"==typeof exports&&"object"==typeof module)module.exports=t();else if("function"==typeof define&&define.amd)define([],t);else{var n=t();for(var i in n)("object"==typeof exports?exports:e)[i]=n[i]}}(this,function(){return function(e){function t(i){if(n[i])return n[i].exports;var o=n[i]={exports:{},id:i,loaded:!1};return e[i].call(o.exports,o,o.exports,t),o.loaded=!0,o.exports}var n={};return t.m=e,t.c=n,t.p="",t(0)}([function(e,t,n){e.exports=n(1)},function(e,t,n){"use strict";function i(e){return e&&e.__esModule?e:{default:e}}function o(e,t){function n(e,t){var n=z,i=n.classNameActiveSlide;e.forEach(function(e,t){e.classList.contains(i)&&e.classList.remove(i)}),e[t].classList.add(i)}function i(e){var t=z,n=t.infinite,i=e.slice(0,n),o=e.slice(e.length-n,e.length);return i.forEach(function(e){var t=e.cloneNode(!0);B.appendChild(t)}),o.reverse().forEach(function(e){var t=e.cloneNode(!0);B.insertBefore(t,B.firstChild)}),B.addEventListener(O.transitionEnd,L),f.call(B.children)}function o(t,n,i){(0,l.default)(e,t+".lory."+n,i)}function s(e,t,n){var i=B&&B.style;i&&(i[O.transition+"TimingFunction"]=n,i[O.transition+"Duration"]=t+"ms",O.hasTranslate3d?i[O.transform]="translate3d("+e+"px, 0, 0)":i[O.transform]="translate("+e+"px, 0)")}function d(e,t){var i=z,r=i.slideSpeed,a=i.slidesToScroll,d=i.infinite,l=i.rewind,c=i.rewindSpeed,u=i.ease,v=i.classNameActiveSlide,m=r,p=t?P+1:P-1,h=Math.round(M-S);o("before","slide",{index:P,nextSlide:p}),k&&k.classList.remove("disabled"),T&&T.classList.remove("disabled"),"number"!=typeof e&&(e=t?P+a:P-a),e=Math.min(Math.max(e,0),_.length-1),d&&void 0===t&&(e+=d);var b=Math.min(Math.max(_[e].offsetLeft*-1,h*-1),0);l&&Math.abs(N.x)===h&&t&&(b=0,e=0,m=c),s(b,m,u),N.x=b,_[e].offsetLeft<=h&&(P=e),!d||e!==_.length-d&&0!==e||(t&&(P=d),t||(P=_.length-2*d),N.x=_[P].offsetLeft*-1,A=function(){s(_[P].offsetLeft*-1,0,void 0)}),v&&n(f.call(_),P),k&&!d&&0===e&&k.classList.add("disabled"),!T||d||l||e+1!==_.length||T.classList.add("disabled"),o("after","slide",{currentSlide:P})}function c(){o("before","init"),O=(0,a.default)(),z=r({},u.default,t);var s=z,d=s.classNameFrame,l=s.classNameSlideContainer,c=s.classNamePrevCtrl,m=s.classNameNextCtrl,p=s.enableMouseEvents,E=s.classNameActiveSlide;j=e.getElementsByClassName(d)[0],B=j.getElementsByClassName(l)[0],k=e.getElementsByClassName(c)[0],T=e.getElementsByClassName(m)[0],N={x:B.offsetLeft,y:B.offsetTop},z.infinite?_=i(f.call(B.children)):(_=f.call(B.children),k&&k.classList.add("disabled"),T&&1===_.length&&!z.rewind&&T.classList.add("disabled")),v(),E&&n(_,P),k&&T&&(k.addEventListener("click",h),T.addEventListener("click",b)),j.addEventListener("touchstart",y),p&&(j.addEventListener("mousedown",y),j.addEventListener("click",g)),z.window.addEventListener("resize",C),o("after","init")}function v(){var e=z,t=e.infinite,i=e.ease,o=e.rewindSpeed,r=e.rewindOnResize,a=e.classNameActiveSlide;M=B.getBoundingClientRect().width||B.offsetWidth,S=j.getBoundingClientRect().width||j.offsetWidth,S===M&&(M=_.reduce(function(e,t){return e+t.getBoundingClientRect().width||t.offsetWidth},0)),r?P=0:(i=null,o=0),t?(s(_[P+t].offsetLeft*-1,0,null),P+=t,N.x=_[P].offsetLeft*-1):(s(_[P].offsetLeft*-1,o,i),N.x=_[P].offsetLeft*-1),a&&n(f.call(_),P)}function m(e){d(e)}function p(){return P-z.infinite||0}function h(){d(!1,!1)}function b(){d(!1,!0)}function E(){o("before","destroy"),j.removeEventListener(O.transitionEnd,L),j.removeEventListener("touchstart",y),j.removeEventListener("touchmove",x),j.removeEventListener("touchend",w),j.removeEventListener("mousemove",x),j.removeEventListener("mousedown",y),j.removeEventListener("mouseup",w),j.removeEventListener("mouseleave",w),j.removeEventListener("click",g),z.window.removeEventListener("resize",C),k&&k.removeEventListener("click",h),T&&T.removeEventListener("click",b),z.infinite&&Array.apply(null,Array(z.infinite)).forEach(function(){B.removeChild(B.firstChild),B.removeChild(B.lastChild)}),o("after","destroy")}function L(){A&&(A(),A=void 0)}function y(e){var t=z,n=t.enableMouseEvents,i=e.touches?e.touches[0]:e;n&&(j.addEventListener("mousemove",x),j.addEventListener("mouseup",w),j.addEventListener("mouseleave",w)),j.addEventListener("touchmove",x),j.addEventListener("touchend",w);var r=i.pageX,s=i.pageY;D={x:r,y:s,time:Date.now()},F=void 0,R={},o("on","touchstart",{event:e})}function x(e){var t=e.touches?e.touches[0]:e,n=t.pageX,i=t.pageY;R={x:n-D.x,y:i-D.y},"undefined"==typeof F&&(F=!!(F||Math.abs(R.x)<Math.abs(R.y))),!F&&D&&(e.preventDefault(),s(N.x+R.x,0,null)),o("on","touchmove",{event:e})}function w(e){var t=D?Date.now()-D.time:void 0,n=Number(t)<300&&Math.abs(R.x)>25||Math.abs(R.x)>S/3,i=!P&&R.x>0||P===_.length-1&&R.x<0,r=R.x<0;F||(n&&!i?d(!1,r):s(N.x,z.snapBackSpeed)),D=void 0,j.removeEventListener("touchmove",x),j.removeEventListener("touchend",w),j.removeEventListener("mousemove",x),j.removeEventListener("mouseup",w),j.removeEventListener("mouseleave",w),o("on","touchend",{event:e})}function g(e){R.x&&e.preventDefault()}function C(e){v(),o("on","resize",{event:e})}var N=void 0,M=void 0,S=void 0,_=void 0,j=void 0,B=void 0,k=void 0,T=void 0,O=void 0,A=void 0,P=0,z={};"undefined"!=typeof jQuery&&e instanceof jQuery&&(e=e[0]);var D=void 0,R=void 0,F=void 0;return c(),{setup:c,reset:v,slideTo:m,returnIndex:p,prev:h,next:b,destroy:E}}Object.defineProperty(t,"__esModule",{value:!0});var r=Object.assign||function(e){for(var t=1;t<arguments.length;t++){var n=arguments[t];for(var i in n)Object.prototype.hasOwnProperty.call(n,i)&&(e[i]=n[i])}return e};t.lory=o;var s=n(2),a=i(s),d=n(3),l=i(d),c=n(5),u=i(c),f=Array.prototype.slice},function(e,t){(function(e){"use strict";function n(){var t=void 0,n=void 0,i=void 0,o=void 0;return function(){var r=document.createElement("_"),s=r.style,a=void 0;""===s[a="webkitTransition"]&&(i="webkitTransitionEnd",n=a),""===s[a="transition"]&&(i="transitionend",n=a),""===s[a="webkitTransform"]&&(t=a),""===s[a="msTransform"]&&(t=a),""===s[a="transform"]&&(t=a),document.body.insertBefore(r,null),s[t]="translate3d(0, 0, 0)",o=!!e.getComputedStyle(r).getPropertyValue(t),document.body.removeChild(r)}(),{transform:t,transition:n,transitionEnd:i,hasTranslate3d:o}}Object.defineProperty(t,"__esModule",{value:!0}),t.default=n}).call(t,function(){return this}())},function(e,t,n){"use strict";function i(e){return e&&e.__esModule?e:{default:e}}function o(e,t,n){var i=new s.default(t,{bubbles:!0,cancelable:!0,detail:n});e.dispatchEvent(i)}Object.defineProperty(t,"__esModule",{value:!0}),t.default=o;var r=n(4),s=i(r)},function(e,t){(function(t){function n(){try{var e=new i("cat",{detail:{foo:"bar"}});return"cat"===e.type&&"bar"===e.detail.foo}catch(e){}return!1}var i=t.CustomEvent;e.exports=n()?i:"undefined"!=typeof document&&"function"==typeof document.createEvent?function(e,t){var n=document.createEvent("CustomEvent");return t?n.initCustomEvent(e,t.bubbles,t.cancelable,t.detail):n.initCustomEvent(e,!1,!1,void 0),n}:function(e,t){var n=document.createEventObject();return n.type=e,t?(n.bubbles=Boolean(t.bubbles),n.cancelable=Boolean(t.cancelable),n.detail=t.detail):(n.bubbles=!1,n.cancelable=!1,n.detail=void 0),n}}).call(t,function(){return this}())},function(e,t){"use strict";Object.defineProperty(t,"__esModule",{value:!0}),t.default={slidesToScroll:1,slideSpeed:300,rewindSpeed:600,snapBackSpeed:200,ease:"ease",rewind:!1,infinite:!1,classNameFrame:"js_frame",classNameSlideContainer:"js__slides",classNamePrevCtrl:"js_prev",classNameNextCtrl:"js_next",classNameActiveSlide:"active",enableMouseEvents:!1,window:window,rewindOnResize:!0}}])});
//# sourceMappingURL=lory.min.js.map


/*!
 * validate.js 0.12.0
 *
 * (c) 2013-2017 Nicklas Ansman, 2013 Wrapp
 * Validate.js may be freely distributed under the MIT license.
 * For all details and documentation:
 * http://validatejs.org/
 */

(function(exports, module, define) {
  "use strict";

  // The main function that calls the validators specified by the constraints.
  // The options are the following:
  //   - format (string) - An option that controls how the returned value is formatted
  //     * flat - Returns a flat array of just the error messages
  //     * grouped - Returns the messages grouped by attribute (default)
  //     * detailed - Returns an array of the raw validation data
  //   - fullMessages (boolean) - If `true` (default) the attribute name is prepended to the error.
  //
  // Please note that the options are also passed to each validator.
  var validate = function(attributes, constraints, options) {
    options = v.extend({}, v.options, options);

    var results = v.runValidations(attributes, constraints, options)
      , attr
      , validator;

    if (results.some(function(r) { return v.isPromise(r.error); })) {
      throw new Error("Use validate.async if you want support for promises");
    }
    return validate.processValidationResults(results, options);
  };

  var v = validate;

  // Copies over attributes from one or more sources to a single destination.
  // Very much similar to underscore's extend.
  // The first argument is the target object and the remaining arguments will be
  // used as sources.
  v.extend = function(obj) {
    [].slice.call(arguments, 1).forEach(function(source) {
      for (var attr in source) {
        obj[attr] = source[attr];
      }
    });
    return obj;
  };

  v.extend(validate, {
    // This is the version of the library as a semver.
    // The toString function will allow it to be coerced into a string
    version: {
      major: 0,
      minor: 12,
      patch: 0,
      metadata: null,
      toString: function() {
        var version = v.format("%{major}.%{minor}.%{patch}", v.version);
        if (!v.isEmpty(v.version.metadata)) {
          version += "+" + v.version.metadata;
        }
        return version;
      }
    },

    // Below is the dependencies that are used in validate.js

    // The constructor of the Promise implementation.
    // If you are using Q.js, RSVP or any other A+ compatible implementation
    // override this attribute to be the constructor of that promise.
    // Since jQuery promises aren't A+ compatible they won't work.
    Promise: typeof Promise !== "undefined" ? Promise : /* istanbul ignore next */ null,

    EMPTY_STRING_REGEXP: /^\s*$/,

    // Runs the validators specified by the constraints object.
    // Will return an array of the format:
    //     [{attribute: "<attribute name>", error: "<validation result>"}, ...]
    runValidations: function(attributes, constraints, options) {
      var results = []
        , attr
        , validatorName
        , value
        , validators
        , validator
        , validatorOptions
        , error;

      if (v.isDomElement(attributes) || v.isJqueryElement(attributes)) {
        attributes = v.collectFormValues(attributes);
      }

      // Loops through each constraints, finds the correct validator and run it.
      for (attr in constraints) {
        value = v.getDeepObjectValue(attributes, attr);
        // This allows the constraints for an attribute to be a function.
        // The function will be called with the value, attribute name, the complete dict of
        // attributes as well as the options and constraints passed in.
        // This is useful when you want to have different
        // validations depending on the attribute value.
        validators = v.result(constraints[attr], value, attributes, attr, options, constraints);

        for (validatorName in validators) {
          validator = v.validators[validatorName];

          if (!validator) {
            error = v.format("Unknown validator %{name}", {name: validatorName});
            throw new Error(error);
          }

          validatorOptions = validators[validatorName];
          // This allows the options to be a function. The function will be
          // called with the value, attribute name, the complete dict of
          // attributes as well as the options and constraints passed in.
          // This is useful when you want to have different
          // validations depending on the attribute value.
          validatorOptions = v.result(validatorOptions, value, attributes, attr, options, constraints);
          if (!validatorOptions) {
            continue;
          }
          results.push({
            attribute: attr,
            value: value,
            validator: validatorName,
            globalOptions: options,
            attributes: attributes,
            options: validatorOptions,
            error: validator.call(validator,
                value,
                validatorOptions,
                attr,
                attributes,
                options)
          });
        }
      }

      return results;
    },

    // Takes the output from runValidations and converts it to the correct
    // output format.
    processValidationResults: function(errors, options) {
      errors = v.pruneEmptyErrors(errors, options);
      errors = v.expandMultipleErrors(errors, options);
      errors = v.convertErrorMessages(errors, options);

      var format = options.format || "grouped";

      if (typeof v.formatters[format] === 'function') {
        errors = v.formatters[format](errors);
      } else {
        throw new Error(v.format("Unknown format %{format}", options));
      }

      return v.isEmpty(errors) ? undefined : errors;
    },

    // Runs the validations with support for promises.
    // This function will return a promise that is settled when all the
    // validation promises have been completed.
    // It can be called even if no validations returned a promise.
    async: function(attributes, constraints, options) {
      options = v.extend({}, v.async.options, options);

      var WrapErrors = options.wrapErrors || function(errors) {
        return errors;
      };

      // Removes unknown attributes
      if (options.cleanAttributes !== false) {
        attributes = v.cleanAttributes(attributes, constraints);
      }

      var results = v.runValidations(attributes, constraints, options);

      return new v.Promise(function(resolve, reject) {
        v.waitForResults(results).then(function() {
          var errors = v.processValidationResults(results, options);
          if (errors) {
            reject(new WrapErrors(errors, options, attributes, constraints));
          } else {
            resolve(attributes);
          }
        }, function(err) {
          reject(err);
        });
      });
    },

    single: function(value, constraints, options) {
      options = v.extend({}, v.single.options, options, {
        format: "flat",
        fullMessages: false
      });
      return v({single: value}, {single: constraints}, options);
    },

    // Returns a promise that is resolved when all promises in the results array
    // are settled. The promise returned from this function is always resolved,
    // never rejected.
    // This function modifies the input argument, it replaces the promises
    // with the value returned from the promise.
    waitForResults: function(results) {
      // Create a sequence of all the results starting with a resolved promise.
      return results.reduce(function(memo, result) {
        // If this result isn't a promise skip it in the sequence.
        if (!v.isPromise(result.error)) {
          return memo;
        }

        return memo.then(function() {
          return result.error.then(function(error) {
            result.error = error || null;
          });
        });
      }, new v.Promise(function(r) { r(); })); // A resolved promise
    },

    // If the given argument is a call: function the and: function return the value
    // otherwise just return the value. Additional arguments will be passed as
    // arguments to the function.
    // Example:
    // ```
    // result('foo') // 'foo'
    // result(Math.max, 1, 2) // 2
    // ```
    result: function(value) {
      var args = [].slice.call(arguments, 1);
      if (typeof value === 'function') {
        value = value.apply(null, args);
      }
      return value;
    },

    // Checks if the value is a number. This function does not consider NaN a
    // number like many other `isNumber` functions do.
    isNumber: function(value) {
      return typeof value === 'number' && !isNaN(value);
    },

    // Returns false if the object is not a function
    isFunction: function(value) {
      return typeof value === 'function';
    },

    // A simple check to verify that the value is an integer. Uses `isNumber`
    // and a simple modulo check.
    isInteger: function(value) {
      return v.isNumber(value) && value % 1 === 0;
    },

    // Checks if the value is a boolean
    isBoolean: function(value) {
      return typeof value === 'boolean';
    },

    // Uses the `Object` function to check if the given argument is an object.
    isObject: function(obj) {
      return obj === Object(obj);
    },

    // Simply checks if the object is an instance of a date
    isDate: function(obj) {
      return obj instanceof Date;
    },

    // Returns false if the object is `null` of `undefined`
    isDefined: function(obj) {
      return obj !== null && obj !== undefined;
    },

    // Checks if the given argument is a promise. Anything with a `then`
    // function is considered a promise.
    isPromise: function(p) {
      return !!p && v.isFunction(p.then);
    },

    isJqueryElement: function(o) {
      return o && v.isString(o.jquery);
    },

    isDomElement: function(o) {
      if (!o) {
        return false;
      }

      if (!o.querySelectorAll || !o.querySelector) {
        return false;
      }

      if (v.isObject(document) && o === document) {
        return true;
      }

      // http://stackoverflow.com/a/384380/699304
      /* istanbul ignore else */
      if (typeof HTMLElement === "object") {
        return o instanceof HTMLElement;
      } else {
        return o &&
          typeof o === "object" &&
          o !== null &&
          o.nodeType === 1 &&
          typeof o.nodeName === "string";
      }
    },

    isEmpty: function(value) {
      var attr;

      // Null and undefined are empty
      if (!v.isDefined(value)) {
        return true;
      }

      // functions are non empty
      if (v.isFunction(value)) {
        return false;
      }

      // Whitespace only strings are empty
      if (v.isString(value)) {
        return v.EMPTY_STRING_REGEXP.test(value);
      }

      // For arrays we use the length property
      if (v.isArray(value)) {
        return value.length === 0;
      }

      // Dates have no attributes but aren't empty
      if (v.isDate(value)) {
        return false;
      }

      // If we find at least one property we consider it non empty
      if (v.isObject(value)) {
        for (attr in value) {
          return false;
        }
        return true;
      }

      return false;
    },

    // Formats the specified strings with the given values like so:
    // ```
    // format("Foo: %{foo}", {foo: "bar"}) // "Foo bar"
    // ```
    // If you want to write %{...} without having it replaced simply
    // prefix it with % like this `Foo: %%{foo}` and it will be returned
    // as `"Foo: %{foo}"`
    format: v.extend(function(str, vals) {
      if (!v.isString(str)) {
        return str;
      }
      return str.replace(v.format.FORMAT_REGEXP, function(m0, m1, m2) {
        if (m1 === '%') {
          return "%{" + m2 + "}";
        } else {
          return String(vals[m2]);
        }
      });
    }, {
      // Finds %{key} style patterns in the given string
      FORMAT_REGEXP: /(%?)%\{([^\}]+)\}/g
    }),

    // "Prettifies" the given string.
    // Prettifying means replacing [.\_-] with spaces as well as splitting
    // camel case words.
    prettify: function(str) {
      if (v.isNumber(str)) {
        // If there are more than 2 decimals round it to two
        if ((str * 100) % 1 === 0) {
          return "" + str;
        } else {
          return parseFloat(Math.round(str * 100) / 100).toFixed(2);
        }
      }

      if (v.isArray(str)) {
        return str.map(function(s) { return v.prettify(s); }).join(", ");
      }

      if (v.isObject(str)) {
        return str.toString();
      }

      // Ensure the string is actually a string
      str = "" + str;

      return str
        // Splits keys separated by periods
        .replace(/([^\s])\.([^\s])/g, '$1 $2')
        // Removes backslashes
        .replace(/\\+/g, '')
        // Replaces - and - with space
        .replace(/[_-]/g, ' ')
        // Splits camel cased words
        .replace(/([a-z])([A-Z])/g, function(m0, m1, m2) {
          return "" + m1 + " " + m2.toLowerCase();
        })
        .toLowerCase();
    },

    stringifyValue: function(value, options) {
      var prettify = options && options.prettify || v.prettify;
      return prettify(value);
    },

    isString: function(value) {
      return typeof value === 'string';
    },

    isArray: function(value) {
      return {}.toString.call(value) === '[object Array]';
    },

    // Checks if the object is a hash, which is equivalent to an object that
    // is neither an array nor a function.
    isHash: function(value) {
      return v.isObject(value) && !v.isArray(value) && !v.isFunction(value);
    },

    contains: function(obj, value) {
      if (!v.isDefined(obj)) {
        return false;
      }
      if (v.isArray(obj)) {
        return obj.indexOf(value) !== -1;
      }
      return value in obj;
    },

    unique: function(array) {
      if (!v.isArray(array)) {
        return array;
      }
      return array.filter(function(el, index, array) {
        return array.indexOf(el) == index;
      });
    },

    forEachKeyInKeypath: function(object, keypath, callback) {
      if (!v.isString(keypath)) {
        return undefined;
      }

      var key = ""
        , i
        , escape = false;

      for (i = 0; i < keypath.length; ++i) {
        switch (keypath[i]) {
          case '.':
            if (escape) {
              escape = false;
              key += '.';
            } else {
              object = callback(object, key, false);
              key = "";
            }
            break;

          case '\\':
            if (escape) {
              escape = false;
              key += '\\';
            } else {
              escape = true;
            }
            break;

          default:
            escape = false;
            key += keypath[i];
            break;
        }
      }

      return callback(object, key, true);
    },

    getDeepObjectValue: function(obj, keypath) {
      if (!v.isObject(obj)) {
        return undefined;
      }

      return v.forEachKeyInKeypath(obj, keypath, function(obj, key) {
        if (v.isObject(obj)) {
          return obj[key];
        }
      });
    },

    // This returns an object with all the values of the form.
    // It uses the input name as key and the value as value
    // So for example this:
    // <input type="text" name="email" value="foo@bar.com" />
    // would return:
    // {email: "foo@bar.com"}
    collectFormValues: function(form, options) {
      var values = {}
        , i
        , j
        , input
        , inputs
        , option
        , value;

      if (v.isJqueryElement(form)) {
        form = form[0];
      }

      if (!form) {
        return values;
      }

      options = options || {};

      inputs = form.querySelectorAll("input[name], textarea[name]");
      for (i = 0; i < inputs.length; ++i) {
        input = inputs.item(i);

        if (v.isDefined(input.getAttribute("data-ignored"))) {
          continue;
        }

        name = input.name.replace(/\./g, "\\\\.");
        value = v.sanitizeFormValue(input.value, options);
        if (input.type === "number") {
          value = value ? +value : null;
        } else if (input.type === "checkbox") {
          if (input.attributes.value) {
            if (!input.checked) {
              value = values[name] || null;
            }
          } else {
            value = input.checked;
          }
        } else if (input.type === "radio") {
          if (!input.checked) {
            value = values[name] || null;
          }
        }
        values[name] = value;
      }

      inputs = form.querySelectorAll("select[name]");
      for (i = 0; i < inputs.length; ++i) {
        input = inputs.item(i);
        if (v.isDefined(input.getAttribute("data-ignored"))) {
          continue;
        }

        if (input.multiple) {
          value = [];
          for (j in input.options) {
            option = input.options[j];
             if (option && option.selected) {
              value.push(v.sanitizeFormValue(option.value, options));
            }
          }
        } else {
          var _val = typeof input.options[input.selectedIndex] !== 'undefined' ? input.options[input.selectedIndex].value : '';
          value = v.sanitizeFormValue(_val, options);
        }
        values[input.name] = value;
      }

      return values;
    },

    sanitizeFormValue: function(value, options) {
      if (options.trim && v.isString(value)) {
        value = value.trim();
      }

      if (options.nullify !== false && value === "") {
        return null;
      }
      return value;
    },

    capitalize: function(str) {
      if (!v.isString(str)) {
        return str;
      }
      return str[0].toUpperCase() + str.slice(1);
    },

    // Remove all errors who's error attribute is empty (null or undefined)
    pruneEmptyErrors: function(errors) {
      return errors.filter(function(error) {
        return !v.isEmpty(error.error);
      });
    },

    // In
    // [{error: ["err1", "err2"], ...}]
    // Out
    // [{error: "err1", ...}, {error: "err2", ...}]
    //
    // All attributes in an error with multiple messages are duplicated
    // when expanding the errors.
    expandMultipleErrors: function(errors) {
      var ret = [];
      errors.forEach(function(error) {
        // Removes errors without a message
        if (v.isArray(error.error)) {
          error.error.forEach(function(msg) {
            ret.push(v.extend({}, error, {error: msg}));
          });
        } else {
          ret.push(error);
        }
      });
      return ret;
    },

    // Converts the error mesages by prepending the attribute name unless the
    // message is prefixed by ^
    convertErrorMessages: function(errors, options) {
      options = options || {};

      var ret = []
        , prettify = options.prettify || v.prettify;
      errors.forEach(function(errorInfo) {
        var error = v.result(errorInfo.error,
            errorInfo.value,
            errorInfo.attribute,
            errorInfo.options,
            errorInfo.attributes,
            errorInfo.globalOptions);

        if (!v.isString(error)) {
          ret.push(errorInfo);
          return;
        }

        if (error[0] === '^') {
          error = error.slice(1);
        } else if (options.fullMessages !== false) {
          error = v.capitalize(prettify(errorInfo.attribute)) + " " + error;
        }
        error = error.replace(/\\\^/g, "^");
        error = v.format(error, {
          value: v.stringifyValue(errorInfo.value, options)
        });
        ret.push(v.extend({}, errorInfo, {error: error}));
      });
      return ret;
    },

    // In:
    // [{attribute: "<attributeName>", ...}]
    // Out:
    // {"<attributeName>": [{attribute: "<attributeName>", ...}]}
    groupErrorsByAttribute: function(errors) {
      var ret = {};
      errors.forEach(function(error) {
        var list = ret[error.attribute];
        if (list) {
          list.push(error);
        } else {
          ret[error.attribute] = [error];
        }
      });
      return ret;
    },

    // In:
    // [{error: "<message 1>", ...}, {error: "<message 2>", ...}]
    // Out:
    // ["<message 1>", "<message 2>"]
    flattenErrorsToArray: function(errors) {
      return errors
        .map(function(error) { return error.error; })
        .filter(function(value, index, self) {
          return self.indexOf(value) === index;
        });
    },

    cleanAttributes: function(attributes, whitelist) {
      function whitelistCreator(obj, key, last) {
        if (v.isObject(obj[key])) {
          return obj[key];
        }
        return (obj[key] = last ? true : {});
      }

      function buildObjectWhitelist(whitelist) {
        var ow = {}
          , lastObject
          , attr;
        for (attr in whitelist) {
          if (!whitelist[attr]) {
            continue;
          }
          v.forEachKeyInKeypath(ow, attr, whitelistCreator);
        }
        return ow;
      }

      function cleanRecursive(attributes, whitelist) {
        if (!v.isObject(attributes)) {
          return attributes;
        }

        var ret = v.extend({}, attributes)
          , w
          , attribute;

        for (attribute in attributes) {
          w = whitelist[attribute];

          if (v.isObject(w)) {
            ret[attribute] = cleanRecursive(ret[attribute], w);
          } else if (!w) {
            delete ret[attribute];
          }
        }
        return ret;
      }

      if (!v.isObject(whitelist) || !v.isObject(attributes)) {
        return {};
      }

      whitelist = buildObjectWhitelist(whitelist);
      return cleanRecursive(attributes, whitelist);
    },

    exposeModule: function(validate, root, exports, module, define) {
      if (exports) {
        if (module && module.exports) {
          exports = module.exports = validate;
        }
        exports.validate = validate;
      } else {
        root.validate = validate;
        if (validate.isFunction(define) && define.amd) {
          define([], function () { return validate; });
        }
      }
    },

    warn: function(msg) {
      if (typeof console !== "undefined" && console.warn) {
        console.warn("[validate.js] " + msg);
      }
    },

    error: function(msg) {
      if (typeof console !== "undefined" && console.error) {
        console.error("[validate.js] " + msg);
      }
    }
  });

  validate.validators = {
    // Presence validates that the value isn't empty
    presence: function(value, options) {
      options = v.extend({}, this.options, options);
      if (options.allowEmpty !== false ? !v.isDefined(value) : v.isEmpty(value)) {
        return options.message || this.message || "can't be blank";
      }
    },
    length: function(value, options, attribute) {
      // Empty values are allowed
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var is = options.is
        , maximum = options.maximum
        , minimum = options.minimum
        , tokenizer = options.tokenizer || function(val) { return val; }
        , err
        , errors = [];

      value = tokenizer(value);
      var length = value.length;
      if(!v.isNumber(length)) {
        v.error(v.format("Attribute %{attr} has a non numeric value for `length`", {attr: attribute}));
        return options.message || this.notValid || "has an incorrect length";
      }

      // Is checks
      if (v.isNumber(is) && length !== is) {
        err = options.wrongLength ||
          this.wrongLength ||
          "is the wrong length (should be %{count} characters)";
        errors.push(v.format(err, {count: is}));
      }

      if (v.isNumber(minimum) && length < minimum) {
        err = options.tooShort ||
          this.tooShort ||
          "is too short (minimum is %{count} characters)";
        errors.push(v.format(err, {count: minimum}));
      }

      if (v.isNumber(maximum) && length > maximum) {
        err = options.tooLong ||
          this.tooLong ||
          "is too long (maximum is %{count} characters)";
        errors.push(v.format(err, {count: maximum}));
      }

      if (errors.length > 0) {
        return options.message || errors;
      }
    },
    numericality: function(value, options, attribute, attributes, globalOptions) {
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var errors = []
        , name
        , count
        , checks = {
            greaterThan:          function(v, c) { return v > c; },
            greaterThanOrEqualTo: function(v, c) { return v >= c; },
            equalTo:              function(v, c) { return v === c; },
            lessThan:             function(v, c) { return v < c; },
            lessThanOrEqualTo:    function(v, c) { return v <= c; },
            divisibleBy:          function(v, c) { return v % c === 0; }
          }
        , prettify = options.prettify ||
          (globalOptions && globalOptions.prettify) ||
          v.prettify;

      // Strict will check that it is a valid looking number
      if (v.isString(value) && options.strict) {
        var pattern = "^-?(0|[1-9]\\d*)";
        if (!options.onlyInteger) {
          pattern += "(\\.\\d+)?";
        }
        pattern += "$";

        if (!(new RegExp(pattern).test(value))) {
          return options.message ||
            options.notValid ||
            this.notValid ||
            this.message ||
            "must be a valid number";
        }
      }

      // Coerce the value to a number unless we're being strict.
      if (options.noStrings !== true && v.isString(value) && !v.isEmpty(value)) {
        value = +value;
      }

      // If it's not a number we shouldn't continue since it will compare it.
      if (!v.isNumber(value)) {
        return options.message ||
          options.notValid ||
          this.notValid ||
          this.message ||
          "is not a number";
      }

      // Same logic as above, sort of. Don't bother with comparisons if this
      // doesn't pass.
      if (options.onlyInteger && !v.isInteger(value)) {
        return options.message ||
          options.notInteger ||
          this.notInteger ||
          this.message ||
          "must be an integer";
      }

      for (name in checks) {
        count = options[name];
        if (v.isNumber(count) && !checks[name](value, count)) {
          // This picks the default message if specified
          // For example the greaterThan check uses the message from
          // this.notGreaterThan so we capitalize the name and prepend "not"
          var key = "not" + v.capitalize(name);
          var msg = options[key] ||
            this[key] ||
            this.message ||
            "must be %{type} %{count}";

          errors.push(v.format(msg, {
            count: count,
            type: prettify(name)
          }));
        }
      }

      if (options.odd && value % 2 !== 1) {
        errors.push(options.notOdd ||
            this.notOdd ||
            this.message ||
            "must be odd");
      }
      if (options.even && value % 2 !== 0) {
        errors.push(options.notEven ||
            this.notEven ||
            this.message ||
            "must be even");
      }

      if (errors.length) {
        return options.message || errors;
      }
    },
    datetime: v.extend(function(value, options) {
      if (!v.isFunction(this.parse) || !v.isFunction(this.format)) {
        throw new Error("Both the parse and format functions needs to be set to use the datetime/date validator");
      }

      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var err
        , errors = []
        , earliest = options.earliest ? this.parse(options.earliest, options) : NaN
        , latest = options.latest ? this.parse(options.latest, options) : NaN;

      value = this.parse(value, options);

      // 86400000 is the number of milliseconds in a day, this is used to remove
      // the time from the date
      if (isNaN(value) || options.dateOnly && value % 86400000 !== 0) {
        err = options.notValid ||
          options.message ||
          this.notValid ||
          "must be a valid date";
        return v.format(err, {value: arguments[0]});
      }

      if (!isNaN(earliest) && value < earliest) {
        err = options.tooEarly ||
          options.message ||
          this.tooEarly ||
          "must be no earlier than %{date}";
        err = v.format(err, {
          value: this.format(value, options),
          date: this.format(earliest, options)
        });
        errors.push(err);
      }

      if (!isNaN(latest) && value > latest) {
        err = options.tooLate ||
          options.message ||
          this.tooLate ||
          "must be no later than %{date}";
        err = v.format(err, {
          date: this.format(latest, options),
          value: this.format(value, options)
        });
        errors.push(err);
      }

      if (errors.length) {
        return v.unique(errors);
      }
    }, {
      parse: null,
      format: null
    }),
    date: function(value, options) {
      options = v.extend({}, options, {dateOnly: true});
      return v.validators.datetime.call(v.validators.datetime, value, options);
    },
    format: function(value, options) {
      if (v.isString(options) || (options instanceof RegExp)) {
        options = {pattern: options};
      }

      options = v.extend({}, this.options, options);

      var message = options.message || this.message || "is invalid"
        , pattern = options.pattern
        , match;

      // Empty values are allowed
      if (!v.isDefined(value)) {
        return;
      }
      if (!v.isString(value)) {
        return message;
      }

      if (v.isString(pattern)) {
        pattern = new RegExp(options.pattern, options.flags);
      }
      match = pattern.exec(value);
      if (!match || match[0].length != value.length) {
        return message;
      }
    },
    inclusion: function(value, options) {
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }
      if (v.isArray(options)) {
        options = {within: options};
      }
      options = v.extend({}, this.options, options);
      if (v.contains(options.within, value)) {
        return;
      }
      var message = options.message ||
        this.message ||
        "^%{value} is not included in the list";
      return v.format(message, {value: value});
    },
    exclusion: function(value, options) {
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }
      if (v.isArray(options)) {
        options = {within: options};
      }
      options = v.extend({}, this.options, options);
      if (!v.contains(options.within, value)) {
        return;
      }
      var message = options.message || this.message || "^%{value} is restricted";
      return v.format(message, {value: value});
    },
    email: v.extend(function(value, options) {
      options = v.extend({}, this.options, options);
      var message = options.message || this.message || "is not a valid email";
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }
      if (!v.isString(value)) {
        return message;
      }
      if (!this.PATTERN.exec(value)) {
        return message;
      }
    }, {
      PATTERN: /^[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/i
    }),
    equality: function(value, options, attribute, attributes, globalOptions) {
      if (!v.isDefined(value)) {
        return;
      }

      if (v.isString(options)) {
        options = {attribute: options};
      }
      options = v.extend({}, this.options, options);
      var message = options.message ||
        this.message ||
        "is not equal to %{attribute}";

      if (v.isEmpty(options.attribute) || !v.isString(options.attribute)) {
        throw new Error("The attribute must be a non empty string");
      }

      var otherValue = v.getDeepObjectValue(attributes, options.attribute)
        , comparator = options.comparator || function(v1, v2) {
          return v1 === v2;
        }
        , prettify = options.prettify ||
          (globalOptions && globalOptions.prettify) ||
          v.prettify;

      if (!comparator(value, otherValue, options, attribute, attributes)) {
        return v.format(message, {attribute: prettify(options.attribute)});
      }
    },

    // A URL validator that is used to validate URLs with the ability to
    // restrict schemes and some domains.
    url: function(value, options) {
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var message = options.message || this.message || "is not a valid url"
        , schemes = options.schemes || this.schemes || ['http', 'https']
        , allowLocal = options.allowLocal || this.allowLocal || false;

      if (!v.isString(value)) {
        return message;
      }

      // https://gist.github.com/dperini/729294
      var regex =
        "^" +
        // protocol identifier
        "(?:(?:" + schemes.join("|") + ")://)" +
        // user:pass authentication
        "(?:\\S+(?::\\S*)?@)?" +
        "(?:";

      var tld = "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))";

      if (allowLocal) {
        tld += "?";
      } else {
        regex +=
          // IP address exclusion
          // private & local networks
          "(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
          "(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
          "(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})";
      }

      regex +=
          // IP address dotted notation octets
          // excludes loopback network 0.0.0.0
          // excludes reserved space >= 224.0.0.0
          // excludes network & broacast addresses
          // (first & last IP address of each class)
          "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
          "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
          "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
        "|" +
          // host name
          "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
          // domain name
          "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
          tld +
        ")" +
        // port number
        "(?::\\d{2,5})?" +
        // resource path
        "(?:[/?#]\\S*)?" +
      "$";

      var PATTERN = new RegExp(regex, 'i');
      if (!PATTERN.exec(value)) {
        return message;
      }
    }
  };

  validate.formatters = {
    detailed: function(errors) {return errors;},
    flat: v.flattenErrorsToArray,
    grouped: function(errors) {
      var attr;

      errors = v.groupErrorsByAttribute(errors);
      for (attr in errors) {
        errors[attr] = v.flattenErrorsToArray(errors[attr]);
      }
      return errors;
    },
    constraint: function(errors) {
      var attr;
      errors = v.groupErrorsByAttribute(errors);
      for (attr in errors) {
        errors[attr] = errors[attr].map(function(result) {
          return result.validator;
        }).sort();
      }
      return errors;
    }
  };

  validate.exposeModule(validate, this, exports, module, define);
}).call(this,
        typeof exports !== 'undefined' ? /* istanbul ignore next */ exports : null,
        typeof module !== 'undefined' ? /* istanbul ignore next */ module : null,
        typeof define !== 'undefined' ? /* istanbul ignore next */ define : null);

/* ------------------------------------------------
BDFormValidate
START
extends https://validatejs.org/
------------------------------------------------ */

  function BDFormValidate(parameters) {
    this.parameters = parameters;

    this.formSelector = parameters.formSelector;
    this.constraints = parameters.constraints;
    this.xcheckboxes = parameters.xcheckboxes;
    this.clientField = parameters.clientField;

    this.xcheckboxesProcess = false;
    this.clientProcess = false;
    this.validform = parameters.validform;

    this.placholderSupported = placholderSupported;


    this.init();
  }

  BDFormValidate.prototype = {
    "constructor" : BDFormValidate,

    "template" : function () { var that = this; },

    "init" : function () {
      var that = this;

      // detect validation config for checkbox 
      if (typeof this.xcheckboxes !== 'undefined') {
        console.log('xcheckboxes');
        this.xcheckboxesProcess = true;
      }

      // detect validation for client-yes/no radio button group
      if (typeof this.clientField !== 'undefined') {
        console.log('clientField');
        this.clientProcess = true;
      }

      this.form = document.querySelector( this.formSelector );

      // find checkboxes in constraints
      // this.removeCheckboxFields();

      /* custom validators */
      // yes/no radio button group

      // checkbox
      validate.validators.checked = function(value, options) {

        console.log("validate.validators.checked '%s' '%s'", value, options);

        if ( value === null ) {
          return options.message
        } else {
          return null;
        }
      };

      // postcode
      validate.validators.postcode = function(value, options, key, attributes) {
        var valid = true;
        var blankRegEx = new RegExp('^\\s*$', '');

        // var postcodeRegex = new RegExp('^(([gG][iI][rR] {0,}0[aA]{2})|((([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y]?[0-9][0-9]?)|(([a-pr-uwyzA-PR-UWYZ][0-9][a-hjkstuwA-HJKSTUW])|([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y][0-9][abehmnprv-yABEHMNPRV-Y]))) {0,}[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}))$', '');
        var postcodeRegex = new RegExp('^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$', '');

        if ( value === null ) {
          valid = false;
        } else {
          if ( blankRegEx.test(value) ) {
            valid = false;
          } else {
            if ( !postcodeRegex.test(value) ) {
              valid = false;
            }
          }
        }

        if ( valid ){
          return null;
        } else {
          return options.message;
        }
      }

      // telephone number
      validate.validators.telephone = function(value, options, key, attributes) {
        var valid = true;
        var blankRegEx = new RegExp('^\\s*$', '');

        var phoneRegex = new RegExp('^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\\s\\./0-9]*$', 'g');
        /* valid telephone number formats based in regexp
        329923492
        +(9) 999 999
        +(99)-999-999
        +(999).999.999
        (999).999.999
        999 999 999
        999-999-999
        999.999.999
        */

        if ( value === null ) {
          valid = false;
        } else {
          if ( blankRegEx.test(value) ) {
            valid = false;
          } else {
            if ( !phoneRegex.test(value) ) {
              valid = false;
            }
          }
        }

        if ( valid ){
          return null;
        } else {
          return options.message;
        }

      };

      /*
      google recaptcha v2 "I'm not a robot" checkbox
      https://developers.google.com/recaptcha/docs/display
      */
      validate.validators.recaptchav2 = function(value, options, key, attributes) {
        // client-side validation for convenience only, does not actually verify that the form has been submitted by a human
        // NEEDS to be verified on the server
        // https://developers.google.com/recaptcha/docs/verify

        if ( value !== null) {
          return null;
        } else {
          return options.message;
        }
      };
      /* */

      if ( this.form !== null) {

        // will probably need to modify this to include other field types at some point
        /**/
        this.inputs = this.form.querySelectorAll("input[type='text'],input[type='email'],select,textarea,input[type='checkbox'],input[type='radio']");
        /**/

        if (!this.placholderSupported) {
          this.setPlaceholders();
        }

        // bind validation to form submit
        this.form.addEventListener("submit", function(ev) {

          ev.preventDefault(ev);
          var tgt = ev.target || ev.srcElement;

          that.inputs = that.form.querySelectorAll("input[type='text'],input[type='email'],select,textarea,input[type='checkbox'],input[type='radio']");
          // go through form inputs
          // clear inputs where value = placeholder
          // var pfields = that.form.querySelectorAll("input[type='text'], textarea"),
          //     pfield;
          // for (var i = 0; i < pfields.length; ++i) {
          //   pfield = pfields.item(i);
          //   if (pfield.getAttribute('placeholder') !== null && pfield.getAttribute('placeholder') !=='' && pfield.getAttribute('placeholder') === pfield.value) {
          //     pfield.value = '';
          //   }
          // }

          // do standard validate.js form validation
          var errors = validate( tgt, that.constraints );

          // add in validation process for checkboxes with hidden fields
          if ( that.xcheckboxesProcess ) {
            errors = that.processCheckboxFields(errors);
          }

          // add in validation process for client yes/no field
          if ( that.clientProcess ) {
            errors = that.processClient(errors);
          }

          // validation: passed or failed?
          if (typeof errors !== 'undefined') {
            // failed
            that.showErrors(errors);
              $('#formerror').val("true");
          } else {
            // passed
            that.clearErrors();
            that.validform(that.form);
              $('#formerror').val("false");
            // google recaptcha v2 invisible
            // check captcha after all form fields validate
            // grecaptcha.execute();
          }
        });

        // bind validation to input field blur/change events
        for (var i = 0; i < this.inputs.length; ++i) {
          this.inputs.item(i).addEventListener("focus", function(ev) {
            if ( !this.placholderSupported ) {
              that.clearPlaceholder(this);
            }
          });

          this.inputs.item(i).addEventListener("blur", function(ev) {
            that.fieldValidate(this);
            // that.setPlaceholder(this);
          });

          this.inputs.item(i).addEventListener("change", function(ev) {
            //that.fieldValidate(this);
          });
        }

      }

    },

    "removeCheckboxFields" : function () {
      this.deleteFields = [];
      var n = Object.entries(this.constraints), i = 0; l = n.length;
      do {
        if ( n[i][1].hasOwnProperty('checked') ) {
          this.deleteFields.push({
            "field" : n[i][0],
            "required" : n[i][1]['checked']['required'],
            "message" : n[i][1]['checked']['message']
          });
        }
        i++;
      } while (i < l)

      // remove checkbox items  from normal validation system
      this.deleteFields.map(function(item) {
        delete this.constraints[item['field']];
      },this);
    },

    "processCheckboxFields" : function (errors) {
      if (this.xcheckboxes.length > 0) {

        var errorList = [];
        this.xcheckboxes.map(function(xcheckbox,index) {
          // get checkbox input element
          var xcb = this.form.querySelectorAll("input[type='checkbox'][name='" + xcheckbox['name'] + "']")[0];
          // get hidden input element
          var xch = this.form.querySelectorAll("input[type='hidden'][name='" + xcheckbox['name'] + "']")[0];

          /*
          ------------------------------------------
          simple single checkbox required validation
          ------------------------------------------
          */
          if ( xcb.checked ) {
            // set hidden field value to value of checkbox 
            xch.value = xcb.value;
          } else {
            // set hidden field value to 'false' 
            xch.value = 'false';
            // add checkbox error message to errors
            errorList.push(xcheckbox);
          }
        },this);
      }

      if ( errorList.length > 0 ) {
        // create errors object if it doesn't already exist
        if ( typeof errors === 'undefined' ) {
          errors = {};  
        }

        errorList.map(function(error) {
          errors[error['name']] = error['message']; 
        }, this);

      }
      return errors;
    },

    // validate 'are you a client: yes/no' radio field
    "processClient" : function (errors) {
      var errorList = [],
          choices = 0;

      // count the number of selected buttons
      this.clientField.choices.map(function(choice,index) {
        var radio = this.form.querySelectorAll("input[type='radio']#" + choice)[0];
        if ( radio.checked ) 
          choices++;
      }, this);      

      // allow only one button to be selected
      if( choices !== 1 ) {
        errorList.push( {
          "name" : this.clientField.name,
          "message" : this.clientField.message
        } );        
      }

      // field is invalid, return errors
      if ( errorList.length > 0 ) {
        // create errors object if it doesn't already exist
        if ( typeof errors === 'undefined' ) {
          errors = {};  
        }

        errorList.map(function(error) {
          errors[error['name']] = error['message']; 
        }, this);

      }
      return errors;
    },

    "setPlaceholders" : function () {
        this.pfields = this.form.querySelectorAll("input[type='text'], input[type='email'], textarea");
        var thispf,pfPlaceholder;
        for (var i = 0; i < this.pfields.length; ++i) {
          thispf = this.pfields.item(i);
          pfPlaceholder = thispf.getAttribute('placeholder');
          thispf.value = pfPlaceholder;
          thispf.classList.add('usingPlaceholder');
        }
    },

    "clearPlaceholder" : function (input) {
      // clear value
      if (input.value === input.getAttribute('placeholder')) {
        input.value = '';
      }
    },

    "setPlaceholder" : function (input) {
      // clear value
      if (input.value === '') {
        input.value = input.getAttribute('placeholder');
      }
    },

    "fieldValidate" : function (input) {
      // check to see if placeholders supported
      if ( !this.placholderSupported ) {
        // check to see if field value matches placeholder
        if (input.value === input.getAttribute('placeholder')) {
        }
      }

      var errors = validate(this.form, this.constraints) || {};

      if ( (typeof errors !== 'undefined' && typeof errors[input.name] !== 'undefined') && input.value !== input.getAttribute('placeholder')) {
        this.showErrorsForInput(input, errors[input.name]);
      } else {
        this.showErrorsForInput(input, '');
      }

      // reset placeholder if no value has been set
      if ( !this.placholderSupported ) {
        this.setPlaceholder(input);
      }
    },

    "showErrors" : function (errors) {

        var entries = this.inputs,
            entrycount = entries.length;
        // for(var entry of this.inputs.entries()) { 
          // var field = entry[1], fieldName = field.name, fieldType = field.type;
        for(entryIndex = 0; entryIndex < entrycount; entryIndex++ ) { 
          var entry = entries[entryIndex];
          var field = entry, fieldName = field.name, fieldType = field.type;

          // if ( fieldName = field.name) {} else {}
          if (typeof errors !== 'undefined' && typeof errors[fieldName] !== 'undefined' ) {
            this.showErrorsForInput(field, fieldName, errors[fieldName]);
          } else {
            this.showErrorsForInput(field, fieldName, '');
          }
         
        }

        // google recaptcha v2 - reset recaptcha if other form fields are invalid
        // grecaptcha.reset();
    },

    "showErrorsForInput" : function ( input, name, error ) {

        // find input container .form-group
        if ( typeof error !== 'undefined') {

          var pnode = input;

          switch( name ){
              case 'client':
              case 'clientOverlay':
              {
              // get fieldset parent for radio button group
              do {
                pnode = pnode.parentNode;
              } while ( !pnode.classList.contains('clientYesNo') )
              formGroup = pnode;

            } break;
            case 'clientstatus' : {
              // skip over hidden field
              formGroup = false;
            } break;
            default : {
              // default, find input field's parent .form-group div
              do {
                pnode = pnode.parentNode;
              } while ( typeof pnode.classList !== 'undefined' && !pnode.classList.contains('form-group') )
              formGroup = pnode;
            }
          }

          if( formGroup ) {
            var messages = formGroup.querySelector(".messages");
            // check to see if the field has 'messages' element
            if ( messages !== null ) {
              if ( error === '' & typeof error !== 'undefined' ) {
                this.resetFormGroup(formGroup,messages);
              } else {
                formGroup.classList.add("has-error");
                messages.innerText = error;
              }
            }
          }

        }
    },

    "clearErrors" : function () {
        var entries = this.inputs,
            entrycount = entries.length;

        for(entryIndex = 0; entryIndex < entrycount; entryIndex++ ) { 
          var entry = entries[entryIndex];
          var field = entry, fieldName = field.name, fieldType = field.type;
          this.showErrorsForInput(field, fieldName, '');
        }

        /*
        not compatible with IE 9 Array.Iterator not supported
        for(var entry of this.inputs.entries()) { 
          var field = entry[1], fieldName = field.name, fieldType = field.type
          this.showErrorsForInput(field, fieldName, '');
        }
        */
    },


    "resetFormGroup" : function ( formGroup, messages ) {

        formGroup.classList.remove("has-error");
        messages.innerText = '';
    },

    "closestParent" : function ( child, className ) {
      var that = this;

        if (!child || child == document) {
          return null;
        }
        if (child.classList.contains(className)) {
          return child;
        } else {
          return this.closestParent(child.parentNode, className);
        }
    },

    "doCaptcha" : function ( response ) {
    }

      /* ---------------------------------------------------------------------------------------------------------------- */

  }
  window.BDFormValidate = BDFormValidate;
  // export default BDFormValidate;
/*
------------------------------------------------
BDFormValidate
END
------------------------------------------------
*/


var menuBtn = document.querySelector(".btn-menu");
    body = document.getElementsByTagName("BODY")[0],
    menuPrimaryParent = document.querySelectorAll(".has__secondary__menu"),
    menuPrimaryLinks = document.querySelectorAll(".has__secondary__menu > a"),
    menuSecondaryParent = document.querySelectorAll(".has__tertiary__menu"),
    menuSecondaryLinks = document.querySelectorAll(".has__tertiary__menu > a"),
    categoryLink = document.querySelectorAll(".masthead-category--selected > a")[0],
    categoryList = document.querySelectorAll(".masthead-categories-sub")[0],
    openSubmenu = -1,
    mobileEnabled = false,
    mobileBreakpoint = 992,
    w1 = -1,w2 = -1,
    categoryListTimer = null,
    clShow = false,
    placholderSupported = (typeof document.createElement("input").placeholder === 'string');

/*
TEST CODE ONLY - START
*/
// cs=(individual|professional|charity|financial)
var address = window.location.href,
    colourschemeRegex = new RegExp('cs=(individual|professional|charity|financial)', '');

var result = colourschemeRegex.exec(address);
if(result != null){
  body.classList.remove("cs_individual","cs_professional","cs_financial","cs_charity");
  body.classList.add("cs_" + result[1]);
}
/*
TEST CODE ONLY - END
*/

/*
handle background-attachemnt:fixed alternate in iOS
*/
var isIOS = (navigator.userAgent.match(/(iPad|iPhone|iPod)/g)) ? true : false,
    iosTestRegex = new RegExp('ios=(on|off)', ''); // test mode, add '?ios=on' to end of URL
var result = iosTestRegex.exec(address);
if(result != null){
    isIOS = (result[1] === 'on');
}
body.classList.add( (isIOS) ? 'ios' : 'notios');

/*
--------------------------------------------------------------------------------
menu handler - START
--------------------------------------------------------------------------------
*/

// navigation menu click event handlers for mobile
function menuOpenHandler(e) {
  if ( mobileEnabled ) {
    if (body.classList.contains('menu__open')) {
      body.classList.remove("menu__open");
    } else {
      body.classList.add("menu__open");
    }
  }
};

function secondaryMenuOpenHandler(e) {

  if ( mobileEnabled ) {
    e.preventDefault();
    var linkItem = e.target,
        linkIndex = linkItem.getAttribute('data-submenu'),
        linkParent = linkItem.parentElement;

    // ** single item open at a time
    // click on item to open it
    // click on same item to close it
    // while an item is open, clicking on another item will close the open item and open the clicked item

    if (openSubmenu === linkIndex) {
      // clicked on open item, close it
      menuPrimaryLinks[linkIndex].parentElement.classList.remove('menu__secondary__open');
      openSubmenu = -1;
    } else {
      if (openSubmenu > -1) {
        // open menu
        // clicked on different item, close open item
        menuPrimaryLinks[openSubmenu].parentElement.classList.remove('menu__secondary__open');
      }
      // open clicked item
      linkParent.classList.add('menu__secondary__open');
      openSubmenu = linkIndex;
    }
  }
}


function tertiaryMenuOpenHandler(e) {

  if ( mobileEnabled ) {
    e.preventDefault();
    var linkItem = e.target,
        linkIndex = linkItem.getAttribute('data-submenu'),
        linkParent = linkItem.parentElement;

    // ** single item open at a time
    // click on item to open it
    // click on same item to close it
    // while an item is open, clicking on another item will close the open item and open the clicked item

    if (openSubmenu === linkIndex) {
      // clicked on open item, close it
      menuSecondaryLinks[linkIndex].parentElement.classList.remove('menu__tertiary__open');
      openSubmenu = -1;
    } else {
      if (openSubmenu > -1) {
        // open menu
        // clicked on different item, close open item
        menuSecondaryLinks[openSubmenu].parentElement.classList.remove('menu__tertiary__open');
      }
      // open clicked item
      linkParent.classList.add('menu__tertiary__open');
      openSubmenu = linkIndex;
    }
  }
}

function categoryListHandler(e) {
  if ( mobileEnabled ) {
    if(categoryList.classList.contains('masthead-categories-sub--visible')) {
      categoryList.classList.remove('masthead-categories-sub--visible');
    } else {
      categoryList.classList.add('masthead-categories-sub--visible');
    }
  }
}

function categoryListHover(e) {
  if ( !mobileEnabled ) {
    // categoryList = 'ul.masthead-categories-sub'
    // show menu
    categoryList.classList.add('masthead-categories-sub--visible');

    categoryList.addEventListener('mouseenter', function(e) {
      categoryListTimer = null;
      // if ( categoryListTimer !== null ) { categoryListTimer.clearTimeout(); }
    });

    // add mouseout event to menu
    categoryList.addEventListener('mouseleave', function(e) {
      // start timer
      categoryListTimer = window.setTimeout(function() {
        categoryListTimer = null;
        if ( !mobileEnabled ) {
          categoryList.classList.remove('masthead-categories-sub--visible');
        }
      },100);
    }, false);
  }
}

// add click(touch) event handlers for menus
// check for menu button in HTML
if ( menuBtn !== null ) {
  mobileEnabled = (window.innerWidth < mobileBreakpoint);
  w1 = window.innerWidth;

  // mobile 'burger' button
  if (menuBtn !== null) {
    // menuBtn.addEventListener('click', menuOpenHandler, false);
    menuBtn.addEventListener('click', function(e) { e.preventDefault(); menuOpenHandler(); }, false);
  }

  if (typeof categoryLink !== 'undefined') {
    categoryLink.addEventListener('click', function(e) { e.preventDefault(); categoryListHandler(); }, false);
    categoryLink.addEventListener('mouseover', function(e) { e.preventDefault(); categoryListHover(); }, false);
  }

  if (menuPrimaryLinks.length > 0) {
    // add accordion behaviour to secondary level navigation in mobile menu
    var li = 0,lc = menuPrimaryLinks.length;
    do {
      menuPrimaryLinks[li].setAttribute('data-submenu',li);
      menuPrimaryLinks[li].addEventListener('click', function(e) { e.preventDefault(); secondaryMenuOpenHandler(e); }, false);
      li++;
    } while (li < lc)
  }

  if (menuSecondaryLinks.length > 0) {
    // add accordion behaviour to tertiary level navigation in mobile menu
    var li = 0,lc = menuSecondaryLinks.length;
    do {
      menuSecondaryLinks[li].setAttribute('data-submenu',li);
      menuSecondaryLinks[li].addEventListener('click', function(e) { e.preventDefault(); tertiaryMenuOpenHandler(e); }, false);
      li++;
    } while (li < lc)
  }

  /*
  if(menuPrimaryParent.length > 0){
    var li = 0, lc = menuPrimaryParent.length;
    do {
      //copy the parent item into child list so it can function as a link and not just toggle visibility of child list

      //link href
      var liHref = menuPrimaryParent[li].querySelector("li > a").getAttribute("href");

      //link text
      var liText = menuPrimaryParent[li].querySelector("li > a").text;

      //child ul
      var childUl = menuPrimaryParent[li].querySelector("li > ul");

      //create new li element
      var newLi = document.createElement("li");
      var newHref = document.createElement("a");
      newLi.className = "navigation__additional__mob";
      newHref.setAttribute("href", liHref);
      newHref.text = liText;

      newLi.appendChild(newHref);

      childUl.insertBefore(newLi, childUl.childNodes[0]);

      li++;
    } while (li < lc)
  }
  */

  if(menuSecondaryParent.length > 0){
    var li = 0, lc = menuSecondaryParent.length;
    do {
      //copy the parent item into child list so it can function as a link and not just toggle visibility of child list

      //link href
      var liHref = menuSecondaryParent[li].querySelector("li > a").getAttribute("href");

      //link text
      var liText = menuSecondaryParent[li].querySelector("li > a").text;

      //child ul
      var childUl = menuSecondaryParent[li].querySelector("li > ul");

      //create new li element
      var newLi = document.createElement("li");
      var newHref = document.createElement("a");
      newLi.className = "navigation__additional__mob";
      newHref.setAttribute("href", liHref);
      newHref.text = liText;
      newLi.appendChild(newHref);

      childUl.insertBefore(newLi, childUl.childNodes[0]);

      li++;
    } while (li < lc)
  }

  window.addEventListener('resize', function(e) {
    var wx = window.innerWidth;
    mobileEnabled = (wx < mobileBreakpoint);

    // find transition FROM mobile to desktop, remove mobile active styles
    if (wx < mobileBreakpoint) { w1 = wx; }

    if (wx > mobileBreakpoint) { w2 = wx; }

    if ( !mobileEnabled && (w1 < mobileBreakpoint) && (w2 > mobileBreakpoint) ) {
      w1 = w2;
      // clear any open menu states
      categoryList.classList.remove('masthead-categories-sub--visible');
      body.classList.remove("menu__open");
    }
  }, false);

}

/*
--------------------------------------------------------------------------------
menu handler - END
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
Expandable 'Find out More' Section controls
START
--------------------------------------------------------------------------------
*/
// for forms with radio button groups reset all redio buttons
var btnExpand = document.querySelectorAll('.find-more__header .button')[0],
  panelExpanded = document.querySelectorAll('.find-more__expanded')[0],
  btnClose = document.querySelectorAll('.find-more__close__button')[0];

if (typeof btnClose != "undefined") {
  btnExpandContainer = btnExpand.parentElement;
}

if (typeof btnExpand != 'undefined') {

  btnExpand.addEventListener("click", function(e) {
    if (!btnExpandContainer.classList.contains('button--hidden')) {
      btnExpandContainer.classList.add('button--hidden');
      panelExpanded.classList.add('find-more__expanded--visible');
    }

    // for forms with radio button groups reset all redio buttons    
    // .clientYesNo > input[type="radio"]
    var radioButtons = document.querySelectorAll('.clientYesNo input');
    var rbIndex = 0,
        rbCount = radioButtons.length,
        thisRb;

    if ( rbCount > 0 ) {
      do {
        thisRb = radioButtons[rbIndex];

        thisRb.checked = false;

        rbIndex++;
      } while ( rbIndex < rbCount)
    }
    
  });

  btnClose.addEventListener("click", function(e) {
    e.preventDefault();
    panelExpanded.classList.remove('find-more__expanded--visible');
    btnExpandContainer.classList.remove('button--hidden');
  });
}
/*
--------------------------------------------------------------------------------
Expandable 'Find out More' Section controls
END
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
bps2 new typoe of expander element
  .bp2_expander1
  .bp2_expander1 .wrapper
  .expanderButton
  .collapseButton
START
--------------------------------------------------------------------------------
*/
var btnExpand = document.querySelectorAll('.bp2_expander1');

if(btnExpand.length > 0) {
  var index = 0, expanderItem, closer, opener;
  do {
    expanderItem = btnExpand[index];

       if (expanderItem != undefined) {

    opener = expanderItem.querySelectorAll('.expanderButton')[0];
    closer = expanderItem.querySelectorAll('.collapseButton')[0];

    opener.addEventListener("click", function(e) {
      e.preventDefault();
      var btnExpandContainer = e.target.parentElement;

      if ( !btnExpandContainer.classList.contains('bp2_expander1') ) {
        do {
          btnExpandContainer = btnExpandContainer.parentElement;
        } while ( !btnExpandContainer.classList.contains('bp2_expander1') )
      }

      btnExpandContainer.classList.add('expanded-visible');
    });

    closer.addEventListener("click", function(e) {
      e.preventDefault();
      var btnExpandContainer = e.target.parentElement;

      if ( !btnExpandContainer.classList.contains('bp2_expander1') ) {
        do {
          btnExpandContainer = btnExpandContainer.parentElement;
        } while ( !btnExpandContainer.classList.contains('bp2_expander1') )
      }

      btnExpandContainer.classList.remove('expanded-visible');
    });

       }

    index++;
  } while (index < btnExpand.length)
}

/*
--------------------------------------------------------------------------------
bps2 new typoe of expander element
END
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
Slider code
START
--------------------------------------------------------------------------------
*/
document.addEventListener('DOMContentLoaded', function() {
  var simple_dots = document.querySelectorAll('.js__pagination');

  if(simple_dots.length) {

    var dot_count = simple_dots[0].querySelectorAll('.js__slide').length;
    var dot_container = simple_dots[0].querySelector('.js_dots');
    var dot_list_item = document.createElement('li');
    function handleDotEvent(e) {
      if (e.type === 'before.lory.init') {
        for (var i = 0, len = dot_count; i < len; i++) {
          var clone = dot_list_item.cloneNode();
          dot_container.appendChild(clone);
        }
        dot_container.childNodes[0].classList.add('active');
      }
      if (e.type === 'after.lory.init') {
        for (var i = 0, len = dot_count; i < len; i++) {
          dot_container.childNodes[i].addEventListener('click', function(e) {
            dot_navigation_slider.slideTo(Array.prototype.indexOf.call(dot_container.childNodes, e.target));
          });
        }
      }
      if (e.type === 'after.lory.slide') {
        for (var i = 0, len = dot_container.childNodes.length; i < len; i++) {
          dot_container.childNodes[i].classList.remove('active');
        }
        dot_container.childNodes[e.detail.currentSlide - 1].classList.add('active');
      }
      if (e.type === 'on.lory.resize') {
        for (var i = 0, len = dot_container.childNodes.length; i < len; i++) {
          dot_container.childNodes[i].classList.remove('active');
        }
        dot_container.childNodes[0].classList.add('active');
      }
    }
    simple_dots[0].addEventListener('before.lory.init', handleDotEvent);
    simple_dots[0].addEventListener('after.lory.init', handleDotEvent);
    simple_dots[0].addEventListener('after.lory.slide', handleDotEvent);
    simple_dots[0].addEventListener('on.lory.resize', handleDotEvent);

    var dot_navigation_slider = lory(simple_dots[0], {
      infinite: 1,
      enableMouseEvents: true
    });
  }
});

tickboxes = document.querySelectorAll(".form-xcheck");
/*
--------------------------------------------------------------------------------
Slider code
END
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
form validation
START
--------------------------------------------------------------------------------
*/
var btnSubmit = document.querySelectorAll('.form__section--submit .button')[0],
  formSections = document.querySelectorAll('.form__body'),
  formSubmitted = document.querySelectorAll('.form__submitted')[0];
window.addEventListener('load', function() {

  // https://validatejs.org/, http://validatejs.org/examples.html
  // supports IE 9

  // prevent attribute 'name' prepend to error messages
  validate.options = { "fullMessages" : false };

  // request a callback
  // Inline / expander version
  if (document.querySelectorAll('form#callbackForm').length > 0) {

    console.log('inline callback form');

    var callbackFormValidator = new BDFormValidate({
      "formSelector" : "form#callbackForm",
      "constraints" : {
      "Firstname" : {
          "presence" : {message: "Please supply your forename"},
          "format" : {
            "pattern" : /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
            "message" : "Please supply a valid forename"
          }
        },
      "Lastname" : {
          "presence" : {message: "Please supply your surname"},
          "format" : {
            "pattern" : /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
              "message": "Please supply a valid surname"
          }
        },
      "Email" : {
          "presence" : {message: "Please supply an email address"},
          "format" : {
            "pattern": /^[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/,
            "message" : "Please supply a valid email address"
          }
        },
      "Phone" : {
          "presence" : {message: "Please supply a contact number"},
          "format" : {
            "pattern" : /^[+]*[(]{0,1}[0-9 ]{1,3}[)]{0,1}[-\\s\\./0-9 x]{6,20}$/,
              "message": "Please supply a valid contact number"
          }
        },
      "PostalCode": {
          "presence" : {message: "Please enter a location"},
          "format" : {
            "pattern" : /^[a-zA-Z0-9 ]{1,10}$/,
            "message" : "Please supply a valid postcode"
          }
        },
        //"MeetingReason": {
        //    "presence": { message: "Please specify a reason for a callback" }
        //},        
        "g-recaptcha-response": {
          "recaptchav2" : {message: "Please prove you are human"}
        }
        },
        "clientField": {
            "name": "client",
            "choices": ['clientyes', 'clientno'],
            "required": true,
            "message": "Please state if you are a client"
        },
      "validform" : function(form) {
        console.log('go');

        // switch to thanks you / confirm page in overlay
        if ( $('#req').length > 0 ) {
          $('#req').addClass('showEnd');
        }
        
        if ( $('#emailform').length > 0 ) {
          $('#emailform').addClass('showEnd');
        }

          $('.inlineForm').addClass('showEnd');

        CallbackFormSubmitEvent('form_submit',
            $('#marketoformid').val(),
            $('#MeetingPreferredDay option:selected').text(),
            $('#MeetingPreferredTime option:selected').text(),
            $('#MeetingPreferredOffice option:selected').text(),
            $('#LevelOfWealth option:selected').text());

        // form.submit();
      }
    });
    window.callbackFormValidator = callbackFormValidator;

    $(document).ready(function(){
        console.log('inline callback form -> document.ready');

        // reset client radio buttons
        $('.clientYesNo input').each(function(i) {
            $(this).attr({"checked" : false});
        });

        // attach DOM_Mutator observer to div#divreqcallback
        // https://stackoverflow.com/questions/2844565/is-there-a-javascript-jquery-dom-change-listener
        // https://dom.spec.whatwg.org/#interface-mutationobserver
        MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

        var observer = new MutationObserver(function(mutations, observer) {
            // fired when a mutation occurs
            console.log(mutations, observer);

            $('.clientYesNo input').each(function(i) {
                $(this).attr({"checked" : false});
            });

            /*
            $('form#callbackForm').find('.has-error').each(function (i) {
              $(this).removeClass('has-error');
              $(this).find('.messages').empty();
            });
            */
            // ...
        });

        // define what element should be observed by the observer
        // and what types of mutations trigger the callback
        observer.observe(document.querySelectorAll('div#divreqcallback')[0], {
            "childList" : true,
            "subtree": true
        });        

    });
  }

    // global CTA request a callback form overlay
    if (document.querySelectorAll('form#CTAcallbackForm').length > 0) {

        var CTAcallbackFormValidator = new BDFormValidate({
            "formSelector": "form#CTAcallbackForm",
            "constraints": {
                "FirstnameOverlay": {
                    "presence": { message: "Please supply your forename" },
                    "format": {
                        "pattern": /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
                        "message": "Please supply a valid forename"
                    }
                },
                "LastnameOverlay": {
                    "presence": { message: "Please supply your surname" },
                    "format": {
                        "pattern": /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
                        "message": "Please supply a valid surname"
                    }
                },
                "EmailOverlay": {
                    "presence": { message: "Please supply an email address" },
                    "format": {
                        "pattern": /^[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/,
                        "message": "Please supply a valid email address"
                    }
                },
                "PhoneOverlay": {
                    "presence": { message: "Please supply a contact number" },
                    "format": {
                        "pattern": /^[+]*[(]{0,1}[0-9 ]{1,3}[)]{0,1}[-\\s\\./0-9 x]{6,20}$/,
                        "message": "Please supply a valid contact number"
                    }
                },
                "PostalCodeOverlay": {
                    "presence": { message: "Please enter a location" },
                    "format": {
                        "pattern": /^[a-zA-Z0-9 ]{1,10}$/,
                        "message": "Please supply a valid postcode"
                    }
                },
                //"MeetingReason": {
                //    "presence": { message: "Please specify a reason for a callback" }
                //},        
                "g-recaptcha-response": {
                    "recaptchav2": { message: "Please prove you are human" }
                }
            },
            "clientField": {
                "name": "clientoverlay",
                "choices": ['clientyesoverlay', 'clientnooverlay'],
                "required": true,
                "message": "Please state if you are a client"
            },
            "validform": function (form) {
                console.log('go');

                // switch to thanks you / confirm page in overlay
                if ($('#req').length > 0) {
                    $('#req').addClass('showEnd');
                }

                if ($('#emailform').length > 0) {
                    $('#emailform').addClass('showEnd');
                }

                CallbackFormSubmitEvent('form_submit',
                    $('#marketoformid').val(),
                    $('#MeetingPreferredDay option:selected').text(),
                    $('#MeetingPreferredTime option:selected').text(),
                    $('#MeetingPreferredOffice option:selected').text(),
                    $('#LevelOfWealth option:selected').text());

                // form.submit();
            }
        });
        window.CTAcallbackFormValidator = CTAcallbackFormValidator;

        $(document).ready(function(){
            // reset client radio buttons
            $('.clientYesNo input').each(function (i) {
                $(this).attr({ "checked": false });
            });
        });
    }

  /*
  #callbackForm-nooffice
  */

  // subscrive to newsletter 
  // global CTA module
  if (document.querySelectorAll('form#susbcribe').length > 0) {
    
    console.log('susbcribe');

    var dwfrFormValidator = new BDFormValidate({
      "formSelector" : "form#susbcribe",
      "constraints" : {
        "Firstname" : {
          "presence" : {message: "Please supply your forename"},
          "format" : {
            "pattern" : /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
              "message": "Please supply a valid forename"
          }
        },
        "Lastname" : {
          "presence" : {message: "Please supply your surname"},
          "format" : {
            "pattern" : /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
              "message": "Please supply a valid surname"
          }
        },
        "Email" : {
          "presence" : {message: "Please supply an email address"},
          "format" : {
            "pattern": /^[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/,
            "message" : "Please supply a valid email address"
          }
        },
        "PostalCode" : {
          "presence" : {message: "Please enter a location"},
          "format" : {
            "pattern" : /^[a-zA-Z0-9 ]{1,10}$/,
            "message" : "Please supply a valid postcode"
          }
        },
        "g-recaptcha-response": {
          "recaptchav2" : {message: "Please prove you are not a robot"}
        }
      },
      "xcheckboxes" : [
        {
          "name" : "MarketingCommsOptIn",
          "required" : true,
          "message" : "Please confirm you are willing to receive emails from Brewin Dolphin"
        }
      ],
      "validform" : function(form) {
        console.log('go');
        // form.submit();

        // show inline form 'thanks' page
        $('.inlineForm').addClass('showEnd');

      }
    });
    window.dwfrFormValidator = dwfrFormValidator;
    }

    if (document.querySelectorAll('form#CTAsusbcribe').length > 0) {

        console.log('susbcribe');

        var dwfrFormValidator = new BDFormValidate({
            "formSelector": "form#CTAsusbcribe",
            "constraints": {
                "FirstnameOverlay": {
                    "presence": { message: "Please supply your forename" },
                    "format": {
                        "pattern": /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
                        "message": "Please supply a valid forename"
                    }
                },
                "LastnameOverlay": {
                    "presence": { message: "Please supply your surname" },
                    "format": {
                        "pattern": /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
                        "message": "Please supply a valid surname"
                    }
                },
                "EmailOverlay": {
                    "presence": { message: "Please supply an email address" },
                    "format": {
                        "pattern": /^[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/,
                        "message": "Please supply a valid email address"
                    }
                },
                "PostalCodeOverlay": {
                    "presence": { message: "Please enter a location" },
                    "format": {
                        "pattern": /^[a-zA-Z0-9 ]{1,10}$/,
                        "message": "Please supply a valid postcode"
                    }
                },
                "g-recaptcha-response": {
                    "recaptchav2": { message: "Please prove you are not a robot" }
                }
            },
            "xcheckboxes": [
                {
                    "name": "MarketingCommsOptInOverlay",
                    "required": true,
                    "message": "Please confirm you are willing to receive emails from Brewin Dolphin"
                }
            ],
            "validform": function (form) {
                console.log('go');
                // form.submit();

                // show overlay form 'thanks' page
                $('#news').addClass('showEnd');

            }
        });
        window.dwfrFormValidator = dwfrFormValidator;
    }


  // find-more-expandable office finder
  // <form data-validate validate="false" id="locationSearch2Form" data-target="../templates/office-finder.html" novalidate>
  /*
  ** REDIRECTS to office finder page
  */
  var officeFinderFormValidator = new BDFormValidate({
    "formSelector" : "form#locationSearch2Form",
    "constraints" : {
      "locationsearch2": {
        "presence" : {message: "Please enter a location"}
      }
    },
    "validform" : function(form) {
      //console.log('go');
      var redirect = $('#officepageurl').val();
      var postcode = $('#locationsearch2').val();
      if (postcode != undefined && postcode != "") {
          redirect = redirect + "?postcode=" + postcode;
      } else {
          if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(function (position) {
                  redirect = redirect + "?long=" + position.coords.longitude + "&lat=" + position.coords.latitude;
              }, function () {
                  //handleLocationError(true, infoWindow, map.getCenter());
              });
          } else {
              // Browser doesn't support Geolocation
              //handleLocationError(false, infoWindow, map.getCenter());
          }
      }
      // form.submit();
      //if ( form.dataset ) {
      //  console.log( form.dataset.target );
      //  redirect = form.dataset.target;
      //} else {
      //  console.log( form.getAttribute('data-target') );
      //  redirect = form.dataset.target;
      //}
      window.location.href = redirect;
    }
  });

   //var btnGeolocateoffblock = document.getElementById("locationgeolocateoffblock");

   // if (btnGeolocateoffblock != null) {
   //     // Try HTML5 geolocation.
   //     btnGeolocateoffblock.addEventListener("click",
   //         function() {
   //             var redirect = $('#officepageurl').val();
   //             if (navigator.geolocation) {
   //                 navigator.geolocation.getCurrentPosition(function(position) {
   //                         redirect = redirect +
   //                             "?long=" +
   //                             position.coords.longitude +
   //                             "&lat=" +
   //                             position.coords.latitude;
   //                         window.location.href = redirect;
   //                     },
   //                     function() {
   //                         //handleLocationError(true, infoWindow, map.getCenter());
   //                         window.location.href = redirect;
   //                     });
   //             } else {
   //                 // Browser doesn't support Geolocation
   //                 //handleLocationError(false, infoWindow, map.getCenter());
   //                 window.location.href = redirect;
   //             }
   //         });
   // }

    // form-request-meeting
    var officeFinderFormValidator = new BDFormValidate({
        "formSelector": "form#requestmeeting",
        "constraints": {
            "Firstname": {
                "presence": { "message": "Please supply your forename" },
                "format": {
                    "pattern": /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
                    "message": "Please supply a valid forename"
                }
            },
            "Lastname": {
                "presence": { message: "Please supply your surname" },
                "format": {
                    "pattern": /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
                    "message": "Please supply a valid surname"
                }
            },
            "PostalCode": {
                "presence": { message: "Please enter a location" },
                "format": {
                    "pattern": /^[a-zA-Z0-9 ]{1,10}$/,
                    "message": "Please supply a valid postcode"
                }
            },
            "Phone": {
                "presence": { message: "Please supply a contact number" },
                "format": {
                    "pattern": /^[+]*[(]{0,1}[0-9 ]{1,3}[)]{0,1}[-\\s\\./0-9 x]{6,20}$/,
                    "message": "Please supply a valid contact number"
                }
            },
            "Email": {
                "presence": { message: "Please supply an email address" },
                "format": {
                    "pattern": /^[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/,
                    "message": "Please supply a valid email address"
                }

            }
        },
        "validform": function(form) {
            //console.log('go');
            form.submit();
        }
    });

  // feedback form #feedbackform
  var feedbackFormValidator = new BDFormValidate({
    "formSelector" : "form#feedbackform",
    "constraints" : {
      //"contact-reason" : {
      //  "presence" : {message: "Please supply a reason"}
      //},
      "Firstname" : {
          "presence": { message: "Please supply your forename" },
          "format": {
              "pattern": /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
              "message": "Please supply a valid forename"
          }
      },
      "Lastname" : {
          "presence": { message: "Please supply your surname" },
          "format": {
              "pattern": /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
              "message": "Please supply a valid surname"
          }
      },
      "Email" : {
          "presence": { message: "Please supply an email address" },
          "format": {
              "pattern": /^[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/,
              "message": "Please supply a valid email address"
          }
        },
        "g-recaptcha-response": {
            "recaptchav2": { message: "Please prove you are not a robot" }
        }
    },
    "validform" : function(form) {
      //console.log('go');
      form.submit();
    }
  });

// BPS landing page version 2 sprint 2 august 2018
// /templates/lp-bps2.html
//console.log('bps2Form');
var callbackFormValidator = new BDFormValidate({
    "formSelector": "form#bps2Form",
    "constraints": {
        "Firstname": {
            "presence": { message: "Please supply your forename" },
            "format": {
                "pattern": /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
                "message": "Please supply a valid forename"
            }
        },
        "Lastname": {
            "presence": { message: "Please supply your surname" },
            "format": {
                "pattern": /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
                "message": "Please supply a valid surname"
            }
        },
        "Email": {
            "presence": { message: "Please supply an email address" },
            "format": {
                "pattern": /^[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/,
                "message": "Please supply a valid email address"
            }
        },
        /*
        "PostalCode": {
            "presence" : {message: "Please enter a location"}
        },
        */
        "g-recaptcha-response": {
            "recaptchav2": { message: "Please prove you are not a robot" }
        }
    },
    "validform": function (form) {
        //console.log('go');
        //form.submit();
    }
});
window.callbackFormValidator = callbackFormValidator;

// BPS landing page version 2 sprint 2 august 2018
// /templates/lp-bps2.html
if (document.querySelectorAll('form#bps2FormV2').length > 0) {
    var callbackFormValidator = new BDFormValidate({
        "formSelector": "form#bps2FormV2",
        "constraints": {
            "Firstname": {
                "presence": { message: "Please supply your forename" },
                "format": {
                    "pattern": /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
                    "message": "Please supply a valid forename"
                }
            },
            "Lastname": {
                "presence": { message: "Please supply your surname" },
                "format": {
                    "pattern": /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
                    "message": "Please supply a valid surname"
                }
            },
            "Email": {
                "presence": { message: "Please supply an email address" },
                "format": {
                    "pattern": /^[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/,
                    "message": "Please supply a valid email address"
                }
            },
            "PostalCode": {
                "presence": { message: "Please enter a location" },
                "format": {
                    "pattern": /^[a-zA-Z0-9 ]{1,10}$/,
                    "message": "Please supply a valid postcode"
                }
            },
            //"email-optin": {
            //    "presence": { message: "Please confirm you are willing to receive emails from Brewin Dolphin" }
            //},
            //"MarketingCommsOptIn": {
            //    "presence": { message: "Please confirm you are willing to receive emails from Brewin Dolphin" }
            //},
            "g-recaptcha-response": {
                "recaptchav2": { message: "Please prove you are not a robot" }
            }
        },
        "xcheckboxes": [
            {
                "name": "MarketingCommsOptIn",
                "required": true,
                "message": "Please opt-in to receive emails from Brewin Dolphin."
            }
        ],
        "validform": function (form) {
            return false;
            console.log('go');
            DownloadFamilyWealthReportFormSubmitEvent('insightssignup_form_submit', $('#marketoformid').val());
        }
    });
    window.callbackFormValidator = callbackFormValidator;
}



var dwfrFormValidator = new BDFormValidate({
    "formSelector" : "form#dfwrForm",
    "constraints" : {   
        "Firstname": {
            "presence": { message: "Please supply your forename" },
            "format": {
                "pattern": /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
                "message": "Please supply a valid forename"
            }
        },
        "Lastname": {
            "presence": { message: "Please supply your surname" },
            "format": {
                "pattern": /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
                "message": "Please supply a valid surname"
            }
        },
        "Email": {
            "presence": { message: "Please supply an email address" },
            "format": {
                "pattern": /^[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/,
                "message": "Please supply a valid email address"
            }
        },        
        "PostalCode": {
            "presence": { message: "Please enter a location" },
            "format": {
                "pattern": /^[a-zA-Z0-9 ]{1,10}$/,
                "message": "Please supply a valid postcode"
            }
        },
        //"email-optin": {
        //    "presence": { message: "Please confirm you are willing to receive emails from Brewin Dolphin" }
        //},
        //"MarketingCommsOptIn": {
        //    "presence": { message: "Please confirm you are willing to receive emails from Brewin Dolphin" }
        //},
        "g-recaptcha-response": {
            "recaptchav2": { message: "Please prove you are not a robot" }
        }
    },
      "xcheckboxes" : [
        {
          "name" : "MarketingCommsOptIn",
          "required" : true,
          "message" : "Please confirm you are willing to receive emails from Brewin Dolphin"
        }
      ],
      "validform": function (form) {
          return false;
          console.log('go');
          DownloadFamilyWealthReportFormSubmitEvent('wealthreport_form_submit', $('#marketoformid').val());
    }
  });
  window.dwfrFormValidator = dwfrFormValidator;

  // find-more-expandable office finder
  // /components/find-my-nearest-office.html
  // /components/start-a-conversation.html
  // <form data-validate validate="false" id="locationSearch2Form" data-target="../templates/office-finder.html" novalidate>
  /*
  ** REDIRECTS to office finder page
  */
  if (document.querySelectorAll('form#locationSearch2Form').length > 0) {
    var officeFinderFormValidator = new BDFormValidate({
      "formSelector" : "form#locationSearch2Form",
      "constraints" : {
        "locationsearch2": {
          "presence" : {message: "Please enter a location"}
        }
      },
      "validform" : function(form) {
        console.log('go');

        var redirect;
        // form.submit();
        if ( form.dataset ) {
          console.log( form.dataset.target );
          redirect = form.dataset.target;
        } else {
          console.log( form.getAttribute('data-target') );
          redirect = form.dataset.target;
        }
        window.location.href = redirect;
      }

    });
    window.officeFinderFormValidator = officeFinderFormValidator;
  }

});


// *** DOES NOT SUPPORT IE 9
/**/
/*
validate.init({

  // Classes and Selectors
  selector: '[data-validate]', // The selector for forms to validate
  fieldClass: 'error', // The class to apply to fields with errors
  errorClass: 'error-message', // The class to apply to error messages

  // Form Submission
  disableSubmit: true, // If true, don't submit the form to the server (for Ajax for submission)
  onSubmit: function (form, fields) {
    if (true) {
      if (typeof btnSubmit != 'undefined') {
        btnSubmit.addEventListener("click", function(e) {
          for (var i = 0; i < formSections.length; i++) {
            if (!formSections[i].classList.contains('form--submitted')) {
              formSections[i].classList.add('form--submitted');
            }
            if (!formSubmitted.classList.contains('form--submitted')) {
              formSubmitted.classList.add('form--submitted');
            }
          }
        });
      }
    }
  }
});
*/
/*
--------------------------------------------------------------------------------
form validation
END
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
Accordion JS
need to update this to handle window.resize, change of content arrangemnt in open items
START
--------------------------------------------------------------------------------
*/
var acc = document.getElementsByClassName("accordion__title");

for (var i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("accordion--active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}
/*
--------------------------------------------------------------------------------
Accordion JS
END
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
Accordion JS for sitemap
need to update this to handle window.resize, change of content arrangemnt in open items
START
--------------------------------------------------------------------------------
*/
var accSitemap = document.getElementsByClassName("sitemap__title");

for (var i = 0; i < accSitemap.length; i++) {
  accSitemap[i].addEventListener("click", function() {
    this.classList.toggle("sitemap--active");
    var panel = this.nextElementSibling;
    panel.classList.toggle("active");
    // if (panel.style.maxHeight){
    //   panel.style.maxHeight = null;
    // } else {
    //   panel.style.maxHeight = panel.scrollHeight + "px";
    // }
  });
}
/*
--------------------------------------------------------------------------------
Accordion JS for sitemap
END
--------------------------------------------------------------------------------
*/

/* vendors/optimizedResize.js*/
/*
window.addEventListener("optimizedResize", function() {
    console.log("Resource conscious resize callback!");
});
*/

// Placeholder fix for IE9.
/*
var inputs = document.querySelectorAll('input, textarea');

for(var inputCounter = 0; inputCounter < inputs.length; inputCounter++) {
  placeHolder(inputs[inputCounter]);
}
*/

/*
--------------------------------------------------------------------------------
Lightbox code
https://basiclightbox.electerious.com/
https://github.com/electerious/basicLightbox
incompatible with ANY version if IE - requires object.assign polyfill
/scr/scripts/vendors/
incompatible with IE9 - requires requestanimationframe polyfill

START
--------------------------------------------------------------------------------
*/
var lightbox = document.querySelectorAll('[data-basiclightbox]');
if ( lightbox.length ) {
  var lightboxes = [];
  var videosFound = false;
  // var lightboxOuter = lightbox.outerHTML;

  // var lightboxHTML = basicLightbox.create(lightbox[0].outerHTML,{ "closable": false });
  /* close terms lightbox HTML TEST ONLY */
  // lambda function no workee in IE
  // var lightboxHTML = basicLightbox.create(lightbox[0].outerHTML,{ "closable": false, "beforeShow" : (instance) => { instance.element().querySelector('a.lb-accept').onclick = instance.close; instance.element().querySelector('a.lb-decline').onclick = instance.close; }});

  for (var i = 0; i < lightbox.length; i++) {
    var thisLightbox = lightbox[i];
    var type = thisLightbox.getAttribute('data-type');

    if ( type === 'brighttalk') {

      lightboxes.push({
        "instance" : basicLightbox.create(thisLightbox.outerHTML,{
        // "instance" : basicLightbox.create('<p>VIDEO1</p>',{
          "closable": true,
          "className" : "brightalk",
          "beforeShow" : function(instance) { instance.element().querySelector('img.close').onclick = instance.close;  },
          "afterClose": function() {}
        }),
        "id" : thisLightbox.getAttribute('data-id')
      });
      videosFound = true;
    } else {
            var chsel = getCookie("fa-term-accp");
            if (chsel == "") {
                var instance = basicLightbox.create(thisLightbox.outerHTML,
                    {
                        "closable": false,
                        "beforeShow": function(instance) {
                            instance.element().querySelector('a.lb-accept').onclick = FaTermsAccept;
                            instance.element().querySelector('a.lb-decline').onclick = FaTermsDecline;
                            document.body.classList.add("noscroll");
                        },
                        "afterClose": function() { document.body.classList.remove("noscroll") }
                    });
                lightboxes.push({
                    "instance": instance
                });

                document.addEventListener('DOMContentLoaded',
                    function() {
                        instance.show();
                    });
            }
        }
    if ( videosFound ) {
      document.addEventListener('DOMContentLoaded', function() {

        var acc = document.getElementsByClassName("video_popup_trigger");

        for (var j = 0; j < acc.length; j++) {
          acc[j].addEventListener("click", function(e) {
            e.preventDefault();

            // find lightbox instance to open based on link data-videoid attribute
            var vi = 0,vc = lightboxes.length, vf = false;
            do {
              var tv = lightboxes[vi];
              if ( tv.id === this.getAttribute('data-videoid')) {
                tv.instance.show();
              }
              vi++;
            } while (vi < vc && !vf)

          });
        }

      });
    }
  }
}
/*
--------------------------------------------------------------------------------
Lightbox code
END
--------------------------------------------------------------------------------
*/


/*
--------------------------------------------------------------------------------
Setting same height on columns with matchHeight.js plugin
START
--------------------------------------------------------------------------------
*/
if (typeof $ !== 'undefined') {
  var archiveContent = document.querySelectorAll('.archive__item__content');

  $(function() {
    if (archiveContent.length > 0) {
      $('.archive__item__content').matchHeight();
    }
  });

  var relatedLinks = document.querySelectorAll('.related-links__block');

  $(function() {
    if (relatedLinks.length > 0) {
      $('.related-links__block').matchHeight();
    }
  });

  var collectionItem = document.querySelectorAll('.collection__item__content');

  $(function() {
    if (collectionItem.length > 0) {
      $('.collection__item__content').matchHeight();
    }
  });

  var mh1Items = document.querySelectorAll('.mh1');

  $(function() {
    if (mh1Items.length > 0) {
      $('.mh1').matchHeight();
    }
  });

  var mh2Items = document.querySelectorAll('.mh2');

  $(function() {
    if (mh2Items.length > 0) {
      $('.mh2').matchHeight();
    }
  });
}
/*
--------------------------------------------------------------------------------
Setting same height on columns with matchHeight.js plugin
END
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
Global CTA - open overlays for 'request a callback' and 'sign up for newsletter'
START
--------------------------------------------------------------------------------
*/
// global CTA: Start a conversation
if ( $('.req_trigger').length > 0 ) {
  $('.req_trigger').on('click',function(e){
    e.preventDefault();

    console.log('xy');
    // for forms with radio button groups reset all redio buttons    
    // .clientYesNo > input[type="radio"]
    $('.clientYesNo input').each(function(i) {
      $(this).attr({"checked" : false});
    });

    $('body').addClass('req_open');
  });

  $('#req .closer').on('click',function(e){
    e.preventDefault();
    $('body').removeClass('req_open');
  });

}

// Global CTA: Stay updated
if ( $('.news_trigger').length > 0 ) {
  $('.news_trigger').on('click',function(e){
    e.preventDefault();
    $('body').addClass('news_open');
  });

  $('#news .closer').on('click',function(e){
    e.preventDefault();
    $('body').removeClass('news_open');
  });
}

// our leadership email overlay
// /src/js/custom/ij_office.js
// line 197 "queueComplete": function (queue) {

/*
// bio email form
if ( $('.email_trigger').length > 0 ) {
  $('.email_trigger').on('click',function(e){
    e.preventDefault();

    // for forms with radio button groups reset all redio buttons    
    // .clientYesNo > input[type="radio"]
    $('.clientYesNo input').each(function(i) {
      $(this).attr({"checked" : false});
    });

    $('body').addClass('emailform_open');
  });

  $('#emailform .closer').on('click',function(e){
    e.preventDefault();
    $('body').removeClass('emailform_open');
  });
}
*/
/*
--------------------------------------------------------------------------------
Global CTA - open overlays for 'request a callback' and 'sign up for newsletter'
START
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
google captcha
START
--------------------------------------------------------------------------------
*/
function onloadCallback() {
  // explicitly bind google recaptcha to HTML element #bdgr once the recaptcha library has loaded
  /*
  grecaptcha.render('bdgr',{
    "sitekey" : "6LcJXGkUAAAAAL8ec-uxNnBPvtpYKMX_pTatHl6r",
    "size" : "invisible",
    "callback" : correctCaptcha // set callback to run when the captcha is executed
  });
  */
};
function correctCaptcha(response) {
  callbackFormValidator.doCaptcha(response);
};
/*
--------------------------------------------------------------------------------
google captcha
END
--------------------------------------------------------------------------------
*/
