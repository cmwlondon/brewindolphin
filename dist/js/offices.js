// <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBkJlPoR5tBQb57cpHsLc-vUtgJ9lPcrbY&libraries=places,geometry&callback=initMap" async defer></script>
// google maps libraris: places, geometry

var API_KEY = 'AIzaSyBkJlPoR5tBQb57cpHsLc-vUtgJ9lPcrbY';

var map,
    geocoder,
    markers = [],
    offices = [],
    start,
    searchtext,
    mapbounds,
    inBelfast = false;
var btnSubmit, mapContainer, listContainer, infoWindow, btnGeolocate;

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    zoom: 5,
    center: {
      lat: 53.480071725877316,
      lng: -2.2492361068725586
    }
  });

  /*
  var belfastbox = new google.maps.LatLngBounds(google.maps.LatLng(54.036213,-8.177240), google.maps.LatLng(55.251712,-5.435084));
    var rectangle = new google.maps.Rectangle({
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#FF0000',
    fillOpacity: 0.35,
    map: map,
    bounds: {
      north: 55.251712,
      south: 54.036213,
      east: -5.435084,
      west: -8.177240
    }
  });
  */

  getOffices(function () {
    // getOffices callback
    /* START */
    geocoder = new google.maps.Geocoder();

    btnSubmit = document.getElementById("locationsubmit");
    mapContainer = document.getElementsByClassName("office-finder--integrated")[0];
    listContainer = document.getElementsByClassName("office-finder__section__list")[0];
    infoWindow = new google.maps.InfoWindow();
    btnGeolocate = document.getElementById("locationgeolocate");

    // Try HTML5 geolocation via 'use my location' button
    btnGeolocate.addEventListener("click", function () {

      if (navigator.geolocation) {
        console.log('navigator.geolocation');
        navigator.geolocation.getCurrentPosition(function (position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };

          /*
          infoWindow.setPosition(pos);
          infoWindow.setContent('Location found.');
          infoWindow.open(map);
          */
          map.setCenter(pos);

          // more reliable method: lay rectangular bounding box over N. Ireland and check to see if location lies within the box
          if (54.036213 <= pos.lat && pos.lat <= 55.251712 && -8.177240 <= pos.lng && pos.lng <= -5.435084) {
            inBelfast = true;
          } else {
            inBelfast = false;
          }

          showOfficesPanels(btnSubmit);
          showOffices(pos);
        }, function () {
          // handleLocationError(true, infoWindow, map.getCenter());
        });
      } else {
        // Browser doesn't support Geolocation
        // handleLocationError(false, infoWindow, map.getCenter());
      }
    });
    // end of HTML5 geoloacation block

    // use information entered in text box to find location
    // find-more-expandable office finder
    // office-finder-template 0 opens google map in page
    var officeFinderFormValidator = new BDFormValidate({
      "formSelector": "form#locationSearchForm",
      "constraints": {
        "locationsearch": {
          "presence": { message: "Please enter a location" }
        }
      },
      "validform": function (form) {

        var address = document.getElementById("locationsearch").value;
        if (searchtext === address) {} else {
          searchtext = address;

          var startLocation = geocodeAddress(geocoder, address, function (startLocation) {
            if (startLocation) {
              /*
              a) get postcode for location via reverse geocode
              geocoder.geocode({'location': startLocation}, function(results, status) {
                var address = results[0]['formatted_address'];
                inBelfast = (address.indexOf('Belfast') !== -1);
                  // put continuance code here
                // display map
                // display office list
                // add office markers to map ->showOffices(startLocation);
              });
              */

              // more reliable method: lay rectangular bounding box over N. Ireland and check to see if location lies within the box
              if (54.036213 <= startLocation.lat && startLocation.lat <= 55.251712 && -8.177240 <= startLocation.lng && startLocation.lng <= -5.435084) {
                inBelfast = true;
              } else {
                inBelfast = false;
              }

              showOfficesPanels();
              showOffices(startLocation);
            }
          });
        }
      }
    });
  });
}
window.initMap = initMap;

function showOfficesPanels() {
  if (!btnSubmit.classList.contains("button--integrated")) {} else {
    if (!mapContainer.classList.contains("office-finder--integrated--visible")) {
      mapContainer.classList.add("office-finder--integrated--visible");
      listContainer.classList.add("office-finder__section__list--invisible");
    }
  }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ? 'Error: The Geolocation service failed.' : 'Error: Your browser doesn\'t support geolocation.');
  infoWindow.open(map);
}

function showOffices(startLocation) {

  // clear info window
  $('.officeDetails').find('.office-finder__office__details').not('.office-finder__office__details__template').remove();

  // clear map markers
  if (markers.length > 0) {
    var mi = 0,
        mc = markers.length,
        mt;
    do {
      markers[mi].setMap(null);
      mi++;
    } while (mi < mc);
    markers = [];
  }

  showStart(startLocation);

  // filter offices based on whether the user is in England/Scotland/Wales or Northern Ireland
  if (inBelfast) {
    // a) user is in belfast, show only belfast offices

    /*
    var belfastOffice = offices[1];
    // get distance from belfast office to startLocation
    var p1 = new google.maps.LatLng(belfastOffice.lat, belfastOffice.lng),
        p2 = new google.maps.LatLng(startLocation.lat, startLocation.lng);
    var d = google.maps.geometry.spherical.computeDistanceBetween (p1,p2 );
    belfastOffice.distance = d;
    addOffice(belfastOffice,0);
    */

    // search for all offices including Belfast
    findOffices(offices, startLocation);
  } else {
    // b) user outisde of belfast, show 3 nearest offices except belfast

    // filter out belfast office
    var ukoffices = offices.filter(function (office) {
      return office.address.indexOf('Belfast') === -1;
    });
    findOffices(ukoffices, startLocation);
  }

  var bounds = new google.maps.LatLngBounds();
  for (var i = 0; i < markers.length; i++) {
    bounds.extend(markers[i].getPosition());
  }
  map.fitBounds(bounds);
}

function findOffices(officeList, startLocation) {
  var p1 = {},
      p2 = {},
      d = 0,
      rangeIndex = [],
      rangeIndex2 = [],
      rangeIndex3 = [];
  officeList.map(function (office) {
    p1 = new google.maps.LatLng(office.lat, office.lng);
    p2 = new google.maps.LatLng(startLocation.lat, startLocation.lng);
    d = google.maps.geometry.spherical.computeDistanceBetween(p1, p2);
    office.distance = d / 1000; // convert metres to kilometres
  });

  // build distance matrix for sorting
  officeList.map(function (office, index) {
    rangeIndex.push({ "office": index, "distance": office.distance });
  });
  // sort  offices by ascending distance
  rangeIndex2 = quickSort(rangeIndex, 0, officeList.length - 1);
  // discard all but nearest 3
  rangeIndex3 = rangeIndex2.slice(0, 3);
  // add  offices to map
  rangeIndex3.map(function (office, index) {
    var thisOffice = officeList[office.office];
    // console.log("title: %s", thisOffice.title);
    addOffice(thisOffice, index);
  });
}

function addOffice(office, index) {
  addOfficeMarker(office, index);
  addOfficeInfo(office, index);
}

function addOfficeMarker(office, index) {
  var labels = "123";
  // add map markers
  var newMarker = new google.maps.Marker({
    "position": new google.maps.LatLng(office.lat, office.lng),
    "label": labels[index],
    "map": map,
    "animation": google.maps.Animation.DROP,
    "office": office.id,
    "url": office.url
  });

  newMarker.addListener('click', function () {
    // highlightOffice(this);
    openOfficePage(this);
  });

  markers.push(newMarker);
}

function addOfficeInfo(office, index) {
  // show office information
  var officeTemplate = $('.office-finder__office__details__template').clone();
  officeTemplate.removeClass('hidden office-finder__office__details__template');
  officeTemplate.attr({ "id": "office" + office.id });

  var title = office.title,
      address = office.address,
      ad2 = address.split(','),
      distance = office.distance,
      email = office.email,
      phone = office.phone,
      page = office.url;
  officeTemplate.find('h3 a').text(index + 1 + '. ' + title).attr({ "href": page });

  ad2.map(function (span) {
    officeTemplate.find('.addressbox').append($('<span></span>').text(span));
  });
  officeTemplate.find('span.office__distance').text(distance.toFixed(2) + ' km');

  $('.officeDetails').append(officeTemplate);
}

function highlightOffice(marker) {
  console.log(marker.office); // office.id, link to items in left hand column
  map.setCenter(marker.position);
}

function openOfficePage(marker) {
  console.log(marker.office, marker.url);
  window.location.href = marker.url;
}

function showStart(startLocation) {
  map.setCenter(startLocation);
  var marker = new google.maps.Marker({
    "map": map,
    "position": startLocation
  });

  markers.push(marker);
}

function buildDistanceMatrix() {
  /* requires gogole maps geometry library
  <script src="https://maps.googleapis.com/maps/api/js?key=[[APIKEY]]&
  &libraries=geometry,places
  &callback=initMap" async defer></script>
  */
}
function sortByDistance() {}

function quickSort(arr, left, right) {
  var i = left,
      j = right,
      tmp,
      pivotidx = (left + right) / 2,
      pivot = arr[pivotidx.toFixed()].distance;

  while (i <= j) {
    // ascending order
    while (arr[i].distance < pivot) {
      i++;
    }

    while (arr[j].distance > pivot) {
      j--;
    }

    if (i <= j) {
      tmp = arr[i];
      arr[i] = arr[j];
      arr[j] = tmp;

      i++;
      j--;
    }
  }

  /* recursion */
  if (left < j) quickSort(arr, left, j);
  if (i < right) quickSort(arr, i, right);
  return arr;
}

function geocodeAddress(geocoder, address, callback) {
  // "country" : ["GB","IE","JE"]
  // can only limit results to ONE country at a time in a request
  // need to query against three countries: GB, Ireland and Jersey
  // queue three requests against required countriees, collate results/ignore queries which return no result
  geocoder.geocode({
    "componentRestrictions": {
      "country": "GB"
    },
    "address": address
  }, function (results, status) {
    if (status === "OK") {
      callback({ "lat": results[0].geometry.location.lat(), "lng": results[0].geometry.location.lng() });
    } else {
      // alert("Geocode was not successful for the following reason: " + status);
      alert('Please enter a UK location.');
    }
  });
}

function addMarkerWithTimeout(officeID, latitude, longitude, timeout) {

  var newMarker = new google.maps.Marker({
    "position": new google.maps.LatLng(latitude, longitude),
    "map": map,
    "animation": google.maps.Animation.DROP,
    "office": officeID
  });

  newMarker.addListener('click', function () {
    console.log(this.office);
    console.log(this.position);
  });

  window.setTimeout(function () {
    markers.push(newMarker);
  }, timeout);
}

function clearMarkers() {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(null);
  }
  markers = [];
}

// ** LOADED ON PAGE LOAD,
function getOffices(callback) {

  var request = new XMLHttpRequest();

  request.open('GET', '../assets/offices/offices.json', true);

  request.onload = function () {
    if (request.status >= 200 && request.status < 400) {
      // Success!
      var data = JSON.parse(request.responseText);

      data.items.map(function (item) {
        offices.push({
          "id": item.id,
          "url": item.url,
          "title": item.title,
          "lat": item.lat,
          "lng": item.lng,
          "address": item.address,
          "phone": item.phone,
          "email": item.email
        });
        // addMarkerWithTimeout(item.id, item.lat, item.lng, i * 200);
      });
      callback();
    } else {
      // We reached our target server, but it returned an error
    }
  };

  request.onerror = function () {
    // There was a connection error of some sort
  };

  request.send();
}

// getOffices();