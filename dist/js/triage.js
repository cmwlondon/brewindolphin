var journeys,
    context = '',
    wpEnable = true;

function buildJourneyStart(journey) {

	var quiz_section_template = $('.secx_template').clone();
	quiz_section_template.removeClass('hidden secx_template');

	var firstButton = buildButton(journey);
	firstButton.addClass('secx_initial');

	quiz_section_template.append(firstButton);

	quiz_section_template.addClass('context_' + journey.context);
	$('.secxbox').append(quiz_section_template);
}

function buildButton(button) {
	var quiz_button_template = $('.secx_button_template').clone();
	quiz_button_template.removeClass('hidden secx_button_template');

	quiz_button_template.attr({
		"data-key": button.key,
		"data-level": button.level,
		"data-result": button.result.length !== 0 ? "true" : "false",
		"data-context": button.context
	}).text(button.value) //   // journey.text
	.on('click', function (e) {
		e.preventDefault();

		var dataFields = [],
		    datakeys = ['key', 'level', 'result', 'context'];
		datakeys.map(function (field) {
			dataFields[field] = $(this).attr('data-' + field);
			// dataFields.push($(this).attr('data-' + field));
		}, this);

		doStep(this, dataFields);
	});

	return quiz_button_template;
}

function buildResultsPanel(result) {
	// extract HTML template from page
	var result_template = $('.triage-quiz__result__template').clone();
	result_template.removeClass('hidden triage-quiz__result__template');

	result_template.find('h3.result-header').html(result.title);
	result_template.find('p.result-content').html(result.content); // SINGLE PARAGRAPH
	result_template.find('div.block-brewin__content--image').css({ "background-image": "url(" + result.imgSrc + ")" }); // backgroundimage

	var buttonContainer = result_template.find('ul.buttons'),
	    buttonf;
	result.buttons.map(function (button) {
		buttonf = $('<li></li>').append($('<a></a>').attr({ "href": button.buttonLink }).html(button.buttonCaption));
		buttonContainer.append(buttonf);
	});

	// result_template.find('a.button').attr({"href" : result.buttonLink }).html( result.buttonCaption ); // button caption and destination

	return result_template;
}

function doStep(button, data) {
	var section = $(button).parents('div.secx').eq(0),
	    j = getStepViaKey(data.key);

	$('.secxbox').slideUp(500, function () {
		section.find('a').addClass('off');

		// show hide relevant section
		if (data.level == 1) {
			if (context !== data.context) {
				context = data.context;

				// hide items in unselected section
				var otherContext = context === 'personal' ? 'financial' : 'personal';
				section.addClass('on');
				$('.context_' + otherContext).addClass('off');
			}
			$('.triage-quiz__start__container').removeClass('unset');
		}

		// add selected button text to breadcrumb
		$('.triage-quiz__crumb__container ul').append('<li><span>' + j.value + '</span></li>');

		// check to see if the journey ahs ended (results.length !== 0)
		if (data.result === "true") {
			j.result.map(function (result) {

				// check to see if wealth pliot results are allowed
				if (wpEnable && result.title === 'WealthPilot' || result.title !== 'WealthPilot') {
					$('#triage-quiz-results-container').append(buildResultsPanel(result));
				}
			});
			$('.triage-quiz__results').addClass('triage-quiz__results--visible');
		}

		if (j.nextOptions.length > 0) {
			// build buttons for next step(s) from j.nextOptions
			j.nextOptions.map(function (step) {
				var stepButtonData = getStepViaKey(step);
				stepButtonData.context = context;
				section.append(buildButton(stepButtonData));
			});

			$('.secxbox').delay(100).slideDown(500);
		}
	});
}

function getStepViaKey(key) {
	var ji = 0,
	    jc = journeys.items.length,
	    j;

	do {
		j = journeys.items[ji];
		if (key === j.key) {
			return j;
		}
		ji++;
	} while (ji < jc);

	return false;
}

$(document).ready(function () {
	wpEnable = $('.triage-quiz').attr('data-wp-enabled') === 'yes' ? true : false;

	if (wpEnable) {
		console.log("wpEnable TRUE");
	} else {
		console.log("wpEnable FALSE");
	}

	// need to supply different JSON data files for personal
	$.getJSON('../assets/triage/triage.json', function (data) {
		journeys = data; // drop journey map into global variable instead of reloading on every click

		// build initial questions
		// start with level 1 items
		var level1Items = [],
		    ji = 0,
		    jc = journeys.items.length,
		    j;

		do {
			j = journeys.items[ji];
			if (j.level === "1") {
				level1Items.push(j);
			}
			ji++;
		} while (ji < jc);

		buildJourneyStart({
			"key": level1Items[0].key,
			"level": level1Items[0].level,
			"result": level1Items[0].result,
			"value": level1Items[0].value,
			"context": "personal"
		});

		buildJourneyStart({
			"key": level1Items[1].key,
			"level": level1Items[1].level,
			"result": level1Items[1].result,
			"value": level1Items[1].value,
			"context": "financial"
		});

		$("#triage-start").click(function () {
			$(".triage-quiz__logic__inner").slideUp(500, function () {
				// clear breadcrumbs
				$('.triage-quiz__crumb__container ul').empty();
				$('.triage-quiz__start__container').addClass('unset');

				$('.secx a').not('.secx_initial').remove();
				$('.secx').removeClass('off on');
				$('.secx_initial').removeClass('off');
				context = '';
				$(".triage-quiz__logic__inner").slideDown(500, function () {

					// reset results panel
					$('.triage-quiz__results').removeClass('triage-quiz__results--visible');
					$(".triage-quiz__logic__inner").delay(500).slideDown(500, function () {
						$("#triage-quiz-results-container").empty();
					});
				});
			});
		});
	});
});