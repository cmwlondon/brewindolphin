$(document).ready(function () {

  $("#triage-start").click(function () {
    $('.triage-quiz__crumbs').empty();
    $(".triage-quiz__section").removeClass("triage-quiz__section--selected");
    $('.triage-quiz__item').removeClass('triage-quiz__item--active');
    $("[data-level=1]").parent().addClass('triage-quiz__item--active');
  });

  $('.triage-quiz__item > a').click(function (e) {
    e.preventDefault();

    var optionSelected = $(this),
        optionParent = $(this).parent(),
        dataLevel = $(this).attr('data-level'),
        dataResult = $(this).attr('data-result'),
        dataID = optionSelected.attr('data-id'),
        selectedContent,
        result,
        resultContent,
        resultTitle;

    var nextOptions = [];

    if (dataResult === "false") {
      $(".triage-quiz__logic__inner").slideUp(500, function () {
        fetchJourney();
        $(".triage-quiz__logic__inner").delay(500).slideDown(500);
      });
    } else {
      $(".triage-quiz__logic__inner").slideUp(500);
      fetchResults();
    }

    function fetchJourney() {

      if (dataLevel == "1") {
        optionParent.parent().addClass("triage-quiz__section--selected");
      }

      $.getJSON('../assets/triage/triage-np.json', function (data) {

        data.items.map(function (item) {
          if (dataID === item.key) {
            nextOptions = item.nextOptions;
            selectedContent = item.value;
          }
        });

        $('.triage-quiz__crumbs').append('<li>' + selectedContent + '</li>');

        if (nextOptions.length) {
          $(".triage-quiz__item--active").removeClass("triage-quiz__item--active");
          for (var i = 0; i < nextOptions.length; i++) {
            $("[data-id=" + nextOptions[i] + "]").parent().addClass('triage-quiz__item--active');
          }
        }
      });
    }

    function fetchResults() {

      $.getJSON('../assets/triage/triage-np.json', function (data) {

        data.items.map(function (item) {
          if (dataID === item.key) {
            selectedContent = item.value;
            result = item.result;
          }
        });

        if (result.length) {
          for (var j = 0; j < result.length; j++) {
            resultTitle = result[j].title;
            resultContent = result[j].content;
            loadData(resultTitle, resultContent);
          }
        }

        $('.triage-quiz__crumbs').append('<li>' + selectedContent + '</li>');

        $('.triage-quiz__results').addClass('triage-quiz__results--visible');
      });
    }

    function loadData(resultTitle, resultContent) {
      $.get("../assets/triage/results/result-skeleton.html", function (data) {
        $('#triage-quiz-results-container').append(data);
        $('.result-header').last().append(resultTitle);
        $('.result-content').last().append(resultContent);
      });
    }
  });
});