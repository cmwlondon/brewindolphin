/*
brewin dolphin
external site template 
javasript modules for main navigation
*/

var menuBtn = document.querySelector(".btn-menu");
    body = document.getElementsByTagName("BODY")[0],
    categoryLink = document.querySelectorAll(".masthead-category--selected")[0],
    categoryList = document.querySelectorAll(".masthead-categories-sub")[0],
    cat_vol = document.querySelectorAll("body")[0],
    openSubmenu = -1,
    mobileEnabled = false,
    mobileBreakpoint = 992,
    w1 = -1,w2 = -1,
    categoryListTimer = null,
    clShow = false,
    placholderSupported = (typeof document.createElement("input").placeholder === 'string');

// navigation menu click event handlers for mobile
function menuOpenHandler(e) {
  if ( mobileEnabled ) {
    if (body.classList.contains('menu__open')) {
      body.classList.remove("menu__open");
    } else {
      body.classList.add("menu__open");
    }
  }
};

function categoryListHandler(e) {
  if ( mobileEnabled ) {
    if(categoryList.classList.contains('masthead-categories-sub--visible')) {
      categoryList.classList.remove('masthead-categories-sub--visible');
      cat_vol.classList.remove('ovl_open');
    } else {
      categoryList.classList.add('masthead-categories-sub--visible');
      cat_vol.classList.add('ovl_open');
    }
  }
}

function categoryListHover(e) {
  if ( !mobileEnabled ) {
    // categoryList = 'ul.masthead-categories-sub'
    // show menu
    categoryList.classList.add('masthead-categories-sub--visible');

    categoryList.addEventListener('mouseenter', function(e) {
      categoryListTimer = null;
      // if ( categoryListTimer !== null ) { categoryListTimer.clearTimeout(); }
    });

    // add mouseout event to menu
    categoryList.addEventListener('mouseleave', function(e) {
      // start timer
      categoryListTimer = window.setTimeout(function() {
        categoryListTimer = null;
        if ( !mobileEnabled ) {
          categoryList.classList.remove('masthead-categories-sub--visible');
        }
      },100);
    }, false);
  }
}

// add click(touch) event handlers for menus
// check for menu button in HTML
if ( menuBtn !== null ) {
  mobileEnabled = (window.innerWidth < mobileBreakpoint);
  w1 = window.innerWidth;

  // mobile 'burger' button
  if (menuBtn !== null) {
    menuBtn.addEventListener('click', menuOpenHandler, false);
  }

  if (typeof categoryLink !== 'undefined') {
    categoryLink.addEventListener('click', categoryListHandler, false);  
    categoryLink.addEventListener('mouseover', categoryListHover, false);
  }


  window.addEventListener('resize', function(e) {
    var wx = window.innerWidth;
    mobileEnabled = (wx < mobileBreakpoint);
    
    // find transition FROM mobile to desktop, remove mobile active styles
    if (wx < mobileBreakpoint) { w1 = wx; }

    if (wx > mobileBreakpoint) { w2 = wx; }

    if ( !mobileEnabled && (w1 < mobileBreakpoint) && (w2 > mobileBreakpoint) ) {
      w1 = w2;
      // clear any open menu states
      categoryList.classList.remove('masthead-categories-sub--visible');
      body.classList.remove("menu__open");
    }
  }, false);  

}
