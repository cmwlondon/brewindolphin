var gulp          = require('gulp');
var sass          = require('gulp-sass');
var notify        = require('gulp-notify');
var browserSync   = require('browser-sync').create();
var sourcemaps    = require('gulp-sourcemaps');
var autoprefixer  = require('gulp-autoprefixer');
var uglify        = require('gulp-uglify');
var concat        = require('gulp-concat');
var rename        = require('gulp-rename');
var babel         = require("gulp-babel");
var bless         = require('gulp-bless');

var paths = {
  styles: {
    src: './src/sass',
    files: './src/sass/**/*.scss',
    dest: './dist/css/'
  }
}

function handleError(err) {
  var msg = 'Error: ' + err.message;

  console.error('Error', err.message);
  browserSync.notify('Error: ' + err.message);

  var args = Array.prototype.slice.call(arguments);
  notify.onError({
    title: 'Compile Error',
    message: '<%= error.message %>'
  }).apply(this, args);

  if (typeof this.emit === 'function') this.emit('end')
}

/*
gulp.task('sass', function() {
  gulp.src(paths.styles.files)
    //.pipe(sourcemaps.init())
    .pipe(sass({
      'sourceComments': false,
      'style' : 'compressed'
      // 'outputStyle': 'expanded'
    })).on('error', handleError)
    .pipe(autoprefixer('last 2 versions', '> 1%', 'ie >= 9'))
    //.pipe(sourcemaps.write('.'))
    .pipe(bless({ "suffix" : "-part" }))
    .pipe(gulp.dest(paths.styles.dest))
    .pipe(browserSync.stream({
      'once': true
    }));
});
*/

// bless: split file up into segments for IE
// need to run *after* sass processing - **REVERSES COMPRESSION**
// sass
// .pipe(bless({ "suffix" : "-part" }))
// output

  // SASS processing disabled due to massive output file size
  // run without bless - large single minifed CSS file
  gulp.task('sass', function () {
      gulp.src(paths.styles.files)
      .pipe(sass({outputStyle: 'compressed'}))
      // .pipe(sass())
      .pipe(gulp.dest(paths.styles.dest));
  });


gulp.task('uglify', function(){
	gulp.src(['./src/scripts/vendor/*.js', './src/scripts/*.js'])
    .pipe(sourcemaps.init())
    // .pipe(babel())
    .pipe(concat('main.js'))
    .pipe(gulp.dest('dist/js'))
    .pipe(rename('main.js'))
    // .pipe(uglify())
    .pipe(gulp.dest('dist/js'));
  gulp.src('./src/scripts/custom/*.js')
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(gulp.dest('dist/js'));
});


gulp.task( 'updatebrowser',function(){
  console.log('update browser');

  browserSync.reload;
});

gulp.task('dist-watcher', function(){
  console.log('dist-watcher START');

  browserSync.init({
    'server': './',
    'open': true
  });

  gulp.watch('./src/scripts/**/*.js', ['uglify','updatebrowser'])
  .on('change', function(event) {
    console.log('js file(s) updated');
  });

  // SASS processing disabled due to massive output file size

  // gulp.watch('./src/sass/**/*.scss', ['sass','updatebrowser'])
  // .on('change', function(event) {
  //   console.log('sass file(s) updated');
  // });
});

gulp.task('default', ['dist-watcher']);

// gulp.task('watch', ['serve']);


