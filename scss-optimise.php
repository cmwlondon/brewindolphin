<?php
/* brewin dolphin
this script is designed to index all class and id CSS selectors in the SASS src files in src/sass/
*/
function scanDirectory($directory) {
	// echo "<p><strong>directory: '$directory'</strong></p>";

	global $omitList;
	global $files;
	$result = array(); 

	$fileList = scandir($directory);

	foreach($fileList AS $item) {
		switch($item) {
			case '.' :
			case '..' :
			case '.DS_Store' : {

			} break;
			default : {
				if (is_dir($directory . $item)) {
					// directory
					if ( !in_array($item, $omitList) ) {
						scanDirectory($directory . $item . '/');
					} else {
					}
				} else {
					if ( !in_array($item, $omitList) ) {
						$files[] = $directory . $item;
					}
				}
			}
		}
	}
	return;
};

function isSelectorAlreadyRecorded($type, $name) {
	global $selectors;

	if ( count($selectors) > 0 ) {
		foreach( $selectors AS $selector ) {
			if ($selector['type'] === $type && $selector['name'] === $name) {
				return true;
			}
		}
	}

	return false;
}

// echo "brewin dolphin (S)CSS optimisation";

$rootPath = '/Applications/MAMP/htdocs/bd/current/';
$scss = $rootPath.'src/sass/';
$templates = $rootPath.'templates/';
$selectorFile = $rootPath.'selectors.json';

$selectorRegex = '((?:\.|\#)[a-z][a-zA-Z0-9\-\_]+)';
// $selectorRegex = '(\#[a-z][a-zA-Z0-9\-\_]+)';
/*
match class and id selectors
only match strings with first letter, avoid matchin .75em font size property
*/

// class=".*[CLASS SELECTOR].*"
// id="[ID SELECTOR]"

// list of directories / files / css colour definitions to skip
// #[a-f0-9]{3,6}
$omitList = array('bootstrap', 'fontawesome', 'slick', 'styles.scss','#fff','#ffffff','#cb2026','#ededed','#ff0000','#f00','#d2d7df','#c7c7c7','#ececed','#ebebed','#iefix','.woff','.woff2','.ttf','.fonts','.net','.css','.eot','.svg','#f8343ddf-e94d-471e-95b0-3b435acc2499','#d5dd03f5-3afb-46e9-aad0-234de8607afa','#a13d383f-5612-4e05-a4b1-bb690be4277c','.typenetwork','.com','.brewin','.co','.uk');

$files = [];
$selectors = [];

scanDirectory($scss);
$scssFiles = $files;

$files = [];
scanDirectory($templates);
$templateFiles = $files;

// echo "<pre>".print_r($scssFiles, true)."</pre>";
// echo "<pre>".print_r($templateFiles, true)."</pre>";

// index class and id selectors in listed scss files
// results in $selectors:
// array( "file" => $file, "selector" => $match )
foreach($scssFiles AS $file) {

	echo "<p>$file</p>";
	// read contents of filke
	$fp = fopen($file , 'r');
	$contents = fread($fp,filesize($file));
	fclose( $fp );

	// scan contents fro selectors using regex
	preg_match_all($selectorRegex, $contents, $matches, PREG_PATTERN_ORDER);
	// $selectors = array_merge($selectors,$matches);

	foreach($matches[0] AS $match) {

		if ( !in_array($match, $omitList) ) {
			$type = ( $match[0] === '#' ) ? 'id' : 'class';
			$name = substr($match,1);

			if( !isSelectorAlreadyRecorded($type, $name) ){
				echo "<p><strong>".$match[0]."$name</strong></p>";
				$selectors[] = array(
					"type" => $type,
					"file" => $file,
					"name" => $name
				);
			}
		}

	}
}
// echo "<pre>".print_r($selectors, true)."</pre>";
// save to local directory
$fp = fopen( $selectorFile, 'w');
fwrite($fp, json_encode( $selectors ));
fclose($fp);

echo "<p>selectors found: <strong>".count($selectors)."</strong></p>";
echo "<p>list written to <strong>$selectorFile</strong></p>";
// header('Content-Type: application/json');
// echo json_encode( $selectors );

?>
