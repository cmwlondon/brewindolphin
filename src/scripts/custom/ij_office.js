/* ------------------------------------------------ */
// Queue
/* ------------------------------------------------ */

function Queue(parameters) {
  this.parameters = parameters;
  this.items = parameters.items || [];
  this.settings = parameters.settings || {};
  this.actions = parameters.actions || {};

  this.itemCount = 0;
  this.activeWorkers = 0;
  this.workerIndex = 0;
  this.workerLimit = 5;
  this.workers = [];

  this.init();
}

Queue.prototype = {
  "constructor": Queue,
  "template": function () {
    var that = this;
  },

  "init": function () {
    var that = this;

    // console.log('Queue.init');
    // console.log(this.settings);
  },
  "addItem": function (newItem) {
    var that = this;

    this.items.push(newItem);
    this.itemCount++;
  },
  "clearQueue": function () {
    var that = this;

    this.workers = [];
    this.itemCount = 0;
  },
  "startQueue": function () {
    var that = this,
        worker;

    this.actions.queueStart(this);

    while (this.items.length > 0 && this.activeWorkers < this.workerLimit) {
      worker = this.items.pop();
      this.workerStart(worker);
    }
  },
  "stopQueue": function () {
    var that = this;
  },
  "workerStart": function (worker) {
    var that = this;

    this.activeWorkers++;
    worker.wi = this.workers.length;
    this.workers[this.workers.length] = worker;

    this.actions.itemStart(this, worker);
  },
  "workerComplete": function (worker) {
    var that = this,
        worker;

    this.actions.itemComplete(this, worker);

    // clear finished worker
    this.activeWorkers--;
    this.workers.splice(worker.wi);

    // are there items left, and worker slots available, if so start unassigned item 
    while (this.items.length > 0 && this.activeWorkers < this.workerLimit + 1) {
      worker = this.items.pop();
      this.workerStart(worker);
    }

    // all items done
    if (this.activeWorkers == 0 && this.items.length == 0) {
      this.queueComplete();
    }
  },
  "queueComplete": function () {
    var that = this;

    this.actions.queueComplete(this);
  }
};

window.Queue = Queue;

/* ------------------------------------------------ */
// OfficePeopleIJ
/* ------------------------------------------------ */

function OfficePeopleIJ(parameters) {
  this.parameters = parameters;

  this.allPeopleBox = parameters.allPeopleBox;
  this.viewmorebutton = parameters.viewmorebutton;
  this.viewmoreBox = parameters.viewmoreBox;

  this.personTemplate = parameters.personTemplate;
  this.allPageMax = parameters.allPageMax;
  this.pageWidth = parameters.pageWidth;
  this.datapath = parameters.datapath;
  this.portraits = parameters.portraits;
  this.bottompad = parameters.bottompad;
  this.placeholder = parameters.placeholder;

  this.allPeople = [];
  this.workingAllPeople = [];
  this.allpage = 0;

  this.init();
}

OfficePeopleIJ.prototype = {
  "constructor": OfficePeopleIJ,

  "template": function () {
    var that = this;
  },
  "test": function () {
    var that = this;console.log("OfficePeopleIJ.test");
  },
  "init": function () {

    var that = this;

    this.loadPeople();
  },

"loadPeople": function () {
    var that = this;
    that.buildPeople(empdata);
    that.setEvents();
    //$.getJSON(this.datapath, function (data) {
    //  that.buildPeople(data);
    //  that.setEvents();
    //});
},

  "buildPeople": function (data) {
    var that = this;

    this.workingAllPeople = this.cloneArray(data.people);
    this.buildAll(this.workingAllPeople);
  },

  "buildAll": function (people) {
    var that = this;

    if (people.length > 0) {

      // display first six
      // var person,
      //     offset = (this.allpage * this.allPageMax),
      //     workingMax = ( (offset + this.allPageMax) > people.length) ? (this.workingAllPeople.length - offset) : this.allPageMax;

      // display all as requested by the client

      var workingMax = this.workingAllPeople.length;

      var juniorQueue = new Queue({
        "settings": {
          "container": this.allPeopleBox,
          "clipper": $('.viewJunior'),
          "workingMax": workingMax,
          "bottompad": this.bottompad
        },
        "actions": {
          "itemStart": function (queue, worker) {
            // build new senior person, attach callback to image onload event to mark this worker as done, move to new worker until all workers are done
            var newPerson = that.buildPerson(worker.index, worker.person, { "queue": queue, "worker": worker, "action": function (queue, worker) {
                queue.workerComplete(worker);
              } });

            queue.settings.container.append(newPerson);
          },
          "itemComplete": function (queue, worker) {
            /*
            var vh = queue.settings.container.height();
            if ( vh > queue.boxheight ) {
              queue.boxheight = vh;
            }
            */
          },
          "queueStart": function (queue) {
            // queue.boxheight = 0;
            that.viewmoreBox.addClass('closed');
          },
          "queueComplete": function (queue) {
            // queue.settings.clipper.css({"max-height" : "100000px"});

            // ensure heights of all boxes are the same based on height of tallest box + arbitrary bottom padding: queue.settings.bottompad
            var juniorMaxBoxHeight = 0,
                juniorCumulativeHeight = 0,
                juniorBoxes = $('ul.flexJunior li'),
                newHeight;

            juniorBoxes.find('.sizer').each(function () {
              if ($(this).height() > juniorMaxBoxHeight) {
                juniorMaxBoxHeight = $(this).height();
              }
            });
            newHeight = juniorMaxBoxHeight + queue.settings.bottompad;
            juniorBoxes.css({ "min-height": newHeight + "px" });

            juniorCumulativeHeight = juniorBoxes.length * (juniorMaxBoxHeight + 20);

            // if ( $(body).hasClass('ios') ) {
              juniorCumulativeHeight = juniorCumulativeHeight + 90;
            // }

            $('.viewJunior').css({ "max-height": juniorCumulativeHeight + "px" });
            // queue.settings.clipper.css({"max-height" : $('ul.flexJunior').height() + "px"}); 

            if ((that.allpage + 1) * that.allPageMax < people.length) {
              that.viewmoreBox.removeClass('closed');
            }

            // rig email overlay
            if ( $('.emailform_trigger').length > 0 ) {
              $('.emailform_trigger').on('click',function(e){
                e.preventDefault();

                // for forms with radio button groups reset all redio buttons    
                // .clientYesNo > input[type="radio"]
                $('.clientYesNo input').each(function(i) {
                  $(this).attr({"checked" : false});
                });

                var dataIndex = $(this).attr('data-index'),
                    thisPerson = thisOfficePeople.workingAllPeople[dataIndex];

                var overlayNode = $('#emailform');

                console.log('form reset');

                // reset form state
                overlayNode.removeClass('showEnd');
                overlayNode.find('form').eq(0).trigger("reset");
                overlayNode.find('.has-error').each(function(i) {
                  console.log('x');
                  $(this).removeClass('has-error');
                  $(this).find('.messages').empty();
                });
                
                overlayNode.find('.personName').eq(0).html( thisPerson.name );

                // store persons name in hidden field in form
                $('#tname').val(thisPerson.name);
                $('#temail').val(thisPerson.email);

                // show overlay
                $('body').addClass('emailform_open');
              });

              $('#emailform .closer').on('click',function(e){
                e.preventDefault();
                $('body').removeClass('emailform_open');
              });
            }

          }
        }
      });

      for (i = 0; i < workingMax; i++) {
        person = people[i];

        juniorQueue.addItem({
          "person" : person,
          "index" : i
        });
      }
      juniorQueue.items = juniorQueue.items.reverse();
      juniorQueue.startQueue();
    } else {
      // no people therefore no 'view more' button
      this.viewmoreBox.addClass('closed');
    }
  },

  /*
  that.buildPerson(
    worker.index,
    worker.person,
    {
      "queue": queue,
      "worker": worker,
      "action": function (queue, worker)
        queue.workerComplete(worker);
      }
    }
  );
  */
  "buildPerson": function (index, person, imageOnLoad) {
    var that = this;

    var pt = this.personTemplate.clone().removeClass('personTemplate');

    // image loader
    var newImg = $('<img></img>')
    .attr({ "src": this.portraits + person.pic, "alt": person.name })
    .addClass('portrait')
    .on('load', function(e) {
      // image loaded, move on to next job
      imageOnLoad.action(imageOnLoad.queue, imageOnLoad.worker);
    })
    .on('error', function(e) {
      // image did not load
      console.log('broken image');
      $(this).addClass('brokenImage');
      $(this).attr({ "src" : that.placeholder});
      imageOnLoad.action(imageOnLoad.queue, imageOnLoad.worker);
    });

    //pt.find('img').attr({"src" : this.portraits + person.pic, "alt" : person.name})
    pt.find('div.frame').append(newImg);
    pt.find('h4.name').html(person.name);
    pt.find('span.role').html(person.job);
    pt.find('span.phone').html(person.phone);
    
    // remove email link and preceeding forward slash if the email field for this person is blank or does not exist
    if (!person.email || person.email.length === 0 || person.email === '') {
      pt.find('span.email').remove();
    } else {
      pt.find('span.email a').attr({"href" : "-", "data-index" : index}).text('Email');
    }

    if (person.linkedin !== '') {
      pt.find('span.linkedin a').attr({ "href": person.linkedin }).text(person.linkedin);
    } else {
      pt.find('span.linkedin').remove();
    }

    pt.find('div.hidden-text h4').html(person.name);
    pt.find('div.hidden-text span.role').html(person.job);
    pt.find('div.hidden-text p.bio-details').html(person.bio);
    pt.find('div.hidden-text p.close-overlay').on('click', function (e) {
      e.preventDefault();
      $('div.hidden-text.bio-overlay').removeClass('bio-overlay');
    });

    // bio popup
    pt.find('span.bio a.bio-overlay').on('click', function (e) {
      e.preventDefault();
      $('div.hidden-text.bio-overlay').removeClass('bio-overlay');
      $(this).parents('div.actions').eq(0).find('div.hidden-text').eq(0).addClass('bio-overlay');
    });
    return pt;
  },

  "setEvents": function () {
    var that = this;

    this.viewmorebutton.on('click', function (e) {
      e.preventDefault();
      that.doViewMore();
    });
  },

  "doViewMore": function () {
    var that = this;

    this.allpage++;
    this.buildAll(this.workingAllPeople);
  },

  "cloneArray": function (array) {
    var that = this,
        ai = 0,
        ac = array.length,
        ta,
        newArray = [],
        newObject;

    do {
      ta = array[ai];

      newObject = {};

      Object.keys(ta).map(function (property) {
        newObject[property] = ta[property];
      });
      newArray.push(newObject);

      /* */

      ai++;
    } while (ai < ac);

    return newArray;
  }
};

window.OfficePeopleIJ = OfficePeopleIJ;

/*
https://www.paulirish.com/2009/throttled-smartresize-jquery-event-handler/
Debounced Resize() jQuery Plugin
August 11th 2009
*/
(function ($, sr) {

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
    var timeout;

    return function debounced() {
      var obj = this,
          args = arguments;
      function delayed() {
        if (!execAsap) func.apply(obj, args);
        timeout = null;
      };

      if (timeout) clearTimeout(timeout);else if (execAsap) func.apply(obj, args);

      timeout = setTimeout(delayed, threshold || 100);
    };
  };
  // smartresize 
  jQuery.fn[sr] = function (fn) {
    return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr);
  };
})(jQuery, 'smartresize');

var supportsFlexBox, ijContext;

$(document).ready(function () {

  window.debugbox = $('#debug');

  ijContext = $('section.office-finder--single').attr('data-context');

  supportsFlexBox = $('html').hasClass('flex');

  // if (supportsFlexBox) {} else {}

  // pageWidth based on browser window width
  var pageWidth = 1;
  if ($(window).width() > 768) {
    pageWidth = 2;
  }
  if ($(window).width() > 1200) {
    pageWidth = 3;
  }

  window.thisOfficePeople = new OfficePeopleIJ({
    "allPeopleBox": $('#people-all'),
    "personTemplate": $('.personTemplate'),
    "viewmorebutton": $('.viewMorePeople'),
    "viewmoreBox": $('.vmClipper'),
    "allPageMax": 6, // display up to 6 items for each page
    "pageWidth": pageWidth,
    "datapath": "../assets/offices/" + ijContext + ".json",
    "portraits": "",
    "flexsupport": supportsFlexBox,
    "bottompad": 35,
    "placeholder" : "../assets/images/placeholder-473x473.png"
  });

  // 
  $(window).smartresize(function () {

    var peopleMaxBoxHeight = 0,
        peopleCumulativeHeight = 0,
        peopleBoxes = $('ul.flexJunior li');

    peopleBoxes.find('.sizer').each(function () {
      if ($(this).height() > peopleMaxBoxHeight) {
        peopleMaxBoxHeight = $(this).height();
      }
    });

    peopleBoxes.css({ "min-height": peopleMaxBoxHeight + 35 + "px" });

    // peopleCumulativeHeight = peopleBoxes.length * (peopleMaxBoxHeight + 20);
    peopleCumulativeHeight = peopleBoxes.length * (peopleMaxBoxHeight + 20);

    $('.viewJunior').css({ "max-height": peopleCumulativeHeight + "px" });
  });
});