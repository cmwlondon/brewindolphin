var platforms = [],
	downloads = [],
	relevantDownloads = [],
	downloadContainer,
	current = {
		"platform" : ""
	},
	thisYear,
	thisMonth,
	mostRecentYear = 0,
	mostRecentMonth =0,
	dlPanelOpen = false,
	archiveMode = false,
	mostRecentDate;

var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

function getDownloads(path,callback) {
  var request = new XMLHttpRequest();

  request.open('GET', path, true);

  request.onload = function() {
    if (request.status >= 200 && request.status < 400) {
      // Success!
      var 	data = JSON.parse(request.responseText);
      platforms = data.platforms,
      downloads = data.items;
      callback();
    } else {
      // We reached our target server, but it returned an error
    }
  };

  request.onerror = function() {
    // There was a connection error of some sort
  };

  request.send();
}

function switchPlatform( platform ) {

	if (platform !== current.platform) {
		if (dlPanelOpen) {
			$('.platform_download_container').animate({"max-height" : "0em"},500,function() {
				// change platform icon highlight
				$('#' + current.platform).removeClass('selected');
				$('#' + platform).addClass('selected');
				current.platform = platform;

				updateDownloads(platform);
				$('.platform_download_container').animate({"max-height" : "100em"},500,function() {  });	
			});	
		} else {
			// change platform icon highlight
			$('#' + current.platform).removeClass('selected');
			$('#' + platform).addClass('selected');
			current.platform = platform;

			updateDownloads(platform);
			$('.platform_download_container').animate({"max-height" : "100em"},500,function() { dlPanelOpen  = true;  });	
		}
	}
}

function updateDownloads(platform) {
	// clear downloads panel
	$('.download_item_list li').not('.download_item_template').remove();

	// archive mode:
	// exclude latest items
	// collect range of dates to build select options
	var nd = filterDownloadsByPlatform(platform);

	nd.sort(function(a,b) {return (a.title < b.title) ? 1 : ((b.title < a.title) ? -1 : 0);} );

	if (nd.length > 0) {
		var itemDates = [];
		var yd = '';
		// in archive mode, get all available dates in return items
		nd.map(function(nd) {
			yd = nd.year.toString();
			if (nd.month < 10) {
				yd = yd + '0';
			}
			yd = yd + nd.month.toString();
			var vx = parseInt(yd,10)
			nd.mark = vx;

			if ( itemDates.indexOf(vx) === -1 ) {
				itemDates.push(vx);
			}
		});

		var nd2 = quickSortMark(nd, 0, nd.length - 1);

		var latestMark = nd2[0].mark;
		current.mark = latestMark; 

		// use this information to build year/month selection
		buildDateSelector(itemDates);

		// show latest items (in archived results)
		buildDownloads(nd2); 
	} 

}

function buildDateSelector(itemDates) {
	//var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
	var sortedDates = quickSort(itemDates,0,itemDates.length - 1);

	$('#datedd option').not(':eq(0)').remove();
	sortedDates.map(function(option,index) {
		var oy = option.toString().substr(0,4),
			om = parseInt(option.toString().substr(4,option.length),10),
			omtext = months[om - 1],
			newOption = $('<option></option>').attr({"value" : option}).text(omtext + ' ' + oy);

		if ( index === 0 ) {
			newOption.attr({"selected" : "selected"});
		}

		$('#datedd').append(newOption);
	});
	
}

function quickSortMark (arr, left, right) {
  var i = left,
    j = right,
    tmp,
    pivotidx = (left + right) / 2,
    pivot = arr[pivotidx.toFixed()].mark;  

  while (i <= j) {
	while (arr[i].mark > pivot) { i++; }
	while (arr[j].mark < pivot) { j--; }
    if (i <= j) { tmp = arr[i]; arr[i] = arr[j]; arr[j] = tmp; i++; j--; }
  }

  /* recursion */
  if (left < j)
    quickSortMark(arr, left, j);
  if (i < right)
    quickSortMark(arr, i, right);
  return arr;
}

function quickSort (arr, left, right) {
	// INT
  var i = left,
    j = right,
    tmp,
    pivotidx = (left + right) / 2,
    pivot = arr[pivotidx.toFixed()];  

  while (i <= j) {
	while (arr[i] > pivot) { i++; }
	while (arr[j] < pivot) { j--; }
    if (i <= j) { tmp = arr[i]; arr[i] = arr[j]; arr[j] = tmp; i++; j--; }
  }

  /* recursion */
  if (left < j)
    quickSort(arr, left, j);
  if (i < right)
    quickSort(arr, i, right);
  return arr;
}

function filterDownloadsByPlatform(platform) {

	var items = [];

	downloads.map(function(item) {

		if ( platform === item.platform) {
			// add check for archive mode and compare against thisYear,thisMonth to find archived items
			if ( archiveMode ) {

				//if ( item.year !== thisYear || item.month !== thisMonth) {
				if ( item.year !== mostRecentYear || item.month !== mostRecentMonth) {
					items.push(item);	
				}
			} else {
				//if ( item.year === thisYear && item.month === thisMonth) {
					if ( item.year === mostRecentYear && item.month === mostRecentMonth) {
					items.push(item);	
				}
			}
		}
	});


	return items;
}

function selectDownloads(platform,year,month) {
	// filter downloads by
	/*
	"platform" : "pl_standardlifeelevate",
	"year" : 2018,
	"month" : 1,
	*/

	relevantDownloads = [];
	downloads.map(function(item) {
		relevantDownloads.push(item);
	});

	relevantDownloads.map(function(download) {
		buildDownload(download);
	});

}

function buildDownloads(items) {
	items.map(function(item) {
		buildDownload(item);
	});
}

function buildDownload(item) {
	var itemTemplate = $('.download_item_template').clone();
	itemTemplate.removeClass('hidden download_item_template');

	var yd = item.year.toString();
	if (item.month < 10) {
		yd = yd + '0';
	}
	yd = yd + item.month.toString();

	itemTemplate.attr({"data-mark" : yd});

	if (archiveMode && yd !== current.mark.toString()) {
		itemTemplate.addClass('hidden');
	}

	itemTemplate.find('p').html(item.title);	
	downloadContainer.append(itemTemplate);
}


function switchDate(date) {
	downloadContainer.animate({"max-height" : "0em"},500,function() {
		var items = downloadContainer.find('li').not('.download_item_template');
		items.each(function(i){
			var im = $(this).attr('data-mark');
			if (im === date) {
				$(this).removeClass('hidden');
			} else {
				$(this).addClass('hidden');
			} 
		});

		downloadContainer.animate({"max-height" : "100em"},500,function() {});
	});
}

function switchArchiveMode() {}

$(document).ready(function(){

	downloadContainer = $('.download_item_list');

	var today = new Date();
	thisYear = today.getFullYear(),
	thisMonth = today.getMonth() + 1; // January = 0
	current.year = thisYear;
	current.month = thisMonth;


	getDownloads('../assets/platforms/downloads.json',function(){
		console.log('downloads loaded');


		downloads.map(function(item) {
			//get the most recent year and month that we have data for	
			if(item.year > mostRecentYear) {
				mostRecentYear = item.year;
				mostRecentMonth = item.month;
			}

			if(item.year == mostRecentYear && item.month > mostRecentMonth) mostRecentMonth = item.month;
		});

		//build most recent date string
		mostRecentDate = months[mostRecentMonth-1] + " " + mostRecentYear;

		//update page title
		document.querySelector('.platform_header > h2').innerHTML = "Latest factsheets - " + mostRecentDate;

		//console.log(mostRecentDate);

		// selectDownloads('',thisYear,thisMonth,false);
	});

	// select platform by clicking on icon
	$('.platform_container ul li').on('click', function(e) {
		e.preventDefault();
		var newID = $(this).attr('id');
		
		switchPlatform( newID );
	});

	// switch between latest and archives items
	$(".dl_switch").on('click',function(e) {
		e.preventDefault();

		// reset selected platform/downloads panel

		if ( $(this).hasClass('archive') ) {
			archiveMode = true;
			//$('.platform_header > h2').innerHTML = "Archived factsheets";
			document.querySelector('.platform_header > h2').innerHTML = "Archived factsheets";
			$('section.article-archive').addClass('archive');
		} else {
			archiveMode = false;
			mostRecentDate = months[mostRecentMonth-1] + " " + mostRecentYear;
			document.querySelector('.platform_header > h2').innerHTML = "Latest factsheets - " + mostRecentDate;
			$('section.article-archive').removeClass('archive');
		}
		updateDownloads(current.platform);
	});

	// switch between dates in archive mode
	$('#datedd').on('change',function(e){
		if ( $(this).val() !== 'null' ) {
			switchDate($(this).val());	
		}
	})
});


