// ISA Value calulator tool

// ----------------------------------------------------------------------------------------------------------------------------------------

window.buildConfigData = function() {
	let data = {
		"labels" : createLabels(startYear, duration),
		"datasets" : []
	};

	// build dataset for flat value line
	let flatOptions = {
		"label" : "flat",
		"borderColor" : "#000",
		"fill" : false,
		"data" : calculateFlatPoints(initialValue, monthlyValue, duration)
	};

	let c1Options = {
		"label" : "compound interest 7.5%",
		"borderColor" : "#f00",
		"backgroundColor" : "rgba(255,0,0,0.5)", 
		"fill" : 2, // fill to c4Options
		"data" : calculateISAValuePoints(initialValue, monthlyValue, duration, 7.5)
	};

	/*
	let c2Options = {
		"label" : "compound interest 5%",
		"borderColor" : "#f00",
		"fill" : false,
		"data" : calculateISAValuePoints(initialValue, monthlyValue, duration, 5)
	};
	*/

	let c3Options = {
		"label" : "compound interest 2.5%",
		"borderColor" : "#f00",
		"fill" : false,
		"data" : calculateISAValuePoints(initialValue, monthlyValue, duration, 2.5)
	};

	let c4Options = {
		"label" : "compound interest 6%",
		"borderColor" : "#00f",
		"backgroundColor" : "rgba(0,0,255,0.5)", 
		"fill" : 3,  // fill to c5Options
		"data" : calculateISAValuePoints(initialValue, monthlyValue, duration, 6)
	};

	let c5Options = {
		"label" : "compound interest 4%",
		"borderColor" : "#00f",
		"backgroundColor" : "rgba(255,0,0,0.5)", 
		"fill" : 4, // fill to c3Options
		"data" : calculateISAValuePoints(initialValue, monthlyValue, duration, 4)
	};

	data.datasets.push(flatOptions); // 0

	// data.datasets.push(c2Options);

	data.datasets.push(c1Options); // 1
	data.datasets.push(c4Options); // 2
	data.datasets.push(c5Options); // 3
	data.datasets.push(c3Options); // 4

	// fill order 1 (red) -> 2 ( blue) -> 3 (red) -> 4

	return data;	
}

window.createLabels = function(start,duration) {
	let labels = [],
		labelIndex = 0;

	do {
		labels.push(labelIndex + start);
		labelIndex++
	} while ( labelIndex < ( duration + 1 ) )

	return labels;
}

window.createData = function() {

}

window.calculateFlat = function(iv,mv,yv) {
	return iv + (mv * 12 * yv);
}

// compound interest only includes initial deposit, not monthly contributions
// iv = initial deposit
// mv = monthly deposit IS NOT CURENTLY USED IN CALCULATION
// yv = number of years
// rv = interest rate
window.calculateCompoundInterest = function(iv, mv, yv, rv) {
	let value = iv * Math.pow( ( 1 + ((rv / 100) / 12) ), (12 * yv));
	return value;
}

// iv = initial deposit
// mv = monthly deposit
// yv = number of years
// rv = interest rate

// P = 5000. PMT = 100. r = 5/100 = 0.05 (decimal). n = 12. t = 10.
// value = [ P(1+r/n)^(nt) ] + [ PMT × (((1 + r/n)^(nt) - 1) / (r/n)) ]

window.calculateISAValue = function(iv, mv, yv, rv) {
	let n1 = Math.pow( 1 + ( rv / 1200 ), (12 * yv) ),
		v1 = iv * ( n1 ),
		v2 = mv * ( (n1  - 1) / ( rv / 1200 ) ),
		value = v1 + v2;

	return value;
}

window.calculateFlatPoints = function(iv,mv,yv) {
	let points = [],
		yearIndex = 0;

	do {
		points.push(calculateFlat(iv,mv,yearIndex));
		yearIndex++;
	} while (yearIndex < (yv + 1))

	return points;
};

window.calculateCompoundInterestPoints = function(iv,mv,yv,rv) {
	let points = [],
		yearIndex = 0;

	do {
		points.push( calculateCompoundInterest(iv,mv,yearIndex,rv) );
		yearIndex++;
	} while (yearIndex < (yv + 1))

	return points;
}

window.calculateISAValuePoints = function(iv,mv,yv,rv) {
	let points = [],
		yearIndex = 0;

	do {
		points.push( calculateISAValue(iv,mv,yearIndex,rv) );
		yearIndex++;
	} while (yearIndex < (yv + 1))

	return points;
}

window.updateChart = function() {

	// console.log("updateChart duration %s data.length %s", duration, myLine.data.datasets[0].data.length);

	/*
	// need to flush data points based on duration
	if ( currentDuration === duration ) {
		console.log('duration update 1');

		let newFlatPoints = calculateFlatPoints(initialValue, monthlyValue, duration),
		c1 = calculateISAValuePoints(initialValue, monthlyValue, duration, 7.5),
		c2 = calculateISAValuePoints(initialValue, monthlyValue, duration, 6),
		c3 = calculateISAValuePoints(initialValue, monthlyValue, duration, 4),
		c4 = calculateISAValuePoints(initialValue, monthlyValue, duration, 2.5);
		
		let yearIndex = 0;
		
		do {
			myLine.data.datasets[0].data[yearIndex] = newFlatPoints[yearIndex];
			myLine.data.datasets[1].data[yearIndex] = c1[yearIndex];
			myLine.data.datasets[2].data[yearIndex] = c2[yearIndex];
			myLine.data.datasets[3].data[yearIndex] = c3[yearIndex];
			myLine.data.datasets[4].data[yearIndex] = c4[yearIndex];

			yearIndex++;
		} while (yearIndex < (duration + 1))

		window.myLine.update();
	} else {
		console.log('duration update 2');

		config.data = buildConfigData();
		window.myLine.update(config);
	}
	*/

	config.data = buildConfigData();
	window.myLine.update(config);

	window.graphcaptured = false;
}

// ----------------------------------------------------------------------------------------------------------------------------------------

// (function(global) {
// } (this));

// ----------------------------------------------------------------------------------------------------------------------------------------

window.graphcaptured = false;
window.chart;

window.initialValue = 5000;
window.monthlyValue = 500;
window.startYear = 2019;
window.duration = 15;
window.rate = 5;
window.currentDuration = duration;

// chart configuration object
window.config = {
	type: 'line',
	data: buildConfigData(),
	options: {
		elements:{
			point: {
				radius: 0
			},
		},
        legend: {
            display: false,
        },
        plugins: {
            filler: {
                propagate: true
            }
        },
		animation: {
		  onComplete: function() {
		  	/*
		  	Note: this event fires every time the graph canvas element is updated: resize events, hover events, graph update events
		  	*/

		  	if (!window.graphcaptured) {
				window.graphcaptured = true;
		  	}

		  }
		},
		responsive : true,
		aspectratio : 2,
		/*
		title: {
			display: true,
			text: 'ISA calculator'
		},
		*/
		scales: {
			xAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Year'
				}
			}],
			yAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Value (£)',
				},
				ticks: {
					// beginAtZero: true,
					callback: function(value, index, values) {
						if(parseInt(value) >= 1000){
							return '£' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						} else {
							return '£' + value;
						}
					}
				}
			}]
		},
	}
};

/*
google chart version of ISA value graph
to-do:
	redraw chart on window.resize event
	redraw chart on arbitrary event: data update
	fill between lines:
		7-5% - 6% red
		6% - 4% blue
		4% - 2.5% red
	reformat horizontal axis labels, string '2019' instead of 2,019
	reformat vertical axis labels (£100,000)
	reposition/remove line legends
*/

function drawGoogleChart() {
	console.log('drawGoogleChart');

    /*
    var data = google.visualization.arrayToDataTable([
      ['Year', 'Sales', 'Expenses'],
      ['2004',  1000,      400],
      ['2005',  1170,      460],
      ['2006',  660,       1120],
      ['2007',  1030,      540]
    ]);
	*/

	// generate dummy data set for graph
    let iv = 5000, // inital contribution
    	mv = 500, // monthly contribution
    	duration = 15, // over 15 years
    	startYear = 2019,
    	yearIndex = 0;

    /*
    let graphData = [
	    	['year', 'flat', '2.5%', '4%', '5%', '6%', '7.5%']
	    ];

    do {
    	let points = [
    		startYear + yearIndex,
    		calculateFlat(iv, mv, yearIndex),
    		calculateISAValue(iv, mv, yearIndex, 2.5),
    		calculateISAValue(iv, mv, yearIndex, 4),
    		calculateISAValue(iv, mv, yearIndex, 5),
    		calculateISAValue(iv, mv, yearIndex, 6),
    		calculateISAValue(iv, mv, yearIndex, 7.5)
    	];

    	graphData.push(points);

    	yearIndex++;
    } while (yearIndex < (duration + 1))

    var data = google.visualization.arrayToDataTable( graphData );
	*/

	let graphData = [];
    do {
    	let points = [
    		(startYear + yearIndex).toString(),
    		calculateFlat(iv, mv, yearIndex),
    		calculateISAValue(iv, mv, yearIndex, 2.5),
    		calculateISAValue(iv, mv, yearIndex, 4),
    		calculateISAValue(iv, mv, yearIndex, 5),
    		calculateISAValue(iv, mv, yearIndex, 6),
    		calculateISAValue(iv, mv, yearIndex, 7.5)
    	];

    	graphData.push(points);

    	yearIndex++;
    } while (yearIndex < (duration + 1))

	let data = new google.visualization.DataTable();
	data.addColumn('string', 'Year');
	data.addColumn('number', 'flat');
	data.addColumn({ "type" : "number", "label" : "2.5%" });
	data.addColumn({ "type" : "number", "label" : "4%" });
	data.addColumn({ "type" : "number", "label" : "5%" });
	data.addColumn({ "type" : "number", "label" : "6%" });
	data.addColumn({ "type" : "number", "label" : "7.5%" });
	data.addRows( graphData );

	// { "vAxis" :{ "format" : "currency" } }

    let options = {
		"width" : "100%",
		"title" : "ISA Value (Google Charts Line Chart)",
		"curveType" : "function",
		//"legend" : { "position" : "bottom" },
		"legend" : { "position" : "none" },
		"vAxis" : { "format" : "£###,###,###" } // format y axis labels as £
    };

    // format data points on chart - add '£' prefix to all values
	let formatter = new google.visualization.NumberFormat({
		prefix: '£',
	});
	formatter.format(data, 1);
	formatter.format(data, 2);
	formatter.format(data, 3);
	formatter.format(data, 4);
	formatter.format(data, 5);
	formatter.format(data, 6);

    chart = new google.visualization.LineChart(document.getElementById('googleChart1'));
    // let chart = new google.visualization.AreaChart(document.getElementById('googleChart1'));

    /* initial chart draw*/
    chart.draw(data, options);
    /* call draw() to update the chart if you change the data */
	
}

function redrawGoogleChart() {
}

// ----------------------------------------------------------------------------------------------------------------------------------------

$(document).ready(function() {

	// load google charts library and set callback to run when library has loaded
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback( drawGoogleChart );
	// google.charts.load('current', {'packages':['corechart']});
	// google.charts.setOnLoadCallback(drawGoogleChart);

	// redraw chart if browser window width changes
	$(window).resize(function(){
	  drawGoogleChart();
	});

	// initialise chart.js and render graph
	let ctx = document.getElementById('lineChartExample').getContext('2d');
	window.myLine = new Chart(ctx, config);

	// setup intial, monthly deposit and duration sliders
	$( "#initialSlider" ).slider({
		min : 0,
		max : 50000,
		values: [ initialValue ],
		/*
		start : function( event, ui ) {},
		stop : function( event, ui ) {},
		slide : function( event, ui ) {},
		create : function( event, ui ) {},
		*/
		slide : function( event, ui ) {
			$('#iv').val( ui.value );
		},
		change : function( event, ui ) {
			$('#iv').val( ui.value );
			initialValue = ui.value;
			
			updateChart();
		}
	});

	$( "#monthlySlider" ).slider({
		min : 0,
		max : 5000,
		values: [ monthlyValue ],
		slide : function( event, ui ) {
			$('#mv').val( ui.value );
		},
		change : function( event, ui ) {
			$('#mv').val( ui.value );
			monthlyValue = ui.value;

			updateChart();
		}
	});

	$( "#durationSlider" ).slider({
		min : 0,
		max : 50,
		values: [ duration ],
		slide : function( event, ui ) {
			$('#dv').val( ui.value );
		},
		change : function( event, ui ) {
			$('#dv').val( ui.value );
			duration = ui.value;

			updateChart();
		}
	});

	// copy start values into text fields
	$('#iv').val(initialValue);
	$('#mv').val(monthlyValue);
	$('#dv').val(duration);

});

// ----------------------------------------------------------------------------------------------------------------------------------------

