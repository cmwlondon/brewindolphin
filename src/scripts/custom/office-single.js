  /* ------------------------------------------------ */
  // Queue
  /* ------------------------------------------------ */

  function Queue(parameters) {
    this.parameters = parameters;
    this.items = parameters.items || [];
    this.settings = parameters.settings || {};
    this.actions = parameters.actions || {};

    this.itemCount = 0;
    this.activeWorkers = 0;
    this.workerIndex = 0;
    this.workerLimit = 5;
    this.workers = [];
  
    this.init();
  }
  
  Queue.prototype = {
    "constructor" : Queue,
    "template" : function () {var that = this; },
    
    "init" : function () {
      var that = this;

      // console.log('Queue.init');
      // console.log(this.settings);
    },
    "addItem" : function (newItem) {
      var that = this;

      this.items.push(newItem);
      this.itemCount++;
    },
    "clearQueue" : function () {
      var that = this;

      this.workers = [];
      this.itemCount = 0;
    },
    "startQueue" : function () {
      var that = this,
        worker;

      this.actions.queueStart(this);

      while (this.items.length > 0 && this.activeWorkers < this.workerLimit ) {
        worker = this.items.pop();
        this.workerStart(worker);
      }
    },
    "stopQueue" : function () {
      var that = this;
    },
    "workerStart" : function (worker) {
      var that = this;

      this.activeWorkers++;
      worker.wi = this.workers.length;
      this.workers[this.workers.length] = worker;

      this.actions.itemStart(this, worker);

    },
    "workerComplete" : function (worker) {
      var that = this,
        worker;

      this.actions.itemComplete(this,worker);

      // clear finished worker
      this.activeWorkers--;
      this.workers.splice(worker.wi);

      // are there items left, and worker slots available, if so start unassigned item 
      while (this.items.length > 0 && this.activeWorkers < (this.workerLimit + 1) ) {
        worker = this.items.pop();
        this.workerStart(worker);
      }

      // all items done
      if (this.activeWorkers == 0 && this.items.length == 0) {
        this.queueComplete();
      }

    },
    "queueComplete" : function () {
      var that = this;

      this.actions.queueComplete(this);
    }
  }

  window.Queue = Queue;

/* ------------------------------------------------ */
// OfficePeople
/* ------------------------------------------------ */

function OfficePeople(parameters) {
  this.parameters = parameters;

  this.seniorHeadBox = parameters.seniorHeadBox;
  this.seniorPeopleBox = parameters.seniorPeopleBox;
  this.allPeopleBox = parameters.allPeopleBox;
  this.namefilter = parameters.namefilter;
  this.jobfilter = parameters.jobfilter;
  this.viewmorebutton = parameters.viewmorebutton;
  this.viewmoreBox = parameters.viewmoreBox;

  this.seniorTemplate = parameters.seniorTemplate;
  this.personTemplate = parameters.personTemplate;
  this.allPageMax = parameters.allPageMax;
  this.pageWidth = parameters.pageWidth;
  this.datapath = parameters.datapath;
  this.portraits = parameters.portraits;
  this.placeholder = parameters.placeholder;

  this.seniorHead = [];
  this.seniorPeople = [];
  this.allPeople = [];
  this.workingAllPeople = [];
  this.allpage = 0;
  this.searchterm = '';
  this.currentFilter = 'all';

  this.init();
}

OfficePeople.prototype = {
  "constructor" : OfficePeople,

  "template" : function () { var that = this; },
  "test" : function () { var that = this; console.log("OfficePeople.test"); },
  "init" : function () {

    var that = this;

    this.loadPeople();
  },    

  "loadPeople" : function () {
    var that = this;

    /*
    $.getJSON(this.datapath, function(data) {
      that.buildPeople(data);
      that.setEvents();
    });
    */
    that.buildPeople(empdata);
    that.setEvents();
  },    

  "buildPeople" : function (data) {
    var that = this;

    // seperate people into senior head of office, senior and junior(all) teams
    data.people.map(function(person) {
      if (person.head === true) {
        this.seniorHead.push(person);
      }
      else if (person.senior === true && person.head === false) {
        this.seniorPeople.push(person);
      } else {
        this.allPeople.push(person);
      }
    },this);

    // head of office may not always be assigned
    if ( this.seniorHead.length > 0 ) {
      this.buildSeniorHead();
      $('body').addClass('headOfOfficePresent');
    } else {
      /*
      handle case where no senior head has been assigned - change layout of senior team section of page:
      omit head of office block .viewSeniorHead
      extend senior team sliderr to span full width of page .viewSenior
      */
      $('body').addClass('noHeadOfOffice');
      // $(this.seniorHeadBox).parents('.office-finder__people__content').addClass("headless");
    }
 
    // handle case where there are no senior team members
    if (this.seniorPeople.length > 0) {
      $('body').addClass('seniorTeamPresent');
      this.buildSenior();
    } 

    // handle case where there are no junior team members
    // junior team block only displayed if there are team members to show
    if (this.allPeople.length > 0) {
      $('body').addClass('juniorTeamPresent');
      this.workingAllPeople = this.cloneArray(this.allPeople);  
      // display poeple on page load - TEST ONLY
      /**/
      // this.buildAll(this.workingAllPeople);
      /**/
    }

  },

  // Adding Senior Head of office to a page

  "buildSeniorHead" : function () {
    var that = this;

    var seniorHeadQueue = new Queue({
      "settings" : {
        "container" : that.seniorHeadBox,
        "clipper" : $('.viewSeniorHead')
      },
      "actions" : {
        "itemStart" : function(queue, worker){
          // build new senior person, attach callback to image onload event to mark this worker as done, move to new worker until all workers are done
          var newPerson = that.buildSeniorPerson(worker.index, worker.person,{"queue" : queue, "worker" : worker, "action" : function(queue, worker){
            queue.workerComplete(worker);
          }});
 
          queue.settings.container
          .append(newPerson);
        },
        "itemComplete" : function(queue, worker){
          /*
          var vh = queue.settings.container.height();
          if ( vh > queue.boxheight ) {
            queue.boxheight = vh;
          }
          */
        },
        "queueStart" : function(queue){
          // queue.boxheight = 0;
        },
        "queueComplete" : function(queue){
          /*
          // normalize heights of boxes
          var seniorMaxBoxHeight = 0,
              seniorBoxes = $('ul.flexSenior li');
          seniorBoxes.find('.sizer').each(function(){
            if ( $(this).height() > seniorMaxBoxHeight) { seniorMaxBoxHeight = $(this).height(); }
          });
          seniorBoxes.css({"min-height" : (seniorMaxBoxHeight + 10) + "px"});

          // window.requestAnimationFrame(that.setSeniorMaxHeight);
          */
          // open bio overlay for head of office
          $('.viewSeniorHead .bio-overlay').on('click', function(event) {
            var allOverlays = $('.office-finder__people__content .hidden-text')
            var overlayOpen = allOverlays.hasClass('bio-overlay');
            var target = $('.viewSeniorHead .hidden-text');

            if (overlayOpen === true) {
              $('.hidden-text').removeClass('bio-overlay');
              target.toggleClass('bio-overlay');
            } else {
              target.toggleClass('bio-overlay');
            }
          });

          $('.viewSeniorHead .close-overlay').on('click', function() {
            var target = $('.viewSeniorHead .hidden-text');
            target.removeClass('bio-overlay');
          });

          // open email form for head of office
          if ( $('.viewSeniorHead .emailform_trigger').length > 0 ) {
            console.log(".viewSeniorHead .emailform_trigger");
            $('.viewSeniorHead .emailform_trigger').on('click',function(e){
              e.preventDefault();

              var overlayNode = $('#emailform');
              overlayNode.removeClass('showEnd');
              overlayNode.find('form').eq(0).trigger("reset");
              overlayNode.find('.has-error').each(function(i) {
                console.log('x');
                $(this).removeClass('has-error');
                $(this).find('.messages').empty();
              });

              // for forms with radio button groups reset all redio buttons    
              // .clientYesNo > input[type="radio"]
              $('.clientYesNo input').each(function(i) {
                $(this).attr({"checked" : false});
              });

              var dataName = $(this).attr('data-name'),
                  dataEmail = $(this).attr('data-email');

              var overlayNode = $('#emailform');
              overlayNode.find('.personName').eq(0).html( dataName );

              // store persons name in hidden field in form
              $('#tname').val(dataName);
              $('#temail').val(dataEmail);

              // show overlay
              $('body').addClass('emailform_open');
            });

            $('#emailform .closer').on('click',function(e){
              e.preventDefault();
              $('body').removeClass('emailform_open');
            });
          }

          queue.settings.clipper.css({"max-height" : "100000px"});
        }
      }
    });

    seniorHeadQueue.addItem({
      "person" : this.seniorHead[0],
      "index" : 1
    });

    seniorHeadQueue.startQueue();

  },

  // Adding Senior Members of office to a page

  "buildSenior" : function () {
    var that = this;

    var seniorQueue = new Queue({
      "settings" : {
        "container" : $('.slick-slider'), //that.seniorPeopleBox,
        "clipper" : $('.viewSenior')
      },
      "actions" : {
        "itemStart" : function(queue, worker){
          // build new senior person, attach callback to image onload event to mark this worker as done, move to new worker until all workers are done
          var newPerson = that.buildSeniorPerson(worker.index, worker.person,{"queue" : queue, "worker" : worker, "action" : function(queue, worker){
            queue.workerComplete(worker);
          }});
 
          $('.slick-slider').append(newPerson);
          // queue.settings.container.append(newPerson);
        },
        "itemComplete" : function(queue, worker){
          /*
          var vh = queue.settings.container.height();
          if ( vh > queue.boxheight ) {
            queue.boxheight = vh;
          }
          */
        },
        "queueStart" : function(queue){
          // queue.boxheight = 0;
        },
        "queueComplete" : function(queue){
          /*
          // normalize heights of boxes
          var seniorMaxBoxHeight = 0,
              seniorBoxes = $('ul.flexSenior li');
          seniorBoxes.find('.sizer').each(function(){
            if ( $(this).height() > seniorMaxBoxHeight) { seniorMaxBoxHeight = $(this).height(); }
          });
          seniorBoxes.css({"min-height" : (seniorMaxBoxHeight + 10) + "px"});

          // window.requestAnimationFrame(that.setSeniorMaxHeight);
          */
          queue.settings.clipper.css({"max-height" : "100000px"});

          console.log('senior team complete');

          var pause = window.setTimeout(function(){
            $('.slick-slider').on('setPosition', function(event, slick){
              $(window).resize(function(){
                var overlayOpen = $('.office-finder__people__content .hidden-text').hasClass('bio-overlay');
                if (overlayOpen === true) {
                  console.log('overlay open');
                  $('.hidden-text').removeClass('bio-overlay');
                }
              });
              $('.slick-arrow').on('click', function() {
                var overlayOpen = $('.office-finder__people__content .hidden-text').hasClass('bio-overlay');
                if (overlayOpen === true) {
                  console.log('overlay open');
                  $('.hidden-text').removeClass('bio-overlay');
                }
              });
            });

            $('.slick-slider').on('init', function(event, slick){
              $('.slick-arrow').on('click', function() {
                var overlayOpen = $('.office-finder__people__content .hidden-text').hasClass('bio-overlay');
                if (overlayOpen === true) {
                  console.log('overlay open');
                  $('.hidden-text').removeClass('bio-overlay');
                }
              });
            });

            // tailor slider for cases with and without head of office 2/3
            var seniorTeamToShow = (that.seniorHead.length !== 0) ? 2 : 3;
            // hide dots in dekstop view if there are only a few people
            var showDots = (that.seniorPeople.length > seniorTeamToShow);

            $('.slick-slider').slick({
              infinite: false,
              slidesToShow: seniorTeamToShow,
              dots: showDots,
              accessibility: false,
              responsive: [
                {
                  breakpoint: 992,
                  settings: {
                    slidesToShow: 1,
                    dots: true,
                  }
                }
              ]
            });

            /* overl START */
          $('.bio-overlay').on('click', function(event) {
            var overlayOpen = $('.office-finder__people__content .hidden-text').hasClass('bio-overlay');
            if (overlayOpen === true) {
              $('.hidden-text').removeClass('bio-overlay');
              var target = $(event.target).parents('div').eq(0).find('.hidden-text');
              target.toggleClass('bio-overlay');
            } else {
              var target = $(event.target).parents('div').eq(0).find('.hidden-text');
              target.toggleClass('bio-overlay');
            }
          });

          $('.close-overlay').on('click', function() {
            $('.hidden-text').removeClass('bio-overlay');
          });
            /* overl END */
          },10);
          /**/
          /**/

          $('.slick-arrow').on('click', function() {
            $('.hidden-text').removeClass('bio-overlay');
          });

          $('.slick-arrow').on('click', function() {
            var overlayOpen = $('.office-finder__people__content .hidden-text').hasClass('bio-overlay');
            if (overlayOpen === true) {
              $('.hidden-text').removeClass('bio-overlay');
            }
          });

          $('.slick-slider').on('init', function(event, slick){
            $(window).resize(function(){
              var overlayOpen = $('.office-finder__people__content .hidden-text').hasClass('bio-overlay');
              if (overlayOpen === true) {
                $('.hidden-text').removeClass('bio-overlay');
              }
            });
            $('.slick-arrow').on('click', function() {
              var overlayOpen = $('.office-finder__people__content .hidden-text').hasClass('bio-overlay');
              if (overlayOpen === true) {
                $('.hidden-text').removeClass('bio-overlay');
              }
            });
          });

          if ( $('.emailform_trigger').length > 0 ) {
            $('.emailform_trigger').on('click',function(e){
              e.preventDefault();

              var overlayNode = $('#emailform');
              overlayNode.removeClass('showEnd');
              overlayNode.find('form').eq(0).trigger("reset");
              overlayNode.find('.has-error').each(function(i) {
                console.log('x');
                $(this).removeClass('has-error');
                $(this).find('.messages').empty();
              });

              // for forms with radio button groups reset all redio buttons    
              // .clientYesNo > input[type="radio"]
              $('.clientYesNo input').each(function(i) {
                $(this).attr({"checked" : false});
              });

              var dataName = $(this).attr('data-name'),
                  dataEmail = $(this).attr('data-email');

              var overlayNode = $('#emailform');
              overlayNode.find('.personName').eq(0).html( dataName );

              // store persons name in hidden field in form
              $('#tname').val(dataName);
              $('#temail').val(dataEmail);

              // show overlay
              $('body').addClass('emailform_open');
            });

            $('#emailform .closer').on('click',function(e){
              e.preventDefault();
              $('body').removeClass('emailform_open');
            });
          }          
          /*
          $('.slick-slider').on('init', function(event, slick, direction){
            console.log('init');
            var hw = $('.slick-slider .slick-slide').width()
            console.log( $('.slick-slider .slick-slide').width() );

            // left
          });
          */
          /**/         
        }
      }
    });

    var workingMax = this.seniorPeople.length;
    for (i = 0; i < workingMax; i++) {
      person = this.seniorPeople[i];

      seniorQueue.addItem({
        "person" : person,
        "index" : i
      });
    }

    seniorQueue.startQueue();

  },

  "setSeniorMaxHeight" : function () {
    var that = this;
    // $('.viewSenior').css({"max-height" : $('ul.flexSenior').height() + "px"}); 
    $('.viewSeniorHead').css({"max-height" : "10000px"});
    $('.viewSenior').css({"max-height" : "10000px"});
  },    

  "buildAll" : function (people) {
    var that = this;

    if (people.length > 0) {
      // display first six
      var person,
          offset = (this.allpage * this.allPageMax),
          workingMax = ( (offset + this.allPageMax) > people.length) ? (this.workingAllPeople.length - offset) : this.allPageMax;

      var juniorQueue = new Queue({
        "settings" : {
          "container" : this.allPeopleBox,
          "clipper" : $('.viewJunior'),
          "offset" : offset,
          "workingMax" : workingMax
        },
        "actions" : {
          "itemStart" : function(queue, worker){
            // build new senior person, attach callback to image onload event to mark this worker as done, move to new worker until all workers are done
            var newPerson = that.buildPerson(worker.index, worker.person,{"queue" : queue, "worker" : worker, "action" : function(queue, worker){
              queue.workerComplete(worker);
            }});
   
            queue.settings.container
            .append(newPerson);
          },
          "itemComplete" : function(queue, worker){
            /*
            var vh = queue.settings.container.height();
            if ( vh > queue.boxheight ) {
              queue.boxheight = vh;
            }
            */
          },
          "queueStart" : function(queue){
            // queue.boxheight = 0;
            that.viewmoreBox.addClass('closed');
          },
          "queueComplete" : function(queue){
            // queue.settings.clipper.css({"max-height" : "100000px"});

            var juniorMaxBoxHeight = 0,
                juniorCumulativeHeight = 0,
                juniorBoxes = $('ul.flexJunior li');

            juniorBoxes.find('.sizer').each(function(){
              if ( $(this).height() > juniorMaxBoxHeight) { juniorMaxBoxHeight = $(this).height(); }
            });
            juniorBoxes.css({"min-height" : (juniorMaxBoxHeight + 10) + "px"});
            juniorCumulativeHeight = juniorBoxes.length * (juniorMaxBoxHeight + 20);
            
            $('.viewJunior').css({"max-height" : juniorCumulativeHeight + "px"}); 
            // queue.settings.clipper.css({"max-height" : $('ul.flexJunior').height() + "px"}); 

            if ( workingMax == that.allPageMax || (offset + this.allPageMax) <= people.length) {
              that.viewmoreBox.removeClass('closed');
            }

            $('.bio-overlay').on('click', function(event) {
              var overlayOpen = $('.office-finder__people__content .hidden-text').hasClass('bio-overlay');
              if (overlayOpen === true) {
                $('.hidden-text').removeClass('bio-overlay');
                var target = $(event.target).parents('div').eq(0).find('.hidden-text');
                target.toggleClass('bio-overlay');
              } else {
                var target = $(event.target).parents('div').eq(0).find('.hidden-text');
                target.toggleClass('bio-overlay');
              }
            });

            $('.close-overlay').on('click', function() {
              $('.hidden-text').removeClass('bio-overlay');
            });

            if ( $('.emailform_trigger').length > 0 ) {
              $('.emailform_trigger').on('click',function(e){
                e.preventDefault();

                var overlayNode = $('#emailform');
                overlayNode.removeClass('showEnd');
                overlayNode.find('form').eq(0).trigger("reset");
                overlayNode.find('.has-error').each(function(i) {
                  console.log('x');
                  $(this).removeClass('has-error');
                  $(this).find('.messages').empty();
                });

                // for forms with radio button groups reset all redio buttons    
                // .clientYesNo > input[type="radio"]
                $('.clientYesNo input').each(function(i) {
                  $(this).attr({"checked" : false});
                });

                var dataName = $(this).attr('data-name'),
                    dataEmail = $(this).attr('data-email');

                var overlayNode = $('#emailform');
                overlayNode.find('.personName').eq(0).html( dataName );

                // store persons name in hidden field in form
                $('#tname').val(dataName);
                $('#temail').val(dataEmail);

                // show overlay
                $('body').addClass('emailform_open');
              });

              $('#emailform .closer').on('click',function(e){
                e.preventDefault();
                $('body').removeClass('emailform_open');
              });
            }          

          }
        }
      });

      /* Shows Limited number of people */

      /*
        for(i = 0; i < workingMax; i++) {
          person = people[ offset + i ];

          juniorQueue.addItem({
            "person" : person
          });
        }
      */

      /* Shows All people filtered by the search */

      /* people.map(function(person){
        // add new worker
        juniorQueue.addItem({
          "person" : person
        });
      },this); */

      workingMax = people.length; 
      for (var i = 0; i < workingMax; i++) {
        var person = people[i];

        juniorQueue.addItem({
          "person" : person,
          "index" : i
        });
      }

      juniorQueue.startQueue();
    } else {
      // no people therefore no 'view more' button
      this.viewmoreBox.addClass('closed');
    }

  },    

  "buildSeniorPerson" : function (index,person,imageOnLoad) {
    
    // this.seniorTemplate
    var that = this;
    var pt = this.seniorTemplate.clone().removeClass('personTemplate').removeClass('hidden');

    /*
    var newImg = $('<img></img>')
    // .attr({ "src" : "../assets/images/offices/people/" + person.pic, "alt" : person.name})
    .attr({ "src" : "https://www.brewin.co.uk" + person.pic, "alt" : person.name})
    .addClass('portrait')
    .on('load',function(e){
      imageOnLoad.action(imageOnLoad.queue,imageOnLoad.worker);
    });
    */

    // image loader
    var newImg = $('<img></img>')
    .attr({ "src": this.portraits + person.pic, "alt": person.name })
    .addClass('portrait')
    .on('load', function(e) {
      // image loaded, move on to next job
      imageOnLoad.action(imageOnLoad.queue, imageOnLoad.worker);
    })
    .on('error', function(e) {
      // image did not load
      console.log('broken image');
      $(this).addClass('brokenImage');
      $(this).attr({ "src" : that.placeholder});
      imageOnLoad.action(imageOnLoad.queue, imageOnLoad.worker);
    });

    pt.find('div.frame').append(newImg);
    // pt.find('div.frame img').attr({"src" : "../assets/images/offices/people/" + person.pic, "alt" : person.name});
    pt.find('div.person-content h4.name').text(person.name);
    pt.find('div.person-content span.role').text(person.job);
    pt.find('div.person-content span.phone').html(person.phone);

    // remove email link and preceeding forward slash if the email field for this person is blank or does not exist
    if (!person.email || person.email.length === 0 || person.email === '') {
      pt.find('span.email, span.forward').remove();
    } else {
      pt.find('span.email a').attr({"href" : "-", "data-name" : person.name, "data-email" : person.email}).text('Email');
      // pt.find('span.email a').attr({"href" : "mailto:" + person.email}).text('Email');
    }

    // overlay content
    pt.find('div.hidden-text').html('<h4>'+person.name+'</h4><span class="role">'+person.job+'</span><p class="bio-details">'+person.bio+'</p>' + '<p class="close-overlay"></p>');

    return pt;
  },

  "buildPerson" : function (index, person,imageOnLoad) {
    var that = this;

    var pt = this.personTemplate.clone().removeClass('personTemplate');

    /*
    var newImg = $('<img></img>')
    .attr({ "src" : this.portraits + person.pic, "alt" : person.name})
    .addClass('portrait')
    .on('load',function(e){
      imageOnLoad.action(imageOnLoad.queue,imageOnLoad.worker);
    });
    */
    
    // image loader
    var newImg = $('<img></img>')
    .attr({ "src": this.portraits + person.pic, "alt": person.name })
    .addClass('portrait')
    .on('load', function(e) {
      // image loaded, move on to next job
      imageOnLoad.action(imageOnLoad.queue, imageOnLoad.worker);
    })
    .on('error', function(e) {
      // image did not load
      console.log('broken image');
      $(this).addClass('brokenImage');
      $(this).attr({ "src" : that.placeholder});
      imageOnLoad.action(imageOnLoad.queue, imageOnLoad.worker);
    });

    //pt.find('img').attr({"src" : this.portraits + person.pic, "alt" : person.name})
    pt.find('div.frame').append(newImg);
    pt.find('h4.name').html(person.name);
    pt.find('span.role').html(person.job);
    pt.find('span.phone').html(person.phone);

    // remove email link and preceeding forward slash if the email field for this person is blank or does not exist
    if (!person.email || person.email.length === 0 || person.email === '') {
      pt.find('span.email, span.forward').remove();
    } else {
      pt.find('span.email a').attr({"href" : "-", "data-name" : person.name, "data-email" : person.email}).text('Email');
    }

    // overlay content
    pt.find('div.hidden-text').html('<h4>'+person.name+'</h4><span class="role">'+person.job+'</span><p class="bio-details">'+person.bio+'</p>' + '<p class="close-overlay"></p>');

    // if (person.linkedin !== '') {
    //   pt.find('span.linkedin a').attr({"href" : person.linkedin}).text(person.linkedin);
    // } else {
    //   pt.find('span.linkedin').remove();
    // }

    // if (person.title !== '') {
    //   pt.find('h3.title').append(person.title);
    // } else {
    //   pt.find('h3.title').remove();
    // }

    return pt;
  },    

  "doViewMore" : function () {
    var that = this;

    this.allpage++;
    this.buildAll(this.workingAllPeople);
  },    

  "doNameFilter" : function (name) {
    var that = this,
        name = $("input[name='namesearch']").val();

      // build regex
      var searchRegEx = new RegExp(name, 'i');

      this.resetBox(); // close all people area

      // wait 1 second for box close animation to run
      var test1 = window.setTimeout(function() {
        that.allPeopleBox.empty();

        var presearch = that.cloneArray(that.allPeople);
        that.workingAllPeople = [];
        presearch.map(function(person) {
          if ( searchRegEx.test(person.name) ) {
            that.workingAllPeople.push(person);    
          }
        },this);        

        that.searchterm = name;
        that.currentFilter = 'x';

        if (that.workingAllPeople.length > 0) {
          $('.nomatchClipper').addClass('closed');
          that.buildAll(that.workingAllPeople);
          } else {
          $('.nomatchClipper').removeClass('closed');
        }
      },1000);

  },    

  "doJobFilter" : function (job) {
    var that = this;

    $('.nomatchClipper').addClass('closed');
    if (this.currentFilter !== job) {
      this.resetBox();

      var test1 = window.setTimeout(function() {
        that.allPeopleBox.empty();
        var presearch = that.cloneArray(that.allPeople);
        if ( job !== 'all' ) {
          that.workingAllPeople = [];
          presearch.map(function(person) {
            if ( job === person.filter ) {
              this.workingAllPeople.push(person);  
            }
          },that);
        } else {
          that.workingAllPeople = presearch;
        }
        that.buildAll(that.workingAllPeople);

        var test2 = window.setTimeout(function() { that.currentFilter = job; }, 1000);
      },1000);
    }
  },    

  "doCombinedFilter" : function () {
    var that = this,
        name = $("input[name='namesearch']").val(),
        job = this.jobfilter.val();

      $('.nomatchClipper').addClass('closed');
      this.resetBox();
      var test1 = window.setTimeout(function() {
        that.allPeopleBox.empty();

        // filter on name -> set1
        if (name !== '' && typeof name !== 'null' && name.length > 0) {
          // name not empty
          var searchRegEx = new RegExp(name, 'i');
          var presearch1 = that.cloneArray(that.allPeople);

          that.workingAllPeople1 = [];
          presearch1.map(function(person) {
            if (searchRegEx.test(person.name) === false) {
              $('.nomatchClipper').removeClass('closed');
            }
            if ( searchRegEx.test(person.name) ) {
              that.workingAllPeople1.push(person);
            }
          },this);        
        } else {
          that.workingAllPeople1 = that.cloneArray(that.allPeople);
        }

        if( that.workingAllPeople1.length > 0) {
          // 'null','all','financial','investment'
          // filter ste1 based on job -> set2
          if ( job !== 'all' && job !== 'null' ) {
            that.workingAllPeople = [];
            var presearch2 = that.cloneArray( that.workingAllPeople1);
            presearch2.map(function(person) {
              if ( job === person.filter ) {
                this.workingAllPeople.push(person);  
              }
            },that);
          } else {
            that.workingAllPeople = that.cloneArray( that.workingAllPeople1);
          }
          // store set 2 and render first 6 items

          if (that.workingAllPeople.length > 0) {

            $('.nomatchClipper').addClass('closed');
            that.buildAll(that.workingAllPeople);
            } else {
            $('.nomatchClipper').removeClass('closed');
          }
        }

      },1000);      
  },

  "resetBox" : function (person) {
    var that = this;
    // reset state of all people are
    $('.viewJunior').css({"max-height" : "0px"});
    this.viewmoreBox.addClass('closed');
    this.allpage = 0;
  },    

  "setEvents" : function () {
    var that = this;

    this.namefilter.on('submit', function(e) {
      e.preventDefault();
      that.doCombinedFilter();
    });

    this.viewmorebutton.on('click', function(e) {
      e.preventDefault();
      that.doViewMore();
    });

    // window.resize event
    // allPageMax based on browser window width
    // 3 > 1200
    // 2 > 992
    // 1

  },    

  "cloneArray" : function (array) {
    var that = this,
      ai = 0,
      ac = array.length,
      ta,
      newArray = [],
      newObject;

    do {
      ta = array[ai];

      newObject = {};

      Object.keys(ta).map(function(property){
        newObject[property] = ta[property];
      });
      newArray.push(newObject);

      /* */

      ai++;
    } while (ai < ac)

    return newArray;
  }

  /* ---------------------------------------------------------------------------------------------------------------- */

}
window.OfficePeople = OfficePeople;
// export default Utility;

/*
https://www.paulirish.com/2009/throttled-smartresize-jquery-event-handler/
Debounced Resize() jQuery Plugin
August 11th 2009
*/
(function($,sr){

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null;
          };

          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);

          timeout = setTimeout(delayed, threshold || 100);
      };
  }
  // smartresize 
  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');

var supportsFlexBox;

var offices = [
  {"lat": 51.4527758, "lng": -2.5883412}
];

var markers = [];
var map;

function drop() {
  clearMarkers();
  for (var i = 0; i < offices.length; i++) {
    addMarkerWithTimeout(offices[i], i * 400);
  }
}

function addMarkerWithTimeout(position, timeout) {
  window.setTimeout(function() {
    try {
      markers.push(new google.maps.Marker({
        position: position,
        map: map,
        animation: google.maps.Animation.DROP
      }));
    } catch(error) {}
  }, timeout);
}

function clearMarkers() {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(null);
  }
  markers = [];
}

function initMap() {
  if ( typeof google !== 'undefined') {
    map = new google.maps.Map(document.getElementById("map"), {
      "zoom": 12,
      "center": {"lat": 51.4527758, "lng": -2.5883412}
    });

    drop();
  }
}

$(document).ready(function() {

  console.log('ofice-single.js');

  supportsFlexBox = $('html').hasClass('flex');
  if ( supportsFlexBox ) {
    console.log('flexbox supported');
  } else {
    console.log('flexbox not supported');
  }

  // pageWidth based on browser window width
  var pageWidth = 1;
  if ($(window).width() > 768) {
    pageWidth = 2;
  }
  if ($(window).width() > 1200) {
    pageWidth = 3;
  }
  
  window.thisOfficePeople = new OfficePeople({
    "seniorPeopleBox" : $('#people-senior'),
    "seniorHeadBox" : $('#senior-head'),
    "allPeopleBox" : $('#people-all'),
    "seniorTemplate" : $('.seniorTemplate'),
    "personTemplate" : $('.personTemplate'),
    "namefilter" : $('#peoplefilterForm'),
    "jobfilter" : $('#categoryfilter'),
    "viewmorebutton" : $('.viewMorePeople'),
    "viewmoreBox" : $('.vmClipper'),
    "allPageMax" : 6, // display up to 6 items for each page
    "pageWidth" : pageWidth,
    "datapath" : "../assets/offices/ireland.json",
    "portraits" : "https://www.brewin.co.uk",
    "placeholder" : "../assets/images/placeholder-473x473.png",
    "flexsupport" : supportsFlexBox
  });

  // 
    $(window).smartresize(function(){

      var seniorMaxBoxHeight = 0,
          seniorBoxes = $('ul.flexSenior li');
      seniorBoxes.find('.sizer').each(function(){
        if ( $(this).height() > seniorMaxBoxHeight) { seniorMaxBoxHeight = $(this).height(); }
      });
      seniorBoxes.css({"min-height" : (seniorMaxBoxHeight + 10) + "px"});

      var juniorMaxBoxHeight = 0,
          juniorCumulativeHeight = 0,
          juniorBoxes = $('ul.flexJunior li');

      juniorBoxes.find('.sizer').each(function(){
        if ( $(this).height() > juniorMaxBoxHeight) { juniorMaxBoxHeight = $(this).height(); }
      });

      juniorBoxes.css({"min-height" : (juniorMaxBoxHeight + 10) + "px"});

      juniorCumulativeHeight = juniorBoxes.length * (juniorMaxBoxHeight + 20);

      $('.viewJunior').css({"max-height" : juniorCumulativeHeight + "px"}); 
    });
});