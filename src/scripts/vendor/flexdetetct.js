// https://gist.github.com/davidhund/b995353fdf9ce387b8a2
function cssPropertySupported(pNames) {
	var element = document.createElement('a');
	var index = pNames.length;

	try {
		while (index--) {
			var name = pNames[index];

			element.style.display = name;
			if (element.style.display === name) {
				return true;
			}
		}
	} catch (pError) {}

	return false;
}

// cssPropertySupported(['-webkit-box', '-ms-flex', 'flex']);

(function(d){
  var c = ( cssPropertySupported(['-webkit-box', '-ms-flex', 'flex'])) ? " flex" : " noflex";
  d.documentElement.className += c; 
})(document);