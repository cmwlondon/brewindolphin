/* ------------------------------------------------
BDFormValidate
START
extends https://validatejs.org/
------------------------------------------------ */

  function BDFormValidate(parameters) {
    this.parameters = parameters;

    this.formSelector = parameters.formSelector;
    this.constraints = parameters.constraints;

    this.xcheckboxes = parameters.xcheckboxes;
    this.xcheckboxesProcess = false;

    this.clientField = parameters.clientField;
    this.clientProcess = false;

    this.validform = parameters.validform;
    this.placholderSupported = placholderSupported;

    this.init();
  }

  BDFormValidate.prototype = {
    "constructor" : BDFormValidate,

    "template" : function () { var that = this; },

    "init" : function () {
      var that = this;

      // detect validation config for checkbox 
      if (typeof this.xcheckboxes !== 'undefined') {
        this.xcheckboxesProcess = true;
      }

      // detect validation for client-yes/no radio button group
      if (typeof this.clientField !== 'undefined') {
        this.clientProcess = true;
      }

      this.form = document.querySelector( this.formSelector );

      // find checkboxes in constraints
      // this.removeCheckboxFields();

      /* custom validators */
      // yes/no radio button group

      // checkbox
      validate.validators.checked = function(value, options) {

        console.log("validate.validators.checked '%s' '%s'", value, options);

        if ( value === null ) {
          return options.message
        } else {
          return null;
        }
      };

      // postcode
      validate.validators.postcode = function(value, options, key, attributes) {
        var valid = true;
        var blankRegEx = new RegExp('^\\s*$', '');

        // var postcodeRegex = new RegExp('^(([gG][iI][rR] {0,}0[aA]{2})|((([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y]?[0-9][0-9]?)|(([a-pr-uwyzA-PR-UWYZ][0-9][a-hjkstuwA-HJKSTUW])|([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y][0-9][abehmnprv-yABEHMNPRV-Y]))) {0,}[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}))$', '');
        var postcodeRegex = new RegExp('^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$', '');

        if ( value === null ) {
          valid = false;
        } else {
          if ( blankRegEx.test(value) ) {
            valid = false;
          } else {
            if ( !postcodeRegex.test(value) ) {
              valid = false;
            }
          }
        }

        if ( valid ){
          return null;
        } else {
          return options.message;
        }
      }

      // telephone number
      validate.validators.telephone = function(value, options, key, attributes) {
        var valid = true;
        var blankRegEx = new RegExp('^\\s*$', '');

        var phoneRegex = new RegExp('^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\\s\\./0-9]*$', 'g');
        /* valid telephone number formats based in regexp
        329923492
        +(9) 999 999
        +(99)-999-999
        +(999).999.999
        (999).999.999
        999 999 999
        999-999-999
        999.999.999
        */

        if ( value === null ) {
          valid = false;
        } else {
          if ( blankRegEx.test(value) ) {
            valid = false;
          } else {
            if ( !phoneRegex.test(value) ) {
              valid = false;
            }
          }
        }

        if ( valid ){
          return null;
        } else {
          return options.message;
        }

      };

      /*
      google recaptcha v2 "I'm not a robot" checkbox
      https://developers.google.com/recaptcha/docs/display
      */
      validate.validators.recaptchav2 = function(value, options, key, attributes) {
        // client-side validation for convenience only, does not actually verify that the form has been submitted by a human
        // NEEDS to be verified on the server
        // https://developers.google.com/recaptcha/docs/verify

        if ( value !== null) {
          return null;
        } else {
          return options.message;
        }
      };
      /* */

      if ( this.form !== null) {

        // will probably need to modify this to include other field types at some point
        /**/
        this.inputs = this.form.querySelectorAll("input[type='text'],input[type='email'],select,textarea,input[type='checkbox'],input[type='radio']");
        /**/

        if (!this.placholderSupported) {
          this.setPlaceholders();
        }

        // bind validation to form submit
        this.form.addEventListener("submit", function(ev) {

          ev.preventDefault(ev);
          var tgt = ev.target || ev.srcElement;

          that.inputs = that.form.querySelectorAll("input[type='text'],input[type='email'],select,textarea,input[type='checkbox'],input[type='radio']");
          // go through form inputs
          // clear inputs where value = placeholder
          // var pfields = that.form.querySelectorAll("input[type='text'], textarea"),
          //     pfield;
          // for (var i = 0; i < pfields.length; ++i) {
          //   pfield = pfields.item(i);
          //   if (pfield.getAttribute('placeholder') !== null && pfield.getAttribute('placeholder') !=='' && pfield.getAttribute('placeholder') === pfield.value) {
          //     pfield.value = '';
          //   }
          // }

          // do standard validate.js form validation
          var errors = validate( tgt, that.constraints );

          // add in validation process for checkboxes with hidden fields
          if ( that.xcheckboxesProcess ) {
            errors = that.processCheckboxFields(errors);
          }

          // add in validation process for client yes/no field
          if ( that.clientProcess ) {
            errors = that.processClient(errors);
          }

          // validation: passed or failed?
          if (typeof errors !== 'undefined') {
            // failed
            that.showErrors(errors);
          } else {
            // passed
            that.clearErrors();
            that.validform(that.form);

            // google recaptcha v2 invisible
            // check captcha after all form fields validate
            // grecaptcha.execute();
          }
        });

        // bind validation to input field blur/change events
        for (var i = 0; i < this.inputs.length; ++i) {
          this.inputs.item(i).addEventListener("focus", function(ev) {
            if ( !this.placholderSupported ) {
              that.clearPlaceholder(this);
            }
          });

          this.inputs.item(i).addEventListener("blur", function(ev) {
            that.fieldValidate(this);
            // that.setPlaceholder(this);
          });

          this.inputs.item(i).addEventListener("change", function(ev) {
            //that.fieldValidate(this);
          });
        }

      }

    },

    "removeCheckboxFields" : function () {
      this.deleteFields = [];
      var n = Object.entries(this.constraints), i = 0; l = n.length;
      do {
        if ( n[i][1].hasOwnProperty('checked') ) {
          this.deleteFields.push({
            "field" : n[i][0],
            "required" : n[i][1]['checked']['required'],
            "message" : n[i][1]['checked']['message']
          });
        }
        i++;
      } while (i < l)

      // remove checkbox items  from normal validation system
      this.deleteFields.map(function(item) {
        delete this.constraints[item['field']];
      },this);
    },

    "processCheckboxFields" : function (errors) {
      if (this.xcheckboxes.length > 0) {

        var errorList = [];
        this.xcheckboxes.map(function(xcheckbox,index) {
          // get checkbox input element
          var xcb = this.form.querySelectorAll("input[type='checkbox'][name='" + xcheckbox['name'] + "']")[0];
          // get hidden input element
          var xch = this.form.querySelectorAll("input[type='hidden'][name='" + xcheckbox['name'] + "']")[0];

          /*
          ------------------------------------------
          simple single checkbox required validation
          ------------------------------------------
          */
          if ( xcb.checked ) {
            // set hidden field value to value of checkbox 
            xch.value = xcb.value;
          } else {
            // set hidden field value to 'false' 
            xch.value = 'false';
            // add checkbox error message to errors
            errorList.push(xcheckbox);
          }
        },this);
      }

      if ( errorList.length > 0 ) {
        // create errors object if it doesn't already exist
        if ( typeof errors === 'undefined' ) {
          errors = {};  
        }

        errorList.map(function(error) {
          errors[error['name']] = error['message']; 
        }, this);

      }
      return errors;
    },

    // validate 'are you a client: yes/no' radio field
    "processClient" : function (errors) {
      var errorList = [],
          choices = 0;

      // count the number of selected buttons
      this.clientField.choices.map(function(choice,index) {
        var radio = this.form.querySelectorAll("input[type='radio']#" + choice)[0];
        if ( radio.checked ) 
          choices++;
      }, this);      

      // allow only one button to be selected
      if( choices !== 1 ) {
        errorList.push( {
          "name" : this.clientField.name,
          "message" : this.clientField.message
        } );        
      }

      // field is invalid, return errors
      if ( errorList.length > 0 ) {
        // create errors object if it doesn't already exist
        if ( typeof errors === 'undefined' ) {
          errors = {};  
        }

        errorList.map(function(error) {
          errors[error['name']] = error['message']; 
        }, this);

      }
      return errors;
    },

    "setPlaceholders" : function () {
        this.pfields = this.form.querySelectorAll("input[type='text'], input[type='email'], textarea");
        var thispf,pfPlaceholder;
        for (var i = 0; i < this.pfields.length; ++i) {
          thispf = this.pfields.item(i);
          pfPlaceholder = thispf.getAttribute('placeholder');
          thispf.value = pfPlaceholder;
          thispf.classList.add('usingPlaceholder');
        }
    },

    "clearPlaceholder" : function (input) {
      // clear value
      if (input.value === input.getAttribute('placeholder')) {
        input.value = '';
      }
    },

    "setPlaceholder" : function (input) {
      // clear value
      if (input.value === '') {
        input.value = input.getAttribute('placeholder');
      }
    },

    "fieldValidate" : function (input) {
      // check to see if placeholders supported
      if ( !this.placholderSupported ) {
        // check to see if field value matches placeholder
        if (input.value === input.getAttribute('placeholder')) {
        }
      }

      var errors = validate(this.form, this.constraints) || {};

      if ( (typeof errors !== 'undefined' && typeof errors[input.name] !== 'undefined') && input.value !== input.getAttribute('placeholder')) {
        this.showErrorsForInput(input, errors[input.name]);
      } else {
        this.showErrorsForInput(input, '');
      }

      // reset placeholder if no value has been set
      if ( !this.placholderSupported ) {
        this.setPlaceholder(input);
      }
    },

    "showErrors" : function (errors) {

        var entries = this.inputs,
            entrycount = entries.length;
        // for(var entry of this.inputs.entries()) { 
          // var field = entry[1], fieldName = field.name, fieldType = field.type;
        for(entryIndex = 0; entryIndex < entrycount; entryIndex++ ) { 
          var entry = entries[entryIndex];
          var field = entry, fieldName = field.name, fieldType = field.type;

          // if ( fieldName = field.name) {} else {}
          if (typeof errors !== 'undefined' && typeof errors[fieldName] !== 'undefined' ) {
            this.showErrorsForInput(field, fieldName, errors[fieldName]);
          } else {
            this.showErrorsForInput(field, fieldName, '');
          }
         
        }

        // google recaptcha v2 - reset recaptcha if other form fields are invalid
        // grecaptcha.reset();
    },

    "showErrorsForInput" : function ( input, name, error ) {

        // find input container .form-group
        if ( typeof error !== 'undefined') {

          var pnode = input;

          switch( name ){
            case 'client' :
            case 'clientOverlay' : {
              // get fieldset parent for radio button group
              do {
                pnode = pnode.parentNode;
              } while ( !pnode.classList.contains('clientYesNo') )
              formGroup = pnode;

            } break;

            case 'clientstatus' : {
              // skip over hidden field
              formGroup = false;
            } break;

            default : {
              // default, find input field's parent .form-group div
              do {
                pnode = pnode.parentNode;
              } while ( typeof pnode.classList !== 'undefined' && !pnode.classList.contains('form-group') )
              formGroup = pnode;
            }
          }

          if( formGroup ) {
            var messages = formGroup.querySelector(".messages");
            // check to see if the field has 'messages' element
            if ( messages !== null ) {
              if ( error === '' & typeof error !== 'undefined' ) {
                this.resetFormGroup(formGroup,messages);
              } else {
                formGroup.classList.add("has-error");
                messages.innerText = error;
              }
            }
          }

        }
    },

    "clearErrors" : function () {
        var entries = this.inputs,
            entrycount = entries.length;

        for(entryIndex = 0; entryIndex < entrycount; entryIndex++ ) { 
          var entry = entries[entryIndex];
          var field = entry, fieldName = field.name, fieldType = field.type;
          this.showErrorsForInput(field, fieldName, '');
        }

        /*
        not compatible with IE 9 Array.Iterator not supported
        for(var entry of this.inputs.entries()) { 
          var field = entry[1], fieldName = field.name, fieldType = field.type
          this.showErrorsForInput(field, fieldName, '');
        }
        */
    },


    "resetFormGroup" : function ( formGroup, messages ) {

        formGroup.classList.remove("has-error");
        messages.innerText = '';
    },

    "closestParent" : function ( child, className ) {
      var that = this;

        if (!child || child == document) {
          return null;
        }
        if (child.classList.contains(className)) {
          return child;
        } else {
          return this.closestParent(child.parentNode, className);
        }
    },

    "doCaptcha" : function ( response ) {
    }

      /* ---------------------------------------------------------------------------------------------------------------- */

  }
  window.BDFormValidate = BDFormValidate;
  // export default BDFormValidate;
/*
------------------------------------------------
BDFormValidate
END
------------------------------------------------
*/

/* nexus 5 */

var menuBtn = document.querySelector(".btn-menu");
    body = document.getElementsByTagName("BODY")[0],
    menuPrimaryParent = document.querySelectorAll(".has__secondary__menu"),
    menuPrimaryLinks = document.querySelectorAll(".has__secondary__menu > a"),
    menuSecondaryParent = document.querySelectorAll(".has__tertiary__menu"),
    menuSecondaryLinks = document.querySelectorAll(".has__tertiary__menu > a"),
    categoryLink = document.querySelectorAll(".masthead-category--selected > a")[0],
    categoryList = document.querySelectorAll(".masthead-categories-sub")[0],
    openSubmenu = -1,
    mobileEnabled = false,
    mobileBreakpoint = 992,
    w1 = -1,w2 = -1,
    categoryListTimer = null,
    clShow = false,
    placholderSupported = (typeof document.createElement("input").placeholder === 'string');

/*
TEST CODE ONLY - START
*/
// switch page colour scheme based on URL parameter cs
// cs=(individual|professional|charity|financial|task1762)
var address = window.location.href,
    colourschemeRegex = new RegExp('cs=(individual|professional|charity|financial|task1762)', '');

var result = colourschemeRegex.exec(address);
if(result != null){
  body.classList.remove("cs_individual","cs_professional","cs_financial","cs_charity","cs_task1762");
  body.classList.add("cs_" + result[1]);
}

// switch google recaptcha key based on host
var hostRegex = new RegExp('(localhost|production.stackworks.com)')
    hostresult = hostRegex.exec(address),
    host = '',
    key = '';

switch( hostresult[1] ) {
  case "localhost" : {
    host = 'localhost';
    key = '6LdmF2sUAAAAAD3wSs1pxnHyd_-f6D1curNtDLQG';
  } break;
  case "production.stackworks.com" : {
    host = 'staging';
    key = '6LdJJmsUAAAAALOTw0lJvSBIUI0twtnl-6ebIQ19';
  } break;
  default : {
    // live / integration / preproduction
    host = 'live';
    key = '?';
  } break;
} 

/*
TEST CODE ONLY - END
*/

/*
handle background-attachemnt:fixed alternate in iOS
*/
var isIOS = (navigator.userAgent.match(/(iPad|iPhone|iPod)/g)) ? true : false,
    iosTestRegex = new RegExp('ios=(on|off)', ''); // test mode, add '?ios=on' to end of URL
var result = iosTestRegex.exec(address);
if(result != null){
    isIOS = (result[1] === 'on');
}
body.classList.add( (isIOS) ? 'ios' : 'notios');

/*
--------------------------------------------------------------------------------
menu handler - START
--------------------------------------------------------------------------------
*/

// navigation menu click event handlers for mobile
function menuOpenHandler(e) {
  if ( mobileEnabled ) {
    if (body.classList.contains('menu__open')) {
      body.classList.remove("menu__open");
    } else {
      body.classList.add("menu__open");
    }
  }
};

function secondaryMenuOpenHandler(e) {

  if ( mobileEnabled ) {
    e.preventDefault();
    var linkItem = e.target,
        linkIndex = linkItem.getAttribute('data-submenu'),
        linkParent = linkItem.parentElement;

    // ** single item open at a time
    // click on item to open it
    // click on same item to close it
    // while an item is open, clicking on another item will close the open item and open the clicked item

    if (openSubmenu === linkIndex) {
      // clicked on open item, close it
      menuPrimaryLinks[linkIndex].parentElement.classList.remove('menu__secondary__open');
      openSubmenu = -1;
    } else {
      if (openSubmenu > -1) {
        // open menu
        // clicked on different item, close open item
        menuPrimaryLinks[openSubmenu].parentElement.classList.remove('menu__secondary__open');
      }
      // open clicked item
      linkParent.classList.add('menu__secondary__open');
      openSubmenu = linkIndex;
    }
  }
}


function tertiaryMenuOpenHandler(e) {

  if ( mobileEnabled ) {
    e.preventDefault();
    var linkItem = e.target,
        linkIndex = linkItem.getAttribute('data-submenu'),
        linkParent = linkItem.parentElement;

    // ** single item open at a time
    // click on item to open it
    // click on same item to close it
    // while an item is open, clicking on another item will close the open item and open the clicked item

    if (openSubmenu === linkIndex) {
      // clicked on open item, close it
      menuSecondaryLinks[linkIndex].parentElement.classList.remove('menu__tertiary__open');
      openSubmenu = -1;
    } else {
      if (openSubmenu > -1) {
        // open menu
        // clicked on different item, close open item
        menuSecondaryLinks[openSubmenu].parentElement.classList.remove('menu__tertiary__open');
      }
      // open clicked item
      linkParent.classList.add('menu__tertiary__open');
      openSubmenu = linkIndex;
    }
  }
}

function categoryListHandler(e) {
  if ( mobileEnabled ) {
    if(categoryList.classList.contains('masthead-categories-sub--visible')) {
      categoryList.classList.remove('masthead-categories-sub--visible');
    } else {
      categoryList.classList.add('masthead-categories-sub--visible');
    }
  }
}

function categoryListHover(e) {
  if ( !mobileEnabled ) {
    // categoryList = 'ul.masthead-categories-sub'
    // show menu
    categoryList.classList.add('masthead-categories-sub--visible');

    categoryList.addEventListener('mouseenter', function(e) {
      categoryListTimer = null;
      // if ( categoryListTimer !== null ) { categoryListTimer.clearTimeout(); }
    });

    // add mouseout event to menu
    categoryList.addEventListener('mouseleave', function(e) {
      // start timer
      categoryListTimer = window.setTimeout(function() {
        categoryListTimer = null;
        if ( !mobileEnabled ) {
          categoryList.classList.remove('masthead-categories-sub--visible');
        }
      },100);
    }, false);
  }
}

// add click(touch) event handlers for menus
// check for menu button in HTML
if ( menuBtn !== null ) {
  mobileEnabled = (window.innerWidth < mobileBreakpoint);
  w1 = window.innerWidth;

  // mobile 'burger' button
  if (menuBtn !== null) {
    // menuBtn.addEventListener('click', menuOpenHandler, false);
    menuBtn.addEventListener('click', function(e) { e.preventDefault(); menuOpenHandler(); }, false);
  }

  if (typeof categoryLink !== 'undefined') {
    categoryLink.addEventListener('click', function(e) { e.preventDefault(); categoryListHandler(); }, false);
    categoryLink.addEventListener('mouseover', function(e) { e.preventDefault(); categoryListHover(); }, false);
  }

  if (menuPrimaryLinks.length > 0) {
    // add accordion behaviour to secondary level navigation in mobile menu
    var li = 0,lc = menuPrimaryLinks.length;
    do {
      menuPrimaryLinks[li].setAttribute('data-submenu',li);
      menuPrimaryLinks[li].addEventListener('click', function(e) { e.preventDefault(); secondaryMenuOpenHandler(e); }, false);
      li++;
    } while (li < lc)
  }

  if (menuSecondaryLinks.length > 0) {
    // add accordion behaviour to tertiary level navigation in mobile menu
    var li = 0,lc = menuSecondaryLinks.length;
    do {
      menuSecondaryLinks[li].setAttribute('data-submenu',li);
      menuSecondaryLinks[li].addEventListener('click', function(e) { e.preventDefault(); tertiaryMenuOpenHandler(e); }, false);
      li++;
    } while (li < lc)
  }

  /*
  if(menuPrimaryParent.length > 0){
    var li = 0, lc = menuPrimaryParent.length;
    do {
      //copy the parent item into child list so it can function as a link and not just toggle visibility of child list

      //link href
      var liHref = menuPrimaryParent[li].querySelector("li > a").getAttribute("href");

      //link text
      var liText = menuPrimaryParent[li].querySelector("li > a").text;

      //child ul
      var childUl = menuPrimaryParent[li].querySelector("li > ul");

      //create new li element
      var newLi = document.createElement("li");
      var newHref = document.createElement("a");
      newLi.className = "navigation__additional__mob";
      newHref.setAttribute("href", liHref);
      newHref.text = liText;

      newLi.appendChild(newHref);

      childUl.insertBefore(newLi, childUl.childNodes[0]);

      li++;
    } while (li < lc)
  }
  */

  if(menuSecondaryParent.length > 0){
    var li = 0, lc = menuSecondaryParent.length;
    do {
      //copy the parent item into child list so it can function as a link and not just toggle visibility of child list

      //link href
      var liHref = menuSecondaryParent[li].querySelector("li > a").getAttribute("href");

      //link text
      var liText = menuSecondaryParent[li].querySelector("li > a").text;

      //child ul
      var childUl = menuSecondaryParent[li].querySelector("li > ul");

      //create new li element
      var newLi = document.createElement("li");
      var newHref = document.createElement("a");
      newLi.className = "navigation__additional__mob";
      newHref.setAttribute("href", liHref);
      newHref.text = liText;
      newLi.appendChild(newHref);

      childUl.insertBefore(newLi, childUl.childNodes[0]);

      li++;
    } while (li < lc)
  }

  window.addEventListener('resize', function(e) {
    var wx = window.innerWidth;
    mobileEnabled = (wx < mobileBreakpoint);

    // find transition FROM mobile to desktop, remove mobile active styles
    if (wx < mobileBreakpoint) { w1 = wx; }

    if (wx > mobileBreakpoint) { w2 = wx; }

    if ( !mobileEnabled && (w1 < mobileBreakpoint) && (w2 > mobileBreakpoint) ) {
      w1 = w2;
      // clear any open menu states
      categoryList.classList.remove('masthead-categories-sub--visible');
      body.classList.remove("menu__open");
    }
  }, false);

}

/*
--------------------------------------------------------------------------------
menu handler - END
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
Expandable 'Find out More' Section controls
START
--------------------------------------------------------------------------------
*/
// for forms with radio button groups reset all redio buttons
var btnExpand = document.querySelectorAll('.find-more__header .button')[0],
  panelExpanded = document.querySelectorAll('.find-more__expanded')[0],
  btnClose = document.querySelectorAll('.find-more__close__button')[0];

if (typeof btnClose != "undefined") {
  btnExpandContainer = btnExpand.parentElement;
}

if (typeof btnExpand != 'undefined') {

  btnExpand.addEventListener("click", function(e) {
    if (!btnExpandContainer.classList.contains('button--hidden')) {
      btnExpandContainer.classList.add('button--hidden');
      panelExpanded.classList.add('find-more__expanded--visible');
    }

    // for forms with radio button groups reset all redio buttons    
    // .clientYesNo > input[type="radio"]
    var radioButtons = document.querySelectorAll('.clientYesNo input');
    var rbIndex = 0,
        rbCount = radioButtons.length,
        thisRb;

    if ( rbCount > 0 ) {
      do {
        thisRb = radioButtons[rbIndex];

        thisRb.checked = false;

        rbIndex++;
      } while ( rbIndex < rbCount)
    }

  });

  btnClose.addEventListener("click", function(e) {
    e.preventDefault();
    panelExpanded.classList.remove('find-more__expanded--visible');
    btnExpandContainer.classList.remove('button--hidden');
  });
}
/*
--------------------------------------------------------------------------------
Expandable 'Find out More' Section controls
END
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
bps2 new type of expander element
  .bp2_expander1
  .bp2_expander1 .wrapper
  .expanderButton
  .collapseButton
START
--------------------------------------------------------------------------------
*/
var btnExpand = document.querySelectorAll('.bp2_expander1');

if(btnExpand.length > 0) {
  var index = 0, expanderItem, closer, opener;
  do {
    expanderItem = btnExpand[index];

    // if (expanderItem != undefined) {

    opener = expanderItem.querySelectorAll('.expanderButton')[0];
    closer = expanderItem.querySelectorAll('.collapseButton')[0];

    opener.addEventListener("click", function(e) {
      e.preventDefault();
      var btnExpandContainer = e.target.parentElement;

      if ( !btnExpandContainer.classList.contains('bp2_expander1') ) {
        do {
          btnExpandContainer = btnExpandContainer.parentElement;
        } while ( !btnExpandContainer.classList.contains('bp2_expander1') )
      }

      btnExpandContainer.classList.add('expanded-visible');
    });

    closer.addEventListener("click", function(e) {
      e.preventDefault();
      var btnExpandContainer = e.target.parentElement;

      if ( !btnExpandContainer.classList.contains('bp2_expander1') ) {
        do {
          btnExpandContainer = btnExpandContainer.parentElement;
        } while ( !btnExpandContainer.classList.contains('bp2_expander1') )
      }

      btnExpandContainer.classList.remove('expanded-visible');
    });

    // }

    index++;
  } while (index < btnExpand.length)
}

/*
--------------------------------------------------------------------------------
bps2 new type of expander element
END
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
Slider code
START
--------------------------------------------------------------------------------
*/
document.addEventListener('DOMContentLoaded', function() {
  var simple_dots = document.querySelectorAll('.js__pagination');

  if(simple_dots.length) {

    var dot_count = simple_dots[0].querySelectorAll('.js__slide').length;
    var dot_container = simple_dots[0].querySelector('.js_dots');
    var dot_list_item = document.createElement('li');
    function handleDotEvent(e) {
      if (e.type === 'before.lory.init') {
        for (var i = 0, len = dot_count; i < len; i++) {
          var clone = dot_list_item.cloneNode();
          dot_container.appendChild(clone);
        }
        dot_container.childNodes[0].classList.add('active');
      }
      if (e.type === 'after.lory.init') {
        for (var i = 0, len = dot_count; i < len; i++) {
          dot_container.childNodes[i].addEventListener('click', function(e) {
            dot_navigation_slider.slideTo(Array.prototype.indexOf.call(dot_container.childNodes, e.target));
          });
        }
      }
      if (e.type === 'after.lory.slide') {
        for (var i = 0, len = dot_container.childNodes.length; i < len; i++) {
          dot_container.childNodes[i].classList.remove('active');
        }
        dot_container.childNodes[e.detail.currentSlide - 1].classList.add('active');
      }
      if (e.type === 'on.lory.resize') {
        for (var i = 0, len = dot_container.childNodes.length; i < len; i++) {
          dot_container.childNodes[i].classList.remove('active');
        }
        dot_container.childNodes[0].classList.add('active');
      }
    }
    simple_dots[0].addEventListener('before.lory.init', handleDotEvent);
    simple_dots[0].addEventListener('after.lory.init', handleDotEvent);
    simple_dots[0].addEventListener('after.lory.slide', handleDotEvent);
    simple_dots[0].addEventListener('on.lory.resize', handleDotEvent);

    var dot_navigation_slider = lory(simple_dots[0], {
      infinite: 1,
      enableMouseEvents: true
    });
  }
});

tickboxes = document.querySelectorAll(".form-xcheck");
/*
--------------------------------------------------------------------------------
Slider code
END
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
form validation
START
--------------------------------------------------------------------------------
*/
var btnSubmit = document.querySelectorAll('.form__section--submit .button')[0],
  formSections = document.querySelectorAll('.form__body'),
  formSubmitted = document.querySelectorAll('.form__submitted')[0];
window.addEventListener('load', function() {

  // https://validatejs.org/, http://validatejs.org/examples.html
  // supports IE 9

  // prevent attribute 'name' prepend to error messages
  validate.options = { "fullMessages" : false };

  // common field validation rules/messages used across most forms
  var validationPatterns = {
    "forename" : {
      "presence" : { "message" : "Please supply your forename"},
      "format" : {
        "pattern" : /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
        "message" : "Please supply your forename"
      }
    },
    "lastname" : {
      "presence" :  { "message" : "Please supply your surname"},
      "format" : {
        "pattern" : /^[a-zA-Z\-\' \u2018\u2019]{1,30}$/,
        "message" : "Please supply your surname"
      }
    },
    "email" : {
      "presence" : { "message" : "Please supply an email address"},
      "format" : {
        "pattern" : /^[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/,
        "message" : "Please supply a valid email address"
      }
    },
    "phone" : {
      "presence" : { "message" : "Please supply a contact number"},
      "format" : {
        "pattern" : /^[+]*[(]{0,1}[0-9 ]{1,3}[)]{0,1}[-\\s\\./0-9 x]{6,20}$/,
        "message" : "Please supply a valid number"
      }
    },
    "postcode" : {
      "presence" : { "message" : "Please enter a location"},
      "format" : {
        "pattern" : /^(?:(?:[a-zA-Z]{1,2}[0-9]{1,2}[a-zA-Z]{0,2} {0,1}[0-9]{0,2}[a-zA-Z]{0,2})|(?:(?:[a-zA-Z]{1}[0-9]{1,2}[a-zA-Z]{0,1} {0,1}[a-zA-Z0-9]{4}))|(?:(?:[a-zA-Z0-9]{1,10}) [0-9 ]{1,2}))$/,
        "message" : "Please supply a valid postcode"
      }
    }
  };

  // request a callback
  // Inline / expander version
  if (document.querySelectorAll('form#callbackForm').length > 0) {

    var callbackFormValidator = new BDFormValidate({
      "formSelector" : "form#callbackForm",
      "constraints" : {
        "Firstname1" : validationPatterns.forename,
        "Lastname1" : validationPatterns.lastname,
        "Email1" : validationPatterns.email,
        "Phone1" : validationPatterns.phone,
        "PostalCode1": validationPatterns.postcode,
        "g-recaptcha-response": {
          "recaptchav2" : {message: "Please prove you are human"}
        }
      },
      "clientField" : {
        "name" : "client",
        "choices" : ['clientyes','clientno'],
        "required" : true,
        "message" : "Please state if you are a client"
      },
      "validform" : function(form) {
        console.log('go');

        $('.inlineForm').addClass('showEnd');

        // form.submit();
      }
    });
    window.callbackFormValidator = callbackFormValidator;

    // reset radio button pre-selection in INLINE version of request-a-callback form
    $(document).ready(function(){
      // reset client radio buttons
      $('.clientYesNo input').each(function(i) {
        $(this).attr({"checked" : false});
      });
    });
  }

  // global CTA request a callback form overlay
  if (document.querySelectorAll('form#CTAcallbackForm').length > 0) {

    var CTAcallbackFormValidator = new BDFormValidate({
      "formSelector" : "form#CTAcallbackForm",
      "constraints" : {
        "Firstname91" : validationPatterns.forename,
        "Lastname91" : validationPatterns.lastname,
        "Email91" : validationPatterns.email,
        "Phone91" : validationPatterns.phone,
        "PostalCode91": validationPatterns.postcode,
        "g-recaptcha-response": {
          "recaptchav2" : {message: "Please prove you are human"}
        }
      },
      "clientField" : {
        "name" : "clientOverlay",
        "choices" : ['clientyesOverlay','clientnoOverlay'],
        "required" : true,
        "message" : "Please state if you are a client"
      },
      "validform" : function(form) {
        console.log('go');

        // switch to thanks you / confirm page in overlay
        // global CTA version
        if ( $('#req').length > 0 ) {
          $('#req').addClass('showEnd');
        }

        // ofices / our leadership version
        if ( $('#emailform').length > 0 ) {
          $('#emailform').addClass('showEnd');
        }

        // form.submit();
      }
    });
    window.CTAcallbackFormValidator = CTAcallbackFormValidator;
  }

  /* #feedback /bd/current/templates/feedback.html */
  if (document.querySelectorAll('form#feedback').length > 0) {

    var feedbackFormValidator = new BDFormValidate({
      "formSelector" : "form#feedback",
      "constraints" : {
        "typeOfFeedback" : {
          "presence" : {message: "Please state what form your feedback takes"}
        },
        "Firstname3" : validationPatterns.forename,
        "Lastname3" : validationPatterns.lastname,
        "Email3" : validationPatterns.email,
        "Phone3" : validationPatterns.phone,
        "contact-feedback": {
          "presence" : {message: "Please include your comments"}
        },
        "g-recaptcha-response": {
          "recaptchav2" : {message: "Please prove you are human"}
        }
      },
      "validform" : function(form) {
        console.log('go');

        $('.inlineForm').addClass('showEnd');

        // form.submit();
      }
    });
    window.feedbackFormValidator = feedbackFormValidator;
  }

  // subscrive to newsletter 
  // global CTA module
  // overlay
  if (document.querySelectorAll('form#CTAsusbcribe').length > 0) {
    
    var dwfrFormValidator = new BDFormValidate({
      "formSelector" : "form#CTAsusbcribe",
      "constraints" : {
        "Firstname62" : validationPatterns.forename,
        "Lastname62" : validationPatterns.lastname,
        "Email62" : validationPatterns.email,
        "PostalCode62": validationPatterns.postcode,
        "g-recaptcha-response": {
          "recaptchav2" : {message: "Please prove you are not a robot"}
        }
      },
      "xcheckboxes" : [
        {
          "name" : "MarketingCommsOptIn62",
          "required" : true,
          "message" : "Please confirm you are willing to receive emails from Brewin Dolphin"
        }
      ],
      "validform" : function(form) {
        console.log('go');
        // form.submit();

        // show overlay form 'thanks' page
        $('#news').addClass('showEnd');
      }
    });
    window.dwfrFormValidator = dwfrFormValidator;
  }

  // inline/expander subscribe form
  if (document.querySelectorAll('form#susbcribe').length > 0) {
    
    var dwfrForm52Validator = new BDFormValidate({
      "formSelector" : "form#susbcribe",
      "constraints" : {
        "Firstname" : validationPatterns.forename,
        "Lastname" : validationPatterns.lastname,
        "Email" : validationPatterns.email,
        "PostalCode": validationPatterns.postcode,
        "g-recaptcha-response": {
          "recaptchav2" : {message: "Please prove you are not a robot"}
        }
      },
      "xcheckboxes" : [
        {
          "name" : "MarketingCommsOptIn",
          "required" : true,
          "message" : "Please confirm you are willing to receive emails from Brewin Dolphin"
        }
      ],
      "validform" : function(form) {
        console.log('go');
        // form.submit();

        // show inline form 'thanks' page
        $('.inlineForm').addClass('showEnd');

      }
    });
    window.dwfrForm52Validator = dwfrForm52Validator;
  }

  // dopwnload family wealth report form
  // /templates/family-wealth-report.html
  if (document.querySelectorAll('form#dfwrForm').length > 0) {
    
    var dwfrForm88Validator = new BDFormValidate({
      "formSelector" : "form#dfwrForm",
      "constraints" : {
        "Firstname" : validationPatterns.forename,
        "Lastname" : validationPatterns.lastname,
        "Email" : validationPatterns.email,
        "PostalCode": validationPatterns.postcode,
        "g-recaptcha-response": {
          "recaptchav2" : {message: "Please prove you are not a robot"}
        }
      },
      "xcheckboxes" : [
        {
          "name" : "MarketingCommsOptIn",
          "required" : true,
          "message" : "Please confirm you are willing to receive emails from Brewin Dolphin"
        }
      ],
      "validform" : function(form) {
        console.log('go');
        // form.submit();

      }
    });
    window.dwfrForm88Validator = dwfrForm88Validator;
  }

  
  if (document.querySelectorAll('form#bps2Form').length > 0) {
    console.log('bps2 keep me updated');
    
    var dwfrForm88Validator = new BDFormValidate({
      "formSelector" : "form#bps2Form",
      "constraints" : {
        "Firstname" : validationPatterns.forename,
        "Lastname" : validationPatterns.lastname,
        "Email" : validationPatterns.email,
        "PostalCode": validationPatterns.postcode,
        "g-recaptcha-response": {
          "recaptchav2" : {message: "Please prove you are not a robot"}
        }
      },
      "validform" : function(form) {
        console.log('go');
        // form.submit();

      }
    });
    window.dwfrForm88Validator = dwfrForm88Validator;
  }

  // find-more-expandable office finder
  // /components/find-my-nearest-office.html
  // /components/start-a-conversation.html
  // <form data-validate validate="false" id="locationSearch2Form" data-target="../templates/office-finder.html" novalidate>
  /*
  ** REDIRECTS to office finder page
  */
  if (document.querySelectorAll('form#locationSearch2Form').length > 0) {
    var officeFinderFormValidator = new BDFormValidate({
      "formSelector" : "form#locationSearch2Form",
      "constraints" : {
        "locationsearch2": {
          "presence" : {message: "Please enter a location"}
        }
      },
      "validform" : function(form) {
        console.log('go');

        var redirect;
        // form.submit();
        if ( form.dataset ) {
          console.log( form.dataset.target );
          redirect = form.dataset.target;
        } else {
          console.log( form.getAttribute('data-target') );
          redirect = form.dataset.target;
        }
        window.location.href = redirect;
      }

    });
    window.officeFinderFormValidator = officeFinderFormValidator;
  }

});

/*
--------------------------------------------------------------------------------
form validation
END
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
Accordion JS
need to update this to handle window.resize, change of content arrangemnt in open items
START
--------------------------------------------------------------------------------
*/
var acc = document.getElementsByClassName("accordion__title");

for (var i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("accordion--active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}
/*
--------------------------------------------------------------------------------
Accordion JS
END
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
Accordion JS for sitemap
need to update this to handle window.resize, change of content arrangemnt in open items
START
--------------------------------------------------------------------------------
*/
var accSitemap = document.getElementsByClassName("sitemap__title");

for (var i = 0; i < accSitemap.length; i++) {
  accSitemap[i].addEventListener("click", function() {
    this.classList.toggle("sitemap--active");
    var panel = this.nextElementSibling;
    panel.classList.toggle("active");
    // if (panel.style.maxHeight){
    //   panel.style.maxHeight = null;
    // } else {
    //   panel.style.maxHeight = panel.scrollHeight + "px";
    // }
  });
}
/*
--------------------------------------------------------------------------------
Accordion JS for sitemap
END
--------------------------------------------------------------------------------
*/

/* vendors/optimizedResize.js*/
/*
window.addEventListener("optimizedResize", function() {
    console.log("Resource conscious resize callback!");
});
*/

// Placeholder fix for IE9.
/*
var inputs = document.querySelectorAll('input, textarea');

for(var inputCounter = 0; inputCounter < inputs.length; inputCounter++) {
  placeHolder(inputs[inputCounter]);
}
*/

/*
--------------------------------------------------------------------------------
Lightbox code
https://basiclightbox.electerious.com/
https://github.com/electerious/basicLightbox
incompatible with ANY version if IE - requires object.assign polyfill
/scr/scripts/vendors/
incompatible with IE9 - requires requestanimationframe polyfill

START
--------------------------------------------------------------------------------
*/
var lightbox = document.querySelectorAll('[data-basiclightbox]');
if ( lightbox.length ) {
  var lightboxes = [];
  var videosFound = false;
  // var lightboxOuter = lightbox.outerHTML;

  // var lightboxHTML = basicLightbox.create(lightbox[0].outerHTML,{ "closable": false });
  /* close terms lightbox HTML TEST ONLY */
  // lambda function no workee in IE
  // var lightboxHTML = basicLightbox.create(lightbox[0].outerHTML,{ "closable": false, "beforeShow" : (instance) => { instance.element().querySelector('a.lb-accept').onclick = instance.close; instance.element().querySelector('a.lb-decline').onclick = instance.close; }});

  for (var i = 0; i < lightbox.length; i++) {
    var thisLightbox = lightbox[i];
    var type = thisLightbox.getAttribute('data-type');

    if ( type === 'brighttalk') {

      lightboxes.push({
        "instance" : basicLightbox.create(thisLightbox.outerHTML,{
        // "instance" : basicLightbox.create('<p>VIDEO1</p>',{
          "closable": true,
          "className" : "brightalk",
          "beforeShow" : function(instance) { instance.element().querySelector('img.close').onclick = instance.close;  },
          "afterClose": function() {}
        }),
        "id" : thisLightbox.getAttribute('data-id')
      });
      videosFound = true;
    } else {
      var instance = basicLightbox.create(thisLightbox.outerHTML,{
        "closable": false,
        "beforeShow" : function(instance) { instance.element().querySelector('a.lb-accept').onclick = instance.close; instance.element().querySelector('a.lb-decline').onclick = instance.close; document.body.classList.add("noscroll")},
        "afterClose": function() {document.body.classList.remove("noscroll")}
      });
      lightboxes.push({
        "instance" : instance
      });

      document.addEventListener('DOMContentLoaded', function() {
        instance.show();
      });
    }

    if ( videosFound ) {
      document.addEventListener('DOMContentLoaded', function() {

        var acc = document.getElementsByClassName("video_popup_trigger");

        for (var j = 0; j < acc.length; j++) {
          acc[j].addEventListener("click", function(e) {
            e.preventDefault();

            // find lightbox instance to open based on link data-videoid attribute
            var vi = 0,vc = lightboxes.length, vf = false;
            do {
              var tv = lightboxes[vi];
              if ( tv.id === this.getAttribute('data-videoid')) {
                tv.instance.show();
              }
              vi++;
            } while (vi < vc && !vf)

          });
        }

      });
    }
  }
}
/*
--------------------------------------------------------------------------------
Lightbox code
END
--------------------------------------------------------------------------------
*/


/*
--------------------------------------------------------------------------------
Setting same height on columns with matchHeight.js plugin
START
--------------------------------------------------------------------------------
*/
if (typeof $ !== 'undefined') {
  var archiveContent = document.querySelectorAll('.archive__item__content');

  $(function() {
    if (archiveContent.length > 0) {
      $('.archive__item__content').matchHeight();
    }
  });

  var relatedLinks = document.querySelectorAll('.related-links__block');

  $(function() {
    if (relatedLinks.length > 0) {
      $('.related-links__block').matchHeight();
    }
  });

  var collectionItem = document.querySelectorAll('.collection__item__content');

  $(function() {
    if (collectionItem.length > 0) {
      $('.collection__item__content').matchHeight();
    }
  });

  var mh1Items = document.querySelectorAll('.mh1');

  $(function() {
    if (mh1Items.length > 0) {
      $('.mh1').matchHeight();
    }
  });

  var mh2Items = document.querySelectorAll('.mh2');

  $(function() {
    if (mh2Items.length > 0) {
      $('.mh2').matchHeight();
    }
  });
}
/*
--------------------------------------------------------------------------------
Setting same height on columns with matchHeight.js plugin
END
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
Global CTA - open overlays for 'request a callback' and 'sign up for newsletter'
START
--------------------------------------------------------------------------------
*/
// global CTA: Start a conversation
if ( $('.req_trigger').length > 0 ) {
  $('.req_trigger').on('click',function(e){
    e.preventDefault();

    // check to see if newsletter signup overlay is open
    if( $('body').hasClass('news_open') ) {
      $('body').removeClass('news_open')
    }

    // for forms with radio button groups reset all redio buttons    
    // .clientYesNo > input[type="radio"]
    $('.clientYesNo input').each(function(i) {
      $(this).attr({"checked" : false});
    });

    // reset form state
    var overlayNode = $('#req');
    overlayNode.removeClass('showEnd');
    overlayNode.find('form').eq(0).trigger("reset");
    overlayNode.find('.has-error').each(function(i) {
      $(this).removeClass('has-error');
      $(this).find('.messages').empty();
    });

    $('body').addClass('req_open');
  });

  $('#req .closer').on('click',function(e){
    e.preventDefault();
    $('body').removeClass('req_open');
  });

}

// Global CTA: Stay updated
if ( $('.news_trigger').length > 0 ) {
  $('.news_trigger').on('click',function(e){
    e.preventDefault();

    // check to see if request a callback overlay is open
    if( $('body').hasClass('req_open') ) {
      $('body').removeClass('req_open')
    }

    // reset form state
    var overlayNode = $('#news');
    overlayNode.removeClass('showEnd');
    overlayNode.find('form').eq(0).trigger("reset");
    overlayNode.find('.has-error').each(function(i) {
      $(this).removeClass('has-error');
      $(this).find('.messages').empty();
    });
    
    $('body').addClass('news_open');
  });

  $('#news .closer').on('click',function(e){
    e.preventDefault();
    $('body').removeClass('news_open');
  });
}

// our leadership email overlay
// /src/js/custom/ij_office.js
// line 197 "queueComplete": function (queue) {

/*
// bio email form
if ( $('.email_trigger').length > 0 ) {
  $('.email_trigger').on('click',function(e){
    e.preventDefault();

    // for forms with radio button groups reset all redio buttons    
    // .clientYesNo > input[type="radio"]
    $('.clientYesNo input').each(function(i) {
      $(this).attr({"checked" : false});
    });

    $('body').addClass('emailform_open');
  });

  $('#emailform .closer').on('click',function(e){
    e.preventDefault();
    $('body').removeClass('emailform_open');
  });
}
*/
/*
--------------------------------------------------------------------------------
Global CTA - open overlays for 'request a callback' and 'sign up for newsletter'
START
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
google captcha
START
--------------------------------------------------------------------------------
*/
// callback fired once gogole recaptcha v2 library has loaded and is ready for use
function captchaReady() {
  console.log('captcha is ready');

  window.onload = function() {
    var recaptchas = document.querySelectorAll('.google_recaptcha_v2');

    var index = 0,
        cursor;

    // iterate through google recaptcha placeholders div.google_recaptcha_v2
    do {
      cursor = recaptchas[index];
      cursor.setAttribute('data-key', key);
      // render recaptcha in placeholder
      grecaptcha.render(
        cursor,
        {
          "sitekey" : key,
          "theme": "light"
          // "callback" : correctCaptcha
        }
      );
          
      index++;
    } while ( index < recaptchas.length )
  };
  
}

function correctCaptcha(response) {
  console.log('correctCaptcha');
  // callbackFormValidator.doCaptcha(response);
};
/*
--------------------------------------------------------------------------------
google captcha
END
--------------------------------------------------------------------------------
*/
