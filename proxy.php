<?php

/*
https://developers.google.com/recaptcha/docs/verify
secret: '6LdGpGcUAAAAAKyDbWuWBycMRf4UCI_Dz4SaikER'
response: '03AHqfIOll44XSvy_p5LZ3Nw1KQVV7CQS4CD77fLs7m3GAkzYJxbC7MBjtRF-OnNtkDwvvXGM394-BmDX8nZOXgBilPcC8jfJ_wig7jVE4ybdH0Y98Rgpto92ACwtwbFmx65zZpDU0a7Z2Z9D6a0RqMk2eorc0R6alJKiExiVil36yhHJk_LA9Wx1fD9VDM2RlU8qjBpqzq1MEcozCWGvrEUtmJpilLAThmd_UthaS0NUNOwRq1vxbBLVWnDgWTYFGZ8SzeH_KxOMZ9jw2nsZ9kqqvShbM5YNOpA'

{
  "success": true|false,
  "challenge_ts": timestamp,  // timestamp of the challenge load (ISO format yyyy-MM-dd'T'HH:mm:ssZZ)
  "hostname": string,         // the hostname of the site where the reCAPTCHA was solved
  "error-codes": [...]        // optional
}
*/

# Our new data
$data = array(
	// 'secret'=> urlencode($_POST['secret']),
	// 'response' => urlencode($_POST['response'])
	'secret' => '6LdGpGcUAAAAAKyDbWuWBycMRf4UCI_Dz4SaikER',
	'response' => '03AHqfIOll44XSvy_p5LZ3Nw1KQVV7CQS4CD77fLs7m3GAkzYJxbC7MBjtRF-OnNtkDwvvXGM394-BmDX8nZOXgBilPcC8jfJ_wig7jVE4ybdH0Y98Rgpto92ACwtwbFmx65zZpDU0a7Z2Z9D6a0RqMk2eorc0R6alJKiExiVil36yhHJk_LA9Wx1fD9VDM2RlU8qjBpqzq1MEcozCWGvrEUtmJpilLAThmd_UthaS0NUNOwRq1vxbBLVWnDgWTYFGZ8SzeH_KxOMZ9jw2nsZ9kqqvShbM5YNOpA'
);


# Create a connection
$url = 'https://www.google.com/recaptcha/api/siteverify';
$ch = curl_init($url);

# Form data string
$postString = http_build_query($data, '', '&');
# Setting our options
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

# Get the response
$response = curl_exec($ch);
curl_close($ch);

$data['query'] = $postString;
$data['sx'] = $response;

header('Content-Type: application/json');
echo json_encode( $data );	
?>